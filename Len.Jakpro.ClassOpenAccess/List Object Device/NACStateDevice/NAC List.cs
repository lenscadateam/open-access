﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Len.Jakpro.ClassOpenAccess
{   
    public class NACList
    {
        private int _NAC1;
        private int _NAC2;
        private int _NAC3;
        private int _NAC4;
        private int _NAC5;
        private int _NAC6;
        private int _NAC7;
        private int _NAC8;
        private int _NAC9;
        private int _NAC10;
        private int _NAC11;
        private int _NAC12;

        public int NAC1
        {
            get { return _NAC1; }
            set { _NAC1 = value; }
        }
        public int NAC2
        {
            get { return _NAC2; }
            set { _NAC2 = value; }
        }
        public int NAC3
        {
            get { return _NAC3; }
            set { _NAC3 = value; }
        }
        public int NAC4
        {
            get { return _NAC4; }
            set { _NAC4 = value; }
        }
        public int NAC5
        {
            get { return _NAC5; }
            set { _NAC5 = value; }
        }
        public int NAC6
        {
            get { return _NAC6; }
            set { _NAC6 = value; }
        }
        public int NAC7
        {
            get { return _NAC7; }
            set { _NAC7 = value; }
        }
        public int NAC8
        {
            get { return _NAC8; }
            set { _NAC8 = value; }
        }
        public int NAC9
        {
            get { return _NAC9; }
            set { _NAC9 = value; }
        }
        public int NAC10
        {
            get { return _NAC10; }
            set { _NAC10 = value; }
        }
        public int NAC11
        {
            get { return _NAC11; }
            set { _NAC11 = value; }
        }
        public int NAC12
        {
            get { return _NAC12; }
            set { _NAC12 = value; }
        }

    }
}
