﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Len.Jakpro.ClassOpenAccess
{   
    public class IPPAList
    {
        private int _IPPAVLD;
        private int _IPPAEQU;
        private int _IPPAPLM;
        private int _IPPABLU;
        private int _IPPABLS;
        private int _IPPADPD;
                
        public int IPPAVLD
        {
            get { return _IPPAVLD; }
            set { _IPPAVLD = value; }
        }
        public int IPPAEQU
        {
            get { return _IPPAEQU; }
            set { _IPPAEQU = value; }
        }
        public int IPPAPLM
        {
            get { return _IPPAPLM; }
            set { _IPPAPLM = value; }
        }
        public int IPPABLU
        {
            get { return _IPPABLU; }
            set { _IPPABLU = value; }
        }
        public int IPPABLS
        {
            get { return _IPPABLS; }
            set { _IPPABLS = value; }
        }
        public int IPPADPD
        {
            get { return _IPPADPD; }
            set { _IPPADPD = value; }
        }
    }
}
