﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Len.Jakpro.ClassOpenAccess
{   
    public class DepoPA
    {
        private int DeviceStatePA1;
        private int DeviceStatePA2;
        private int DeviceStatePA3;
        private int DeviceStatePA4;

        private int DeviceStateNPA1;
        private int DeviceStateNPA2;
        private int DeviceStateNPA3;
        private int DeviceStateNPA4;
        private int DeviceStateNPA5;

        private int DeviceStatePF1;
        private int DeviceStatePF2;
        private int DeviceStatePF3;
        private int DeviceStatePF4;

        private int DeviceStatePAID1;
        private int DeviceStatePAID2;
        private int DeviceStatePAID3;
        private int DeviceStatePAID4;

        private int DeviceStateNPAID1;
        private int DeviceStateNPAID2;
        private int DeviceStateNPAID3;
        private int DeviceStateNPAID4;
        private int DeviceStateNPAID5;

        private int DeviceStatePFID1;
        private int DeviceStatePFID2;
        private int DeviceStatePFID3;
        private int DeviceStatePFID4;

        public int ObjPA1
        {
            get { return DeviceStatePA1; }
            set { DeviceStatePA1 = value; }
        }
        public int ObjPA2
        {
            get { return DeviceStatePA2; }
            set { DeviceStatePA2 = value; }
        }
        public int ObjPA3
        {
            get { return DeviceStatePA3; }
            set { DeviceStatePA3 = value; }
        }
        public int ObjPA4
        {
            get { return DeviceStatePA4; }
            set { DeviceStatePA4 = value; }
        }

        public int ObjNPA1
        {
            get { return DeviceStateNPA1; }
            set { DeviceStateNPA1 = value; }
        }
        public int ObjNPA2
        {
            get { return DeviceStateNPA2; }
            set { DeviceStateNPA2 = value; }
        }
        public int ObjNPA3
        {
            get { return DeviceStateNPA3; }
            set { DeviceStateNPA3 = value; }
        }
        public int ObjNPA4
        {
            get { return DeviceStateNPA4; }
            set { DeviceStateNPA4 = value; }
        }
        public int ObjNPA5
        {
            get { return DeviceStateNPA5; }
            set { DeviceStateNPA5 = value; }
        }

        public int ObjPF1
        {
            get { return DeviceStatePF1; }
            set { DeviceStatePF1 = value; }
        }
        public int ObjPF2
        {
            get { return DeviceStatePF2; }
            set { DeviceStatePF2 = value; }
        }
        public int ObjPF3
        {
            get { return DeviceStatePF3; }
            set { DeviceStatePF3 = value; }
        }
        public int ObjPF4
        {
            get { return DeviceStatePF4; }
            set { DeviceStatePF4 = value; }
        }

        public int ObjDevicePAID1
        {
            get { return DeviceStatePAID1; }
            set { DeviceStatePAID1 = value; }
        }
        public int ObjDevicePAID2
        {
            get { return DeviceStatePAID2; }
            set { DeviceStatePAID2 = value; }
        }
        public int ObjDevicePAID3
        {
            get { return DeviceStatePAID3; }
            set { DeviceStatePAID3 = value; }
        }
        public int ObjDevicePAID4
        {
            get { return DeviceStatePAID4; }
            set { DeviceStatePAID4 = value; }
        }

        public int ObjDeviceNPAID1
        {
            get { return DeviceStateNPAID1; }
            set { DeviceStateNPAID1 = value; }
        }
        public int ObjDeviceNPAID2
        {
            get { return DeviceStateNPAID2; }
            set { DeviceStateNPAID2 = value; }
        }
        public int ObjDeviceNPAID3
        {
            get { return DeviceStateNPAID3; }
            set { DeviceStateNPAID3 = value; }
        }
        public int ObjDeviceNPAID4
        {
            get { return DeviceStateNPAID4; }
            set { DeviceStateNPAID4 = value; }
        }
        public int ObjDeviceNPAID5
        {
            get { return DeviceStateNPAID5; }
            set { DeviceStateNPAID5 = value; }
        }

        public int ObjDevicePFID1
        {
            get { return DeviceStatePFID1; }
            set { DeviceStatePFID1 = value; }
        }
        public int ObjDevicePFID2
        {
            get { return DeviceStatePFID2; }
            set { DeviceStatePFID2 = value; }
        }
        public int ObjDevicePFID3
        {
            get { return DeviceStatePFID3; }
            set { DeviceStatePFID3 = value; }
        }
        public int ObjDevicePFID4
        {
            get { return DeviceStatePFID4; }
            set { DeviceStatePFID4 = value; }
        }
    }
}
