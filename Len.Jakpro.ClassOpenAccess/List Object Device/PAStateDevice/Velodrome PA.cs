﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;

namespace Len.Jakpro.ClassOpenAccess
{    
    public class VelodromePA
    {
        private int DeviceStatePA1;
        private int DeviceStatePA2;
        private int DeviceStatePA3;
        private int DeviceStatePA4;

        private int DeviceStateNPA1;
        private int DeviceStateNPA2;
        private int DeviceStateNPA3;
        private int DeviceStateNPA4;
        private int DeviceStateNPA5;

        private int DeviceStatePF1;
        private int DeviceStatePF2;
        private int DeviceStatePF3;

        private int DeviceStateAFIL1;
        private int DeviceStateAFIL2;

        public int ObjPA1
        {
            get { return DeviceStatePA1; }
            set { DeviceStatePA1 = value; }
        }
        public int ObjPA2
        {
            get { return DeviceStatePA2; }
            set { DeviceStatePA2 = value; }
        }
        public int ObjPA3
        {
            get { return DeviceStatePA3; }
            set { DeviceStatePA3 = value; }
        }
        public int ObjPA4
        {
            get { return DeviceStatePA4; }
            set { DeviceStatePA4 = value; }        
        }

        public int ObjNPA1
        {
            get { return DeviceStateNPA1; }
            set { DeviceStateNPA1 = value; }
        }
        public int ObjNPA2
        {
            get { return DeviceStateNPA2; }
            set { DeviceStateNPA2 = value; }
        }
        public int ObjNPA3
        {
            get { return DeviceStateNPA3; }
            set { DeviceStateNPA3 = value; }
        }
        public int ObjNPA4
        {
            get { return DeviceStateNPA4; }
            set { DeviceStateNPA4 = value; }
        }
        public int ObjNPA5
        {
            get { return DeviceStateNPA5; }
            set { DeviceStateNPA5 = value; }
        }

        public int ObjPF1
        {
            get { return DeviceStatePF1; }
            set { DeviceStatePF1 = value; }
        }
        public int ObjPF2
        {
            get { return DeviceStatePF2; }
            set { DeviceStatePF2 = value; }
        }
        public int ObjPF3
        {
            get { return DeviceStatePF3; }
            set { DeviceStatePF3 = value; }
        }

        public int ObjAFIL1
        {
            get { return DeviceStateAFIL1; }
            set { DeviceStateAFIL1 = value; }
        }
        public int ObjAFIL2
        {
            get { return DeviceStateAFIL2; }
            set { DeviceStateAFIL2 = value; }
        }



    }
}
