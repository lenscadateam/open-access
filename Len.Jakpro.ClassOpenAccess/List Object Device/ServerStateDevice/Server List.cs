﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Len.Jakpro.ClassOpenAccess
{
   
    public class ServerList
    {
        private int DeviceStateServer1;
        private int DeviceStateServer2;

        public int Server1
        {
            get { return DeviceStateServer1; }
            set { DeviceStateServer1 = value; }
        }
        public int Server2
        {
            get { return DeviceStateServer2; }
            set { DeviceStateServer2 = value; }
        }
        
    }
}
