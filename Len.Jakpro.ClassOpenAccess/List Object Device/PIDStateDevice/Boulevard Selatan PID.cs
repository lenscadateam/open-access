﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Len.Jakpro.ClassOpenAccess
{
   
    public class BoulevardSelatanPID
    {
        private int DeviceStatePID1;
        private int DeviceStatePID2;
        private int DeviceStatePID3;
        private int DeviceStatePID4;

        public int ObjPID1
        {
            get { return DeviceStatePID1; }
            set { DeviceStatePID1 = value; }
        }
        public int ObjPID2
        {
            get { return DeviceStatePID2; }
            set { DeviceStatePID2 = value; }
        }
        public int ObjPID3
        {
            get { return DeviceStatePID3; }
            set { DeviceStatePID3 = value; }
        }
        public int ObjPID4
        {
            get { return DeviceStatePID4; }
            set { DeviceStatePID4 = value; }        
        }
    }
}
