﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Len.Jakpro.ClassOpenAccess
{
   
    public class DepoDuaPHP
    {
        private int DeviceStatePHP1;
        private int DeviceStatePHP2;
        private int DeviceStatePHP3;
        private int DeviceStatePHP4;
        private int DeviceStatePHP5;
        private int DeviceStatePHP6;

        public int ObjPHP1
        {
            get { return DeviceStatePHP1; }
            set { DeviceStatePHP1 = value; }
        }
        public int ObjPHP2
        {
            get { return DeviceStatePHP2; }
            set { DeviceStatePHP2 = value; }
        }
        public int ObjPHP3
        {
            get { return DeviceStatePHP3; }
            set { DeviceStatePHP3 = value; }
        }
        public int ObjPHP4
        {
            get { return DeviceStatePHP4; }
            set { DeviceStatePHP4 = value; }
        }
        public int ObjPHP5
        {
            get { return DeviceStatePHP5; }
            set { DeviceStatePHP5 = value; }
        }
        public int ObjPHP6
        {
            get { return DeviceStatePHP6; }
            set { DeviceStatePHP6 = value; }
        }
    }
}
