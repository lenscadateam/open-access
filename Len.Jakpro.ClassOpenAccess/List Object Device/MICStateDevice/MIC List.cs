﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Len.Jakpro.ClassOpenAccess
{   
    public class MicList
    {
        private int _MICVLD;
        private int _MICEQU;
        private int _MICPLM;
        private int _MICBLU;
        private int _MICBLS;
        private int _MICDPD;

        private int _MICOCC;
        private int _MICBCC;

        public int MICVLD
        {
            get { return _MICVLD; }
            set { _MICVLD = value; }
        }
        public int MICEQU
        {
            get { return _MICEQU; }
            set { _MICEQU = value; }
        }
        public int MICPLM
        {
            get { return _MICPLM; }
            set { _MICPLM = value; }
        }
        public int MICBLU
        {
            get { return _MICBLU; }
            set { _MICBLU = value; }
        }
        public int MICBLS
        {
            get { return _MICBLS; }
            set { _MICBLS = value; }
        }
        public int MICDPD
        {
            get { return _MICDPD; }
            set { _MICDPD = value; }
        }
        public int MICOCC
        {
            get { return _MICOCC; }
            set { _MICOCC = value; }
        }
        public int MICBCC
        {
            get { return _MICBCC; }
            set { _MICBCC = value; }
        }
    }
}
