﻿using netspire;
using SimpleTCP;
using System;
using Len.Jakpro.Database.Rendundancy;
using System.Data.SqlClient;
using System.Collections.Generic;
using Len.Jakpro.OpenAccess.Logging;
using NLog;
namespace Len.Jakpro.ClassOpenAccess
{
    /// <summary>
    /// This is all information about OA Audioserver  
    /// </summary>
    public class AudioObserver : AudioServerObserver
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private static readonly string _Datetime = DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss : ");
        private List<string> _ListIPServer = new List<string>();
        private List<string> _ListIPServerCXS = new List<string>();
        private List<string> _ListIPPA = new List<string>();
        private List<string> _ListMIC = new List<string>();
        // List NAC
        private List<string> _ListNAC = new List<string>();
        // List PID
        private List<string> _ListIPVelodromePID = new List<string>();
        private List<string> _ListIPEquistrianPID = new List<string>();
        private List<string> _ListIPPulomasPID = new List<string>();
        private List<string> _ListIPBoulevardUtaraPID = new List<string>();
        private List<string> _ListIPBoulevardSelatanPID = new List<string>();
        private List<string> _ListIPDepoDuaPID = new List<string>();
        // List PHP
        private List<string> _ListIDPHPVLD = new List<string>();
        private List<string> _ListIDPHPEQU = new List<string>();
        private List<string> _ListIDPHPPLM = new List<string>();
        private List<string> _ListIDPHPBLU = new List<string>();
        private List<string> _ListIDPHPBLS = new List<string>();
        private List<string> _ListIDPHPDPD = new List<string>();

        private Connection _Con;
        private readonly string _IPServer = "127.0.0.1";
        private readonly int _Port = 1505;
        private IPPAList _ObjIPPA;
        private ServerList _ObjServer;
        // PHP Object
        private VelodromePHP _ObjVelodromePHP;
        private EquistrianPHP _ObjEquistrianPHP;
        private BoulevardSelatanPHP _ObjBoulevardSelatanPHP;
        private BoulevardUtaraPHP _ObjBoulevatdUtaraPHP;
        private PulomasPHP _ObjPulomasPHP;
        private DepoDuaPHP _ObjDepoPHP;
        // PID Object
        private VelodromePID _ObjVLDPID;
        private EquistrianPID _ObjEQUPID;
        private PulomasPID _ObjPLMPID;
        private BoulevardUtaraPID _ObjBLUPID;
        private BoulevardSelatanPID _ObjBLSPID;
        private DepoDuaPID _ObjDPDPID;
        // NAC Class
        private NACList _ObjNAC;
        public AudioObserver(
            IPPAList ObjIPPA,
            MicList ObjMIC,
            NACList ObjNAC,
            ServerList ObjServer,
            VelodromePHP ObjVLD,
            EquistrianPHP ObjEQU,
            BoulevardSelatanPHP ObjSLU,
            BoulevardUtaraPHP ObjBLU,
            PulomasPHP ObjPLM,
            DepoDuaPHP ObjDepo,
            VelodromePID ObjVLDPID,
            EquistrianPID ObjEQUPID,
            PulomasPID ObjPLMPID,
            BoulevardUtaraPID ObjBLUPID,
            BoulevardSelatanPID ObjBLSPID,
            DepoDuaPID ObjDPDPID
            )
        {
            _ObjIPPA = ObjIPPA;
            _ObjNAC = ObjNAC;
            _ObjServer = ObjServer;
            _ObjVelodromePHP = ObjVLD;
            _ObjEquistrianPHP = ObjEQU;
            _ObjBoulevardSelatanPHP = ObjSLU;
            _ObjBoulevatdUtaraPHP = ObjBLU;
            _ObjPulomasPHP = ObjPLM;
            _ObjDepoPHP = ObjDepo;

            _ObjVLDPID = ObjVLDPID;
            _ObjEQUPID = ObjEQUPID;
            _ObjPLMPID = ObjPLMPID;
            _ObjBLUPID = ObjBLUPID;
            _ObjBLSPID = ObjBLSPID;
            _ObjDPDPID = ObjDPDPID;
            var logging = new NLogConfigurator();
            logging.Configure();
        }
        public int GetState(string State)
        {
            if (State.Equals("IDLE"))
            {
                return 1;
            }
            else if (State.Equals("ALERTING"))
            {
                return 2;
            }
            else if (State.Equals("ACTIVE"))
            {
                return 3;
            }
            else if (State.Equals("FAULTY"))
            {
                return 4;
            }
            else if (State.Equals("COMMSFAULT"))
            {
                return 5;
            }
            else if(State.Equals("HELD"))
            {
                return 6;
            }
            else
            {
                return 10;
            }
        }
        public void SendPHPState(string variableName, int Value1, int Value2)
        {
            SimpleTcpClient _SendPHPState;
            try
            {

                _SendPHPState = new SimpleTcpClient().Connect(_IPServer, _Port);
                _SendPHPState.Write("" + variableName + "," +
                    Value1 + "," +
                    Value2 + ""
                    );
                _SendPHPState.Dispose();
            }
            catch (Exception e)
            {
                _logger.Error(_Datetime + e.Message.ToString());
            }

        }
        public void SendServerState(string variableName, int ServerIndex,int ServerValue)
        {
            try
            {
                SimpleTcpClient client;
                client = new SimpleTcpClient().Connect(_IPServer, _Port);
                client.Write("" + variableName + "," +
                    ServerIndex + ","+
                    ServerValue + ""
                    );
                client.Dispose();
            } 
            catch(Exception e)
            {
                _logger.Error(_Datetime + e.Message.ToString());
            }  
        }
        public void SendIPPAState(string variableName, int Value1, int value2)
        {
            try
            {
                SimpleTcpClient _IPPAClient;
                _IPPAClient = new SimpleTcpClient().Connect(_IPServer, _Port);
                _IPPAClient.Write("" + variableName + "," +
                    Value1 + "," +
                    value2 + ""
                    );
                _IPPAClient.Dispose();
            }
            catch (Exception e)
            {
                _logger.Error(_Datetime + e.Message.ToString());
            }
        }
        public void SendPIDState(string variableName, int Value1, int Value2)
        {
            SimpleTcpClient _SendPIDState;
            try
            {

                _SendPIDState = new SimpleTcpClient().Connect(_IPServer, _Port);
                _SendPIDState.Write("" + variableName + "," +
                    Value1 + "," +
                    Value2 + ""
                    );
                _SendPIDState.Dispose();
            }
            catch (Exception e)
            {
                _logger.Error(_Datetime + e.Message.ToString());
            }

        }
        public override void onDeviceStateChange(Device device)
        {
            _ListIPServer.Clear();
            _ListIPServerCXS.Clear();
            _ListIPPA.Clear();
            _ListNAC.Clear();

            _ListIPVelodromePID.Clear();
            _ListIPEquistrianPID.Clear();
            _ListIPPulomasPID.Clear();
            _ListIPBoulevardUtaraPID.Clear();
            _ListIPBoulevardSelatanPID.Clear();
            _ListIPDepoDuaPID.Clear();

            _ListIDPHPVLD.Clear();
            _ListIDPHPEQU.Clear();
            _ListIDPHPPLM.Clear();
            _ListIDPHPBLU.Clear();
            _ListIDPHPBLS.Clear();
            _ListIDPHPDPD.Clear();

            Device thisDevice = new Device(device);
            _Con = new Connection();
            _Con.Connected();
            _Con.SqlCmd = new SqlCommand
            {
                Connection = _Con.SqlCon,
                CommandType = System.Data.CommandType.StoredProcedure,
                CommandText = "OA_ExecuteConfiguration"
            };
            _Con.SqlRead = _Con.SqlCmd.ExecuteReader();
            while (_Con.SqlRead.Read())
            {
                // Get Configuration station and server From db
                if (_Con.SqlRead.GetValue(2).Equals("CXS"))
                {
                    _ListIPServerCXS.Add(_Con.SqlRead.GetValue(1).ToString());
                }
                if (_Con.SqlRead.GetValue(2).Equals("Server"))
                {
                    _ListIPServer.Add(_Con.SqlRead.GetValue(1).ToString());
                }
                if (_Con.SqlRead.GetValue(2).Equals("IPPA"))
                {
                    _ListIPPA.Add(_Con.SqlRead.GetValue(3).ToString());
                }
                if (_Con.SqlRead.GetValue(2).Equals("MIC"))
                {
                    _ListMIC.Add(_Con.SqlRead.GetValue(3).ToString());
                }
                if (_Con.SqlRead.GetValue(2).Equals("NAC"))
                {
                    _ListNAC.Add(_Con.SqlRead.GetValue(3).ToString());
                }
                if (_Con.SqlRead.GetValue(2).Equals("PID"))
                {
                    if (_Con.SqlRead.GetValue(0).Equals(2))
                    {
                        _ListIPVelodromePID.Add(_Con.SqlRead.GetValue(1).ToString());
                    }
                    else if (_Con.SqlRead.GetValue(0).Equals(3))
                    {
                        _ListIPEquistrianPID.Add(_Con.SqlRead.GetValue(1).ToString());
                    }
                    else if (_Con.SqlRead.GetValue(0).Equals(4))
                    {
                        _ListIPPulomasPID.Add(_Con.SqlRead.GetValue(1).ToString());
                    }
                    else if (_Con.SqlRead.GetValue(0).Equals(5))
                    {
                        _ListIPBoulevardUtaraPID.Add(_Con.SqlRead.GetValue(1).ToString());
                    }
                    else if (_Con.SqlRead.GetValue(0).Equals(6))
                    {
                        _ListIPBoulevardSelatanPID.Add(_Con.SqlRead.GetValue(1).ToString());
                    }
                    else if (_Con.SqlRead.GetValue(0).Equals(7))
                    {
                        _ListIPDepoDuaPID.Add(_Con.SqlRead.GetValue(1).ToString());
                    }
                }
                if (_Con.SqlRead.GetValue(2).Equals("PHP"))
                {
                    if (_Con.SqlRead.GetValue(0).Equals(2))
                    {
                        _ListIDPHPVLD.Add(_Con.SqlRead.GetValue(3).ToString());
                    }
                    else if (_Con.SqlRead.GetValue(0).Equals(3))
                    {
                        _ListIDPHPEQU.Add(_Con.SqlRead.GetValue(3).ToString());
                    }
                    else if (_Con.SqlRead.GetValue(0).Equals(4))
                    {
                        _ListIDPHPPLM.Add(_Con.SqlRead.GetValue(3).ToString());
                    }
                    else if (_Con.SqlRead.GetValue(0).Equals(5))
                    {
                        _ListIDPHPBLU.Add(_Con.SqlRead.GetValue(3).ToString());
                    }
                    else if (_Con.SqlRead.GetValue(0).Equals(6))
                    {
                        _ListIDPHPBLS.Add(_Con.SqlRead.GetValue(3).ToString());
                    }
                    else if (_Con.SqlRead.GetValue(0).Equals(7))
                    {
                        _ListIDPHPDPD.Add(_Con.SqlRead.GetValue(3).ToString());
                    }
                }
            }
            // IPPA List                
            if (thisDevice.getDstNo().ToString().Equals(_ListIPPA[0]))
            {
                _ObjIPPA.IPPAVLD = GetState(thisDevice.getState().ToString());
                SendIPPAState("IPPA.Status", 0, _ObjIPPA.IPPAVLD);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListIPPA[1]))
            {
                _ObjIPPA.IPPAEQU = GetState(thisDevice.getState().ToString());
                SendIPPAState("IPPA.Status", 1, _ObjIPPA.IPPAEQU);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListIPPA[2]))
            {
                _ObjIPPA.IPPAPLM = GetState(thisDevice.getState().ToString());
                SendIPPAState("IPPA.Status", 2, _ObjIPPA.IPPAPLM);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListIPPA[3]))
            {
                _ObjIPPA.IPPABLU = GetState(thisDevice.getState().ToString());
                SendIPPAState("IPPA.Status", 3, _ObjIPPA.IPPABLU);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListIPPA[4]))
            {
                _ObjIPPA.IPPABLS = GetState(thisDevice.getState().ToString());
                SendIPPAState("IPPA.Status", 4, _ObjIPPA.IPPABLS);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListIPPA[5]))
            {
                _ObjIPPA.IPPADPD = GetState(thisDevice.getState().ToString());
                SendIPPAState("IPPA.Status", 5, _ObjIPPA.IPPADPD);
            }
            // NAC List
            if (thisDevice.getDstNo().ToString().Equals(_ListNAC[0]))
            {
                _ObjNAC.NAC1 = GetState(thisDevice.getState().ToString());
                SendPHPState("NAC.Status", 0, _ObjNAC.NAC1);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListNAC[1]))
            {
                _ObjNAC.NAC2 = GetState(thisDevice.getState().ToString());
                SendPHPState("NAC.Status", 1, _ObjNAC.NAC2);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListNAC[2]))
            {
                _ObjNAC.NAC3 = GetState(thisDevice.getState().ToString());
                SendPHPState("NAC.Status", 2, _ObjNAC.NAC3);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListNAC[3]))
            {
                _ObjNAC.NAC4 = GetState(thisDevice.getState().ToString());
                SendPHPState("NAC.Status", 3, _ObjNAC.NAC4);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListNAC[4]))
            {
                _ObjNAC.NAC5 = GetState(thisDevice.getState().ToString());
                SendPHPState("NAC.Status", 4, _ObjNAC.NAC5);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListNAC[5]))
            {
                _ObjNAC.NAC6 = GetState(thisDevice.getState().ToString());
                SendPHPState("NAC.Status", 5, _ObjNAC.NAC6);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListNAC[6]))
            {
                _ObjNAC.NAC7 = GetState(thisDevice.getState().ToString());
                SendPHPState("NAC.Status", 6, _ObjNAC.NAC7);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListNAC[7]))
            {
                _ObjNAC.NAC8 = GetState(thisDevice.getState().ToString());
                SendPHPState("NAC.Status", 7, _ObjNAC.NAC8);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListNAC[8]))
            {
                _ObjNAC.NAC9 = GetState(thisDevice.getState().ToString());
                SendPHPState("NAC.Status", 8, _ObjNAC.NAC9);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListNAC[9]))
            {
                _ObjNAC.NAC10 = GetState(thisDevice.getState().ToString());
                SendPHPState("NAC.Status", 9, _ObjNAC.NAC10);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListNAC[10]))
            {
                _ObjNAC.NAC11 = GetState(thisDevice.getState().ToString());
                SendPHPState("NAC.Status", 10, _ObjNAC.NAC11);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListNAC[11]))
            {
                _ObjNAC.NAC12 = GetState(thisDevice.getState().ToString());
                SendPHPState("NAC.Status", 11, _ObjNAC.NAC12);
            }
            // Server List  CXS Server                      
            if (thisDevice.getIP().ToString().Equals(_ListIPServerCXS[0]))
            {
                _ObjServer.Server1 = GetState(thisDevice.getState().ToString());
                SendServerState("CXSServer", 0, _ObjServer.Server1);
            }
            if (thisDevice.getIP().ToString().Equals(_ListIPServerCXS[1]))
            {
                _ObjServer.Server2 = GetState(thisDevice.getState().ToString());
                SendServerState("CXSServer", 1, _ObjServer.Server2);
            }
            // PHP Velodrome
            if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPVLD[0]))
            {
                _ObjVelodromePHP.ObjPHP1 = GetState(thisDevice.getState().ToString());
                SendPHPState("PHPVLD", 0, _ObjVelodromePHP.ObjPHP1);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPVLD[1]))
            {
                _ObjVelodromePHP.ObjPHP2 = GetState(thisDevice.getState().ToString());
                SendPHPState("PHPVLD", 1, _ObjVelodromePHP.ObjPHP2);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPVLD[2]))
            {
                _ObjVelodromePHP.ObjPHP3 = GetState(thisDevice.getState().ToString());
                SendPHPState("PHPVLD", 2, _ObjVelodromePHP.ObjPHP3);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPVLD[3]))
            {
                _ObjVelodromePHP.ObjPHP4 = GetState(thisDevice.getState().ToString());
                SendPHPState("PHPVLD", 3, _ObjVelodromePHP.ObjPHP4);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPVLD[4]))
            {
                _ObjVelodromePHP.ObjPHP5 = GetState(thisDevice.getState().ToString());
                SendPHPState("PHPVLD", 4, _ObjVelodromePHP.ObjPHP5);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPVLD[5]))
            {
                _ObjVelodromePHP.ObjPHP6 = GetState(thisDevice.getState().ToString());
                SendPHPState("PHPVLD", 5, _ObjVelodromePHP.ObjPHP6);
            }
            // PHP Equistrian
            if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPEQU[0]))
            {
                _ObjEquistrianPHP.ObjPHP1 = GetState(thisDevice.getState().ToString());
                SendPHPState("PHPEQU", 0, _ObjEquistrianPHP.ObjPHP1);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPEQU[1]))
            {
                _ObjEquistrianPHP.ObjPHP2 = GetState(thisDevice.getState().ToString());
                SendPHPState("PHPEQU", 1, _ObjEquistrianPHP.ObjPHP2);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPEQU[2]))
            {
                _ObjEquistrianPHP.ObjPHP3 = GetState(thisDevice.getState().ToString());
                SendPHPState("PHPEQU", 2, _ObjEquistrianPHP.ObjPHP3);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPEQU[3]))
            {
                _ObjEquistrianPHP.ObjPHP4 = GetState(thisDevice.getState().ToString());
                SendPHPState("PHPEQU", 3, _ObjEquistrianPHP.ObjPHP4);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPEQU[4]))
            {
                _ObjEquistrianPHP.ObjPHP5 = GetState(thisDevice.getState().ToString());
                SendPHPState("PHPEQU", 4, _ObjEquistrianPHP.ObjPHP5);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPEQU[5]))
            {
                _ObjEquistrianPHP.ObjPHP6 = GetState(thisDevice.getState().ToString());
                SendPHPState("PHPEQU", 5, _ObjEquistrianPHP.ObjPHP6);
            }
            // PHP Pulomas
            if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPPLM[0]))
            {
                _ObjPulomasPHP.ObjPHP1 = GetState(thisDevice.getState().ToString());
                SendPHPState("PHPPLM", 0, _ObjPulomasPHP.ObjPHP1);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPPLM[1]))
            {
                _ObjPulomasPHP.ObjPHP2 = GetState(thisDevice.getState().ToString());
                SendPHPState("PHPPLM", 1, _ObjPulomasPHP.ObjPHP2);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPPLM[2]))
            {
                _ObjPulomasPHP.ObjPHP3 = GetState(thisDevice.getState().ToString());
                SendPHPState("PHPPLM", 2, _ObjPulomasPHP.ObjPHP3);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPPLM[3]))
            {
                _ObjPulomasPHP.ObjPHP4 = GetState(thisDevice.getState().ToString());
                SendPHPState("PHPPLM", 3, _ObjPulomasPHP.ObjPHP4);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPPLM[4]))
            {
                _ObjPulomasPHP.ObjPHP5 = GetState(thisDevice.getState().ToString());
                SendPHPState("PHPPLM", 4, _ObjPulomasPHP.ObjPHP5);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPPLM[5]))
            {
                _ObjPulomasPHP.ObjPHP6 = GetState(thisDevice.getState().ToString());
                SendPHPState("PHPPLM", 5, _ObjPulomasPHP.ObjPHP6);
            }
            // PHP Boulevard Raya Utara
            if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPBLU[0]))
            {
                _ObjBoulevatdUtaraPHP.ObjPHP1 = GetState(thisDevice.getState().ToString());
                SendPHPState("PHPBLU", 0, _ObjBoulevatdUtaraPHP.ObjPHP1);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPBLU[1]))
            {
                _ObjBoulevatdUtaraPHP.ObjPHP2 = GetState(thisDevice.getState().ToString());
                SendPHPState("PHPBLU", 1, _ObjBoulevatdUtaraPHP.ObjPHP2);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPBLU[2]))
            {
                _ObjBoulevatdUtaraPHP.ObjPHP3 = GetState(thisDevice.getState().ToString());
                SendPHPState("PHPBLU", 2, _ObjBoulevatdUtaraPHP.ObjPHP3);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPBLU[3]))
            {
                _ObjBoulevatdUtaraPHP.ObjPHP4 = GetState(thisDevice.getState().ToString());
                SendPHPState("PHPBLU", 3, _ObjBoulevatdUtaraPHP.ObjPHP4);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPBLU[4]))
            {
                _ObjBoulevatdUtaraPHP.ObjPHP5 = GetState(thisDevice.getState().ToString());
                SendPHPState("PHPBLU", 4, _ObjBoulevatdUtaraPHP.ObjPHP5);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPBLU[5]))
            {
                _ObjBoulevatdUtaraPHP.ObjPHP6 = GetState(thisDevice.getState().ToString());
                SendPHPState("PHPBLU", 5, _ObjBoulevatdUtaraPHP.ObjPHP6);
            }
            // PHP Boulevard Raya Selatan
            if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPBLS[0]))
            {
                _ObjBoulevardSelatanPHP.ObjPHP1 = GetState(thisDevice.getState().ToString());
                SendPHPState("PHPBLS", 0, _ObjBoulevardSelatanPHP.ObjPHP1);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPBLS[1]))
            {
                _ObjBoulevardSelatanPHP.ObjPHP2 = GetState(thisDevice.getState().ToString());
                SendPHPState("PHPBLS", 1, _ObjBoulevardSelatanPHP.ObjPHP2);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPBLS[2]))
            {
                _ObjBoulevardSelatanPHP.ObjPHP3 = GetState(thisDevice.getState().ToString());
                SendPHPState("PHPBLS", 2, _ObjBoulevardSelatanPHP.ObjPHP3);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPBLS[3]))
            {
                _ObjBoulevardSelatanPHP.ObjPHP4 = GetState(thisDevice.getState().ToString());
                SendPHPState("PHPBLS", 3, _ObjBoulevardSelatanPHP.ObjPHP4);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPBLS[4]))
            {
                _ObjBoulevardSelatanPHP.ObjPHP5 = GetState(thisDevice.getState().ToString());
                SendPHPState("PHPBLS", 4, _ObjBoulevardSelatanPHP.ObjPHP5);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPBLS[5]))
            {
                _ObjBoulevardSelatanPHP.ObjPHP6 = GetState(thisDevice.getState().ToString());
                SendPHPState("PHPBLS", 5, _ObjBoulevardSelatanPHP.ObjPHP6);
            }
            // PHP Depo
            if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPDPD[0]))
            {
                _ObjDepoPHP.ObjPHP1 = GetState(thisDevice.getState().ToString());
                SendPHPState("PHPDPD", 0, _ObjDepoPHP.ObjPHP1);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPDPD[1]))
            {
                _ObjDepoPHP.ObjPHP2 = GetState(thisDevice.getState().ToString());
                SendPHPState("PHPDPD", 1, _ObjDepoPHP.ObjPHP2);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPDPD[2]))
            {
                _ObjDepoPHP.ObjPHP3 = GetState(thisDevice.getState().ToString());
                SendPHPState("PHPDPD", 2, _ObjDepoPHP.ObjPHP3);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPDPD[3]))
            {
                _ObjDepoPHP.ObjPHP4 = GetState(thisDevice.getState().ToString());
                SendPHPState("PHPDPD", 3, _ObjDepoPHP.ObjPHP4);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPDPD[4]))
            {
                _ObjDepoPHP.ObjPHP5 = GetState(thisDevice.getState().ToString());
                SendPHPState("PHPDPD", 4, _ObjDepoPHP.ObjPHP5);
            }
            if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPDPD[5]))
            {
                _ObjDepoPHP.ObjPHP6 = GetState(thisDevice.getState().ToString());
                SendPHPState("PHPDPD", 5, _ObjDepoPHP.ObjPHP6);
            }
            // PID Velodrome
            if (thisDevice.getIP().ToString().Equals(_ListIPVelodromePID[0]))
            {
                _ObjVLDPID.ObjPID1 = GetState(thisDevice.getState().ToString());
                SendPIDState("PID.CNC", 0, _ObjVLDPID.ObjPID1);
            }
            if (thisDevice.getIP().ToString().Equals(_ListIPVelodromePID[1]))
            {
                _ObjVLDPID.ObjPID2 = GetState(thisDevice.getState().ToString());
                SendPIDState("PID.CNC", 1, _ObjVLDPID.ObjPID2);
            }
            if (thisDevice.getIP().ToString().Equals(_ListIPVelodromePID[2]))
            {
                _ObjVLDPID.ObjPID3 = GetState(thisDevice.getState().ToString());
                SendPIDState("PID.PFM", 0, _ObjVLDPID.ObjPID3);
            }
            if (thisDevice.getIP().ToString().Equals(_ListIPVelodromePID[3]))
            {
                _ObjVLDPID.ObjPID4 = GetState(thisDevice.getState().ToString());
                SendPIDState("PID.PFM", 1, _ObjVLDPID.ObjPID4);
            }
            // PID Equistrian 
            if (thisDevice.getIP().ToString().Equals(_ListIPEquistrianPID[0]))
            {
                _ObjEQUPID.ObjPID1 = GetState(thisDevice.getState().ToString());
                SendPIDState("PID.CNC", 2, _ObjEQUPID.ObjPID1);
            }
            if (thisDevice.getIP().ToString().Equals(_ListIPEquistrianPID[1]))
            {
                _ObjEQUPID.ObjPID2 = GetState(thisDevice.getState().ToString());
                SendPIDState("PID.CNC", 3, _ObjEQUPID.ObjPID2);
            }
            if (thisDevice.getIP().ToString().Equals(_ListIPEquistrianPID[2]))
            {
                _ObjEQUPID.ObjPID3 = GetState(thisDevice.getState().ToString());
                SendPIDState("PID.PFM", 2, _ObjEQUPID.ObjPID3);
            }
            if (thisDevice.getIP().ToString().Equals(_ListIPEquistrianPID[3]))
            {
                _ObjEQUPID.ObjPID4 = GetState(thisDevice.getState().ToString());
                SendPIDState("PID.PFM", 3, _ObjEQUPID.ObjPID4);
            }
            // PID Pulomas 
            if (thisDevice.getIP().ToString().Equals(_ListIPPulomasPID[0]))
            {
                _ObjPLMPID.ObjPID1 = GetState(thisDevice.getState().ToString());
                SendPIDState("PID.CNC", 4, _ObjPLMPID.ObjPID1);
            }
            if (thisDevice.getIP().ToString().Equals(_ListIPPulomasPID[1]))
            {
                _ObjPLMPID.ObjPID2 = GetState(thisDevice.getState().ToString());
                SendPIDState("PID.CNC", 5, _ObjPLMPID.ObjPID2);
            }
            if (thisDevice.getIP().ToString().Equals(_ListIPPulomasPID[2]))
            {
                _ObjPLMPID.ObjPID3 = GetState(thisDevice.getState().ToString());
                SendPIDState("PID.PFM", 4, _ObjPLMPID.ObjPID3);
            }
            if (thisDevice.getIP().ToString().Equals(_ListIPPulomasPID[3]))
            {
                _ObjPLMPID.ObjPID4 = GetState(thisDevice.getState().ToString());
                SendPIDState("PID.PFM", 5, _ObjPLMPID.ObjPID4);
            }
            // PID Boulevard Utara 
            if (thisDevice.getIP().ToString().Equals(_ListIPBoulevardUtaraPID[0]))
            {
                _ObjBLUPID.ObjPID1 = GetState(thisDevice.getState().ToString());
                SendPIDState("PID.CNC", 6, _ObjBLUPID.ObjPID1);
            }
            if (thisDevice.getIP().ToString().Equals(_ListIPBoulevardUtaraPID[1]))
            {
                _ObjBLUPID.ObjPID2 = GetState(thisDevice.getState().ToString());
                SendPIDState("PID.CNC", 7, _ObjBLUPID.ObjPID2);
            }
            if (thisDevice.getIP().ToString().Equals(_ListIPBoulevardUtaraPID[2]))
            {
                _ObjBLUPID.ObjPID3 = GetState(thisDevice.getState().ToString());
                SendPIDState("PID.PFM", 6, _ObjBLUPID.ObjPID3);
            }
            if (thisDevice.getIP().ToString().Equals(_ListIPBoulevardUtaraPID[3]))
            {
                _ObjBLUPID.ObjPID4 = GetState(thisDevice.getState().ToString());
                SendPIDState("PID.PFM", 7, _ObjBLUPID.ObjPID4);
            }
            // PID Boulevard Selatan 
            if (thisDevice.getIP().ToString().Equals(_ListIPBoulevardSelatanPID[0]))
            {
                _ObjBLSPID.ObjPID1 = GetState(thisDevice.getState().ToString());
                SendPIDState("PID.CNC", 8, _ObjBLSPID.ObjPID1);
            }
            if (thisDevice.getIP().ToString().Equals(_ListIPBoulevardSelatanPID[1]))
            {
                _ObjBLSPID.ObjPID2 = GetState(thisDevice.getState().ToString());
                SendPIDState("PID.CNC", 9, _ObjBLSPID.ObjPID2);
            }
            if (thisDevice.getIP().ToString().Equals(_ListIPBoulevardSelatanPID[2]))
            {
                _ObjBLSPID.ObjPID3 = GetState(thisDevice.getState().ToString());
                SendPIDState("PID.PFM", 8, _ObjBLSPID.ObjPID3);
            }
            if (thisDevice.getIP().ToString().Equals(_ListIPBoulevardSelatanPID[3]))
            {
                _ObjBLSPID.ObjPID4 = GetState(thisDevice.getState().ToString());
                SendPIDState("PID.PFM", 9, _ObjBLSPID.ObjPID4);
            }
            // PID Depo 
            if (thisDevice.getIP().ToString().Equals(_ListIPDepoDuaPID[0]))
            {
                _ObjDPDPID.ObjPID1 = GetState(thisDevice.getState().ToString());
                SendPIDState("PID.CNC", 10, _ObjDPDPID.ObjPID1);
            }
            if (thisDevice.getIP().ToString().Equals(_ListIPDepoDuaPID[1]))
            {
                _ObjDPDPID.ObjPID2 = GetState(thisDevice.getState().ToString());
                SendPIDState("PID.CNC", 11, _ObjDPDPID.ObjPID2);
            }
            if (thisDevice.getIP().ToString().Equals(_ListIPDepoDuaPID[2]))
            {
                _ObjDPDPID.ObjPID3 = GetState(thisDevice.getState().ToString());
                SendPIDState("PID.PFM", 10, _ObjDPDPID.ObjPID3);
            }
            if (thisDevice.getIP().ToString().Equals(_ListIPDepoDuaPID[3]))
            {
                _ObjDPDPID.ObjPID4 = GetState(thisDevice.getState().ToString());
                SendPIDState("PID.PFM", 11, _ObjDPDPID.ObjPID4);
            }
           
        }

    }
}
