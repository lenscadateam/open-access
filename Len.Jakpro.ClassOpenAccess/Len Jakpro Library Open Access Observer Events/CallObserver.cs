﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Len.Jakpro.Database.Rendundancy;
using Len.Jakpro.OpenAccess.Logging;
using netspire;
using NLog;
using SimpleTCP;
namespace Len.Jakpro.ClassOpenAccess
{
    /// <summary>
    /// This is all VOIP activity status 
    /// </summary>
    public class callObserverClass : CallControllerObserver
    {
        private string _IPServer="127.0.0.1";
        private int _Port=1505;
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private Connection _Con;
        private static readonly string _Datetime = DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss : ");
        private List<string> _ListHadwareID = new List<string>();
        private List<string> _ListHadwareCommunication = new List<string>();
        public int GetStateVOIP(string State)
        {
            if (State.Equals("PROGRESS"))
            {
                return 1;
            }
            else if (State.Equals("CONNECTED"))
            {
                return 2;
            }
            else if (State.Equals("HELD"))
            {
                return 3;
            }
            else if (State.Equals("DISCONNECTED"))
            {
                return 5;
            }
            else if (State.Equals("UNKNOWN"))
            {
                return 6;
            }
            else
            {
                return 10;
            }
        }
        public void SendVOIPState(string Communication)
        {
            try
            {
                SimpleTcpClient _VoipClient;
                _VoipClient = new SimpleTcpClient().Connect(_IPServer, _Port);
                _VoipClient.Write("" + Communication + ""
                    );
                _VoipClient.Dispose();
            }
            catch (Exception e)
            {
                _logger.Error(_Datetime + e.Message.ToString());
            }
        }
        public void GetTblListVOIP(string QueryProcedure, List<string> _ListHadwareID, List<string> _ListCOMM)
        {
            _Con = new Connection();
            _Con.Connected();
            _Con.SqlCmd = new SqlCommand
            {
                Connection = _Con.SqlCon,
                CommandType = CommandType.StoredProcedure,
                CommandText = QueryProcedure
            };
            try
            {
                _Con.SqlRead = _Con.SqlCmd.ExecuteReader();
                while (_Con.SqlRead.Read())
                {
                    _ListHadwareID.Add(_Con.SqlRead.GetValue(0).ToString());
                    _ListCOMM.Add(_Con.SqlRead.GetValue(1).ToString());
                }
            }
            catch (SqlException e)
            {
                _logger.Error(_Datetime + e.Message.ToString());
            }
        }
        public callObserverClass()
        {
            GetTblListVOIP("OA_GetDeviceVOIP", _ListHadwareID, _ListHadwareCommunication);
            var _logging = new NLogConfigurator();
            _logging.Configure();
        }           
        private void _GetCommunicationByID(string QueryProcedure,string DeviceID, string GetValue)
        {
            _Con = new Connection();
            _Con.Connected();
            _Con.SqlCmd = new SqlCommand
            {
                Connection = _Con.SqlCon,
                CommandType = CommandType.StoredProcedure,
                CommandText = QueryProcedure
            };
            try
            {
                _Con.SqlCmd.Parameters.AddWithValue("@HadwareID",DeviceID);
                _Con.SqlRead = _Con.SqlCmd.ExecuteReader();
                while (_Con.SqlRead.Read())
                {
                    GetValue = _Con.SqlRead.GetValue(0).ToString();
                }
            _Con.Disconnected();
            }
            catch (SqlException e)
            {
                _logger.Error(_Datetime + e.Message.ToString());
            }
        }
        public override void onCallUpdate(CallInfo _CallInfo)
        {
            CallInfo _Status = new CallInfo(_CallInfo);           
           if (_ListHadwareID.Contains(_Status.callAPartyId.ToString()))
           {
                _Con = new Connection();
                _Con.Connected();
                _Con.SqlCmd = new SqlCommand
                {
                    Connection = _Con.SqlCon,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "OA_GetCommunicationVoIP"
                };
                _Con.SqlCmd.Parameters.AddWithValue("@HadwareID", _Status.callAPartyId.ToString());
                _Con.SqlRead = _Con.SqlCmd.ExecuteReader();
                while (_Con.SqlRead.Read())
                {
                    SendVOIPState(_Con.SqlRead.GetString(1) + "," + GetStateVOIP(_Status.callStateText.ToString()));
                }
                _Con.Disconnected();
           }
           if (_ListHadwareID.Contains(_Status.callBPartyId.ToString()))
           {
                _Con = new Connection();
                _Con.Connected();
                _Con.SqlCmd = new SqlCommand
                {
                    Connection = _Con.SqlCon,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "OA_GetCommunicationVoIP"
                };
                _Con.SqlCmd.Parameters.AddWithValue("@HadwareID", _Status.callBPartyId.ToString());
                _Con.SqlRead = _Con.SqlCmd.ExecuteReader();
                while (_Con.SqlRead.Read())
                {
                    SendVOIPState(_Con.SqlRead.GetString(1) + "," + GetStateVOIP(_Status.callStateText.ToString()));
                }
                _Con.Disconnected();
           }
        }
    }
}
