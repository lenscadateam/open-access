﻿using netspire;
using System;
using SimpleTCP;
using System.Collections.Generic;
using Len.Jakpro.AddInSampleLibrary.Logging;
using Len.Jakpro.Database.Rendundancy;
using NLog;
using System.Data.SqlClient;
using Microsoft.Win32;
using System.Data;

namespace Len.Jakpro.ClassOpenAccess
{
    /// <summary>
    /// This is all information PA status 
    /// </summary>
    class PAObserverClass : PAControllerObserver
    {
        // PA
        private VelodromePA _ObjVelodromePA;
        private EquistrianPA _ObjEquistrianPA;
        private PulomasPA _ObjPulomasPA;
        private BoulevardUtaraPA _ObjBoulevardUtaraPA;
        private BoulevardSelatanPA _ObjBoulevardSelatanPA;
        private DepoPA _ObjDepoPA;
        // MIC
        private MicList _ObjMIC;
        private string _IPServer;
        private int _Port;
        // Database Connection
        private Connection _Con;
        // List Zone PA
        private List<Int32> _ListPAZoneVLD = new List<Int32>();
        private List<Int32> _ListPAZoneEQU = new List<Int32>();
        private List<Int32> _ListPAZonePLM = new List<Int32>();
        private List<Int32> _ListPAZoneBLU = new List<Int32>();
        private List<Int32> _ListPAZoneBLS = new List<Int32>();
        private List<Int32> _ListPAZoneDPD = new List<Int32>();
        // List Mic
        private List<Int32> _ListMic = new List<Int32>();

        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private static readonly string _Datetime = DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss : ");
        public void PAObserverSend(string IP, int Port)
        {
            _IPServer = IP;
            _Port = Port;
            _ObjVelodromePA = new VelodromePA();
            _ObjEquistrianPA = new EquistrianPA();
            _ObjPulomasPA = new PulomasPA();
            _ObjBoulevardUtaraPA = new BoulevardUtaraPA();
            _ObjBoulevardSelatanPA = new BoulevardSelatanPA();
            _ObjDepoPA = new DepoPA();
            // Mic
            _ObjMIC = new MicList();
            // Get Configuration PA
            GetDeviceList("OA_GetPAZoneVLD", _ListPAZoneVLD);
            GetDeviceList("OA_GetPAZoneEQU", _ListPAZoneEQU);
            GetDeviceList("OA_GetPAZonePLM", _ListPAZonePLM);
            GetDeviceList("OA_GetPAZoneBLU", _ListPAZoneBLU);
            GetDeviceList("OA_GetPAZoneBLS", _ListPAZoneBLS);
            GetDeviceList("OA_GetPAZoneDPD", _ListPAZoneDPD);
            GetDeviceList("OA_GetMicID", _ListMic);

            var logging = new NLogConfigurator();
            logging.Configure();            
        }
        public void GetDeviceList(string QueryProcedure, List<Int32> _TempVariables)
        {
            _Con = new Connection();
            _Con.Connected();
            _Con.SqlCmd = new SqlCommand
            {
                Connection = _Con.SqlCon,
                CommandType = CommandType.StoredProcedure,
                CommandText = QueryProcedure
            };
            try
            {
                _Con.SqlRead = _Con.SqlCmd.ExecuteReader();
                while (_Con.SqlRead.Read())
                {
                    _TempVariables.Add(_Con.SqlRead.GetInt32(0));
                }
            }
            catch (SqlException e)
            {
                _logger.Error(_Datetime + e.Message.ToString());
            }
        }
        public int GetStatePA(string State)
        {
            if (State.Equals("HEALTHY"))
            {
                return 1;
            }
            else if (State.Equals("UNKNOWN_HEALTHSTATE"))
            {
                return 2;
            }
            else if (State.Equals("MINOR_FAULT"))
            {
                return 3;
            }
            else if (State.Equals("MAJOR_FAULT"))
            {
                return 5;
            }
            else
            {
                return 10;
            }
        }
        public int GetStateMIC(string State)
        {
            if (State.Equals("HEALTHY"))
            {
                return 1;
            }
            else if (State.Equals("UNKNOWN_HEALTHSTATE"))
            {
                return 4;
            }
            else if (State.Equals("FAULTY"))
            {
                return 5;
            }
            else
            {
                return 10;
            }
        }
        public void SendPAState(string variableName, int Value1, int Value2)
        {
            SimpleTcpClient _SendPAState;
            try
            {

                _SendPAState = new SimpleTcpClient().Connect(_IPServer, _Port);
                _SendPAState.Write("" + variableName + "," +
                    Value1 + "," +
                    Value2 + ""
                    );
                _SendPAState.Dispose();
            }
            catch (Exception e)
            {
                _logger.Error(_Datetime + e.Message.ToString());
            }
        }
        //   Sink Audio Status
        public override void onPaSinkUpdate(PaSink sink)
        {
            PaSink _GetPASink = new PaSink(sink);
            if (_GetPASink.id.Equals(_ListPAZoneVLD[0]))
            {
                _ObjVelodromePA.ObjPA1 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.PA", 0, _ObjVelodromePA.ObjPA1);
            }
            if (_GetPASink.id.Equals(_ListPAZoneVLD[1]))
            {
                _ObjVelodromePA.ObjPA2 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.PA", 1, _ObjVelodromePA.ObjPA2);
            }
            if (_GetPASink.id.Equals(_ListPAZoneVLD[2]))
            {
                _ObjVelodromePA.ObjPA3 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.PA", 2, _ObjVelodromePA.ObjPA3);
            }
            if (_GetPASink.id.Equals(_ListPAZoneVLD[3]))
            {
                _ObjVelodromePA.ObjPA4 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.PA", 3, _ObjVelodromePA.ObjPA4);
            }

            // Equistrian
            if (_GetPASink.id.Equals(_ListPAZoneEQU[0]))
            {
                _ObjEquistrianPA.ObjPA1 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.PA", 4, _ObjEquistrianPA.ObjPA1);
            }
            if (_GetPASink.id.Equals(_ListPAZoneEQU[1]))
            {
                _ObjEquistrianPA.ObjPA2 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.PA", 5, _ObjEquistrianPA.ObjPA2);
            }
            if (_GetPASink.id.Equals(_ListPAZoneEQU[2]))
            {
                _ObjEquistrianPA.ObjPA3 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.PA", 6, _ObjEquistrianPA.ObjPA3);
            }
            if (_GetPASink.id.Equals(_ListPAZoneEQU[3]))
            {
                _ObjEquistrianPA.ObjPA4 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.PA", 7, _ObjEquistrianPA.ObjPA4);
            }
            if (_GetPASink.id.Equals(_ListPAZoneEQU[4]))
            {
                _ObjEquistrianPA.ObjPA5 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.PA", 8, _ObjEquistrianPA.ObjPA5);
            }
            // Pulomas
            if (_GetPASink.id.Equals(_ListPAZonePLM[0]))
            {
                _ObjPulomasPA.ObjPA1 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.PA", 9, _ObjPulomasPA.ObjPA1);
            }
            if (_GetPASink.id.Equals(_ListPAZonePLM[1]))
            {
                _ObjPulomasPA.ObjPA2 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.PA", 10, _ObjPulomasPA.ObjPA2);
            }
            if (_GetPASink.id.Equals(_ListPAZonePLM[2]))
            {
                _ObjPulomasPA.ObjPA3 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.PA", 11, _ObjPulomasPA.ObjPA3);
            }
            if (_GetPASink.id.Equals(_ListPAZonePLM[3]))
            {
                _ObjPulomasPA.ObjPA4 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.PA", 12, _ObjPulomasPA.ObjPA4);
            }
            // Boulevard Utara
            if (_GetPASink.id.Equals(_ListPAZoneBLU[0]))
            {
                _ObjBoulevardUtaraPA.ObjPA1 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.PA", 13, _ObjBoulevardUtaraPA.ObjPA1);
            }
            if (_GetPASink.id.Equals(_ListPAZoneBLU[1]))
            {
                _ObjBoulevardUtaraPA.ObjPA2 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.PA", 14, _ObjBoulevardUtaraPA.ObjPA2);
            }
            if (_GetPASink.id.Equals(_ListPAZoneBLU[2]))
            {
                _ObjBoulevardUtaraPA.ObjPA3 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.PA", 15, _ObjBoulevardUtaraPA.ObjPA3);
            }
            if (_GetPASink.id.Equals(_ListPAZoneBLU[3]))
            {
                _ObjBoulevardUtaraPA.ObjPA4 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.PA", 16, _ObjBoulevardUtaraPA.ObjPA4);
            }
            // Boulevard Selatan
            if (_GetPASink.id.Equals(_ListPAZoneBLS[0]))
            {
                _ObjBoulevardSelatanPA.ObjPA1 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.PA", 17, _ObjBoulevardSelatanPA.ObjPA1);
            }
            if (_GetPASink.id.Equals(_ListPAZoneBLS[1]))
            {
                _ObjBoulevardSelatanPA.ObjPA2 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.PA", 18, _ObjBoulevardSelatanPA.ObjPA2);
            }
            if (_GetPASink.id.Equals(_ListPAZoneBLS[2]))
            {
                _ObjBoulevardSelatanPA.ObjPA3 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.PA", 19, _ObjBoulevardSelatanPA.ObjPA3);
            }

            // Non Public Area
            //Velodrome
            if (_GetPASink.id.Equals(_ListPAZoneVLD[4]))
            {
                _ObjVelodromePA.ObjNPA1 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.NPA", 0, _ObjVelodromePA.ObjNPA1);
            }
            if (_GetPASink.id.Equals(_ListPAZoneVLD[5]))
            {
                _ObjVelodromePA.ObjNPA2 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.NPA", 1, _ObjVelodromePA.ObjNPA2);
            }
            if (_GetPASink.id.Equals(_ListPAZoneVLD[6]))
            {
                _ObjVelodromePA.ObjNPA3 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.NPA", 2, _ObjVelodromePA.ObjNPA3);
            }
            if (_GetPASink.id.Equals(_ListPAZoneVLD[7]))
            {
                _ObjVelodromePA.ObjNPA4 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.NPA", 3, _ObjVelodromePA.ObjNPA4);
            }
            if (_GetPASink.id.Equals(_ListPAZoneVLD[8]))
            {
                _ObjVelodromePA.ObjNPA5 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.NPA", 4, _ObjVelodromePA.ObjNPA5);
            }
            // Equistrian
            if (_GetPASink.id.Equals(_ListPAZoneEQU[5]))
            {
                _ObjEquistrianPA.ObjNPA1 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.NPA", 5, _ObjEquistrianPA.ObjNPA1);
            }
            if (_GetPASink.id.Equals(_ListPAZoneEQU[6]))
            {
                _ObjEquistrianPA.ObjNPA2 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.NPA", 6, _ObjEquistrianPA.ObjNPA2);
            }
            if (_GetPASink.id.Equals(_ListPAZoneEQU[7]))
            {
                _ObjEquistrianPA.ObjNPA3 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.NPA", 7, _ObjEquistrianPA.ObjNPA3);
            }
            if (_GetPASink.id.Equals(_ListPAZoneEQU[8]))
            {
                _ObjEquistrianPA.ObjNPA4 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.NPA", 8, _ObjEquistrianPA.ObjNPA4);
            }
            if (_GetPASink.id.Equals(_ListPAZoneEQU[9]))
            {
                _ObjEquistrianPA.ObjNPA5 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.NPA", 9, _ObjEquistrianPA.ObjNPA5);
            }
            // Pulomas
            if (_GetPASink.id.Equals(_ListPAZonePLM[4]))
            {
                _ObjPulomasPA.ObjNPA1 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.NPA", 10, _ObjPulomasPA.ObjNPA1);
            }
            if (_GetPASink.id.Equals(_ListPAZonePLM[5]))
            {
                _ObjPulomasPA.ObjNPA2 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.NPA", 11, _ObjPulomasPA.ObjNPA2);
            }
            if (_GetPASink.id.Equals(_ListPAZonePLM[6]))
            {
                _ObjPulomasPA.ObjNPA3 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.NPA", 12, _ObjPulomasPA.ObjNPA3);
            }
            if (_GetPASink.id.Equals(_ListPAZonePLM[7]))
            {
                _ObjPulomasPA.ObjNPA4 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.NPA", 13, _ObjPulomasPA.ObjNPA4);
            }
            if (_GetPASink.id.Equals(_ListPAZonePLM[8]))
            {
                _ObjPulomasPA.ObjNPA5 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.NPA", 14, _ObjPulomasPA.ObjNPA5);
            }
            if (_GetPASink.id.Equals(_ListPAZonePLM[9]))
            {
                _ObjPulomasPA.ObjNPA6 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.NPA", 15, _ObjPulomasPA.ObjNPA6);
            }
            // Boulevard Utara
            if (_GetPASink.id.Equals(_ListPAZoneBLU[4]))
            {
                _ObjBoulevardUtaraPA.ObjNPA1 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.NPA", 16, _ObjBoulevardUtaraPA.ObjNPA1);
            }
            if (_GetPASink.id.Equals(_ListPAZoneBLU[5]))
            {
                _ObjBoulevardUtaraPA.ObjNPA2 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.NPA", 17, _ObjBoulevardUtaraPA.ObjNPA2);
            }
            if (_GetPASink.id.Equals(_ListPAZoneBLU[6]))
            {
                _ObjBoulevardUtaraPA.ObjNPA3 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.NPA", 18, _ObjBoulevardUtaraPA.ObjNPA3);
            }
            if (_GetPASink.id.Equals(_ListPAZoneBLU[7]))
            {
                _ObjBoulevardUtaraPA.ObjNPA4 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.NPA", 19, _ObjBoulevardUtaraPA.ObjNPA4);
            }
            if (_GetPASink.id.Equals(_ListPAZoneBLU[8]))
            {
                _ObjBoulevardUtaraPA.ObjNPA5 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.NPA", 20, _ObjBoulevardUtaraPA.ObjNPA5);
            }
            // Boulevard Selatan
            if (_GetPASink.id.Equals(_ListPAZoneBLS[3]))
            {
                _ObjBoulevardSelatanPA.ObjNPA1 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.NPA", 21, _ObjBoulevardSelatanPA.ObjNPA1);
            }
            if (_GetPASink.id.Equals(_ListPAZoneBLS[4]))
            {
                _ObjBoulevardSelatanPA.ObjNPA2 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.NPA", 22, _ObjBoulevardSelatanPA.ObjNPA2);
            }
            if (_GetPASink.id.Equals(_ListPAZoneBLS[5]))
            {
                _ObjBoulevardSelatanPA.ObjNPA3 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.NPA", 23, _ObjBoulevardSelatanPA.ObjNPA3);
            }
            if (_GetPASink.id.Equals(_ListPAZoneBLS[6]))
            {
                _ObjBoulevardSelatanPA.ObjNPA4 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.NPA", 24, _ObjBoulevardSelatanPA.ObjNPA4);
            }
            if (_GetPASink.id.Equals(_ListPAZoneBLS[7]))
            {
                _ObjBoulevardSelatanPA.ObjNPA5 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.NPA", 25, _ObjBoulevardSelatanPA.ObjNPA5);
            }
            if (_GetPASink.id.Equals(_ListPAZoneBLS[8]))
            {
                _ObjBoulevardSelatanPA.ObjNPA6 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.NPA", 26, _ObjBoulevardSelatanPA.ObjNPA6);
            }

            // Platform
            //Velodrome
            if (_GetPASink.id.Equals(_ListPAZoneVLD[9]))
            {
                _ObjVelodromePA.ObjPF1 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.PFM", 0, _ObjVelodromePA.ObjPF1);
            }
            if (_GetPASink.id.Equals(_ListPAZoneVLD[10]))
            {
                _ObjVelodromePA.ObjPF2 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.PFM", 1, _ObjVelodromePA.ObjPF2);
            }
            if (_GetPASink.id.Equals(_ListPAZoneVLD[11]))
            {
                _ObjVelodromePA.ObjPF3 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.PFM", 2, _ObjVelodromePA.ObjPF3);
            }
            //Equistrian
            if (_GetPASink.id.Equals(_ListPAZoneEQU[10]))
            {
                _ObjEquistrianPA.ObjPF1 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.PFM", 3, _ObjEquistrianPA.ObjPF1);
            }
            if (_GetPASink.id.Equals(_ListPAZoneEQU[11]))
            {
                _ObjEquistrianPA.ObjPF2 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.PFM", 4, _ObjEquistrianPA.ObjPF2);
            }
            //Pulomas
            if (_GetPASink.id.Equals(_ListPAZonePLM[10]))
            {
                _ObjPulomasPA.ObjPF1 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.PFM", 5, _ObjPulomasPA.ObjPF1);
            }
            if (_GetPASink.id.Equals(_ListPAZonePLM[11]))
            {
                _ObjPulomasPA.ObjPF2 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.PFM", 6, _ObjPulomasPA.ObjPF2);
            }

            ////Boulevard Utara
            if (_GetPASink.id.Equals(_ListPAZoneBLU[9]))
            {
                _ObjBoulevardUtaraPA.ObjPF1 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.PFM", 7, _ObjBoulevardUtaraPA.ObjPF1);
            }
            if (_GetPASink.id.Equals(_ListPAZoneBLU[10]))
            {
                _ObjBoulevardUtaraPA.ObjPF2 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.PFM", 8, _ObjBoulevardUtaraPA.ObjPF2);
            }
            if (_GetPASink.id.Equals(_ListPAZoneBLU[11]))
            {
                _ObjBoulevardUtaraPA.ObjPF3 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.PFM", 9, _ObjBoulevardUtaraPA.ObjPF3);
            }
            //Boulevard Selatan
            if (_GetPASink.id.Equals(_ListPAZoneBLS[9]))
            {
                _ObjBoulevardSelatanPA.ObjPF1 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.PFM", 10, _ObjBoulevardSelatanPA.ObjPF1);
            }
            if (_GetPASink.id.Equals(_ListPAZoneBLS[10]))
            {
                _ObjBoulevardSelatanPA.ObjPF2 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.PFM", 11, _ObjBoulevardSelatanPA.ObjPF2);
            }
            if (_GetPASink.id.Equals(_ListPAZoneBLS[11]))
            {
                _ObjBoulevardSelatanPA.ObjPF3 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.PFM", 12, _ObjBoulevardSelatanPA.ObjPF3);
            }
            // AFIL
            // Velodrome
            if (_GetPASink.id.Equals(_ListPAZoneVLD[12]))
            {
                _ObjVelodromePA.ObjAFIL1 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.AFIL", 0, _ObjVelodromePA.ObjAFIL1);
            }
            if (_GetPASink.id.Equals(_ListPAZoneVLD[13]))
            {
                _ObjVelodromePA.ObjAFIL2 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.AFIL", 1, _ObjVelodromePA.ObjAFIL2);
            }
            // Equistrian
            if (_GetPASink.id.Equals(_ListPAZoneEQU[12]))
            {
                _ObjEquistrianPA.ObjAFIL1 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.AFIL", 2, _ObjEquistrianPA.ObjAFIL1);
            }
            if (_GetPASink.id.Equals(_ListPAZoneEQU[13]))
            {
                _ObjEquistrianPA.ObjAFIL2 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.AFIL", 3, _ObjEquistrianPA.ObjAFIL2);
            }
            // Pulomas
            if (_GetPASink.id.Equals(_ListPAZonePLM[12]))
            {
                _ObjPulomasPA.ObjAFIL1 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.AFIL", 4, _ObjPulomasPA.ObjAFIL1);
            }
            if (_GetPASink.id.Equals(_ListPAZonePLM[13]))
            {
                _ObjPulomasPA.ObjAFIL2 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.AFIL", 5, _ObjPulomasPA.ObjAFIL2);
            }
            // Boulevard Utara
            if (_GetPASink.id.Equals(_ListPAZoneBLU[12]))
            {
                _ObjBoulevardUtaraPA.ObjAFIL1 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.AFIL", 6, _ObjBoulevardUtaraPA.ObjAFIL1);
            }
            if (_GetPASink.id.Equals(_ListPAZoneBLU[13]))
            {
                _ObjBoulevardUtaraPA.ObjAFIL2 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.AFIL", 7, _ObjBoulevardUtaraPA.ObjAFIL2);
            }
            // Boulevard Selatan
            if (_GetPASink.id.Equals(_ListPAZoneBLS[12]))
            {
                _ObjBoulevardSelatanPA.ObjAFIL1 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.AFIL", 8, _ObjBoulevardSelatanPA.ObjAFIL1);
            }
            if (_GetPASink.id.Equals(_ListPAZoneBLS[13]))
            {
                _ObjBoulevardSelatanPA.ObjAFIL2 = GetStatePA(_GetPASink.healthState.ToString());
                SendPAState("PA.AFIL", 9, _ObjBoulevardSelatanPA.ObjAFIL2);
            }
        }
        // Source Microphone Status
        public override void onPaSourceUpdate(PaSource _DeviceSource)
        {
            PaSource _PASource = new PaSource(_DeviceSource);
            if (_PASource.id.Equals(_ListMic[0]))
            {
                _ObjMIC.MICVLD = GetStateMIC(_PASource.healthState.ToString());
                SendPAState("IPPA.Status.Mic", 0, _ObjMIC.MICVLD);
            }
            if (_PASource.id.Equals(_ListMic[1]))
            {
                _ObjMIC.MICEQU = GetStateMIC(_PASource.healthState.ToString());
                SendPAState("IPPA.Status.Mic", 1, _ObjMIC.MICEQU);
            }
            if (_PASource.id.Equals(_ListMic[2]))
            {
                _ObjMIC.MICPLM = GetStateMIC(_PASource.healthState.ToString());
                SendPAState("IPPA.Status.Mic", 2, _ObjMIC.MICPLM);
            }
            if (_PASource.id.Equals(_ListMic[3]))
            {
                _ObjMIC.MICBLU = GetStateMIC(_PASource.healthState.ToString());
                SendPAState("IPPA.Status.Mic", 3, _ObjMIC.MICBLU);
            }
            if (_PASource.id.Equals(_ListMic[4]))
            {
                _ObjMIC.MICBLS = GetStateMIC(_PASource.healthState.ToString());
                SendPAState("IPPA.Status.Mic", 4, _ObjMIC.MICBLS);
            }
            if (_PASource.id.Equals(_ListMic[5]))
            {
                _ObjMIC.MICDPD = GetStateMIC(_PASource.healthState.ToString());
                SendPAState("IPPA.Status.Mic", 5, _ObjMIC.MICDPD);
            }
            if (_PASource.id.Equals(_ListMic[6]))
            {
                _ObjMIC.MICOCC = GetStateMIC(_PASource.healthState.ToString());
                SendPAState("IPPA.Status.Mic", 6, _ObjMIC.MICOCC);
            }
            if (_PASource.id.Equals(_ListMic[7]))
            {
                _ObjMIC.MICBCC = GetStateMIC(_PASource.healthState.ToString());
                SendPAState("IPPA.Status.Mic", 7, _ObjMIC.MICBCC);
            }
            if (_PASource.state.ToString()=="ATTACHED")
            {
                Console.WriteLine(DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss :") +"Source ID :"+ _PASource.id+" Status: ATTACHED!");
            }
          }
        // Zone Status
        public override void onPaZoneUpdate(PaZone paZone)
        {
            PaZone TempPAZone = new PaZone(paZone);
            if (TempPAZone.activityText == "Active" )
            {
                Console.WriteLine(DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss :")+"ZoneID : " + TempPAZone.id + " Active "+ TempPAZone.announcementTypeText.ToString());
            }
        }                                                           
    }
}
