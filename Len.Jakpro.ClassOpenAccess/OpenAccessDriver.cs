﻿using System;
using netspire;
using System.Threading;
using System.Linq;
using System.Collections.Generic;
using System.Timers;
using Len.Jakpro.Database.Rendundancy;
using SimpleTCP;
using System.Data.SqlClient;
using System.Data;
using NLog;
using Len.Jakpro.OpenAccess.Logging;

namespace Len.Jakpro.ClassOpenAccess
{
    public class OpenAccessDriver
    {
        public DisplayVariableValueMap PIDDisplay { get; set; }
        // Netspire AudioServer and PA server
        private AudioServer _AudioServer = null;
        private AudioObserver _AudioServerObserver=null;
        private PAObserverClass _PAControllerObserver=null;
        private callObserverClass _CallControllerObserver=null;
        // Database Connection
        private Connection _Con;
        // Temporary from database
        private List<string> _ListIPServerCXS = new List<string>();
        private List<string> _ListIPServer = new List<string>();
        private List<string> _ListIPPA = new List<string>();
        private List<Int32> _ListMIC = new List<Int32>();
        // List NAC
        private List<string> _ListNAC = new List<string>();
        // List PID
        private List<string> _ListIPPIDVLD = new List<string>();
        private List<string> _ListIPPIDEQU = new List<string>();
        private List<string> _ListIPPIDPLM = new List<string>();
        private List<string> _ListIPPIDBLU = new List<string>();
        private List<string> _ListIPPIDBLS = new List<string>();
        private List<string> _ListIPPIDDPD = new List<string>();
        // List VOIP
        private List<string> _ListIPVOIPVLD = new List<string>();
        private List<string> _ListIPVOIPEQU = new List<string>();
        private List<string> _ListIPVOIPPLM = new List<string>();
        private List<string> _ListIPVOIPBLU = new List<string>();
        private List<string> _ListIPVOIPBLS = new List<string>();
        private List<string> _ListIPVOIPDPD = new List<string>();
        // List PHP
        private List<string> _ListIDPHPVLD = new List<string>();
        private List<string> _ListIDPHPEQU = new List<string>();
        private List<string> _ListIDPHPPLM = new List<string>();
        private List<string> _ListIDPHPBLU = new List<string>();
        private List<string> _ListIDPHPBLS = new List<string>();
        private List<string> _ListIDPHPDPD = new List<string>();
        // List Zone PA
        private List<Int32> _ListPAZoneVLD = new List<Int32>();
        private List<Int32> _ListPAZoneEQU = new List<Int32>();
        private List<Int32> _ListPAZonePLM = new List<Int32>();
        private List<Int32> _ListPAZoneBLU = new List<Int32>();
        private List<Int32> _ListPAZoneBLS = new List<Int32>();
        private List<Int32> _ListPAZoneDPD = new List<Int32>();
        // Variable for Activate Annoucements
        private int _mySourceId = 0, _mySWTriggerId = 0;
        // Send to HMI Server
        private string _IPServer;
        private int _Port;
        private string _TemplateTmp;
        // Log data
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        // Dictionary Key PID 
        private static Dictionary<string, string> _DetailTemplate = new Dictionary<string, string>();
        // IPPA Class
        private IPPAList _ObjIPPA;
        private MicList _ObjMIC;
        // PA Class
        private VelodromePA _ObjVelodromePA;  
        private EquistrianPA _ObjEquistrianPA;
        private PulomasPA _ObjPulomasPA;
        private BoulevardUtaraPA _ObjBoulevardUtaraPA;
        private BoulevardSelatanPA _ObjBoulevardSelatanPA;
        private DepoPA _ObjDepoPA;
        // Server Class
        private ServerList _ObjServer;
        // NAC Class
        private NACList _ObjNAC;
        // PHP Class
        private VelodromePHP _ObjVelodromePHP;
        private EquistrianPHP _ObjEquistrianPHP;
        private BoulevardSelatanPHP _ObjBoulevardSelatanPHP;
        private BoulevardUtaraPHP _ObjBoulevatdUtaraPHP;
        private PulomasPHP _ObjPulomasPHP;
        private DepoDuaPHP _ObjDepoPHP;
        // PID Class
        private VelodromePID _ObjVLDPID;
        private EquistrianPID _ObjEQUPID;
        private PulomasPID _ObjPLMPID;
        private BoulevardUtaraPID _ObjBLUPID;
        private BoulevardSelatanPID _ObjBLSPID;
        private DepoDuaPID _ObjDPDPID;
        // VOIP Class
        private VelodromeVOIP _ObjVLDVOIP;
        private EquistrianVOIP _ObjEQUVOIP;
        private PulomasVOIP _ObjPLMVOIP;
        private BoulevardUtaraVOIP _ObjBLUVOIP;
        private BoulevardSelatanVOIP _ObjBLSVOIP;
        private DepotVOIP _ObjDPDVOIP;

        private System.Timers.Timer _countDownTimer;
        private static readonly string _Datetime = DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss : ");
        /// <summary>
        /// Construct OA Driver
        /// </summary>
        public OpenAccessDriver()
        {
            _ObjIPPA = new IPPAList();
            _ObjMIC = new MicList();
            _ObjServer = new ServerList();
            _ObjNAC = new NACList();
            _ObjVelodromePA = new VelodromePA();
            _ObjEquistrianPA = new EquistrianPA();
            _ObjPulomasPA = new PulomasPA();
            _ObjBoulevardUtaraPA = new BoulevardUtaraPA();
            _ObjBoulevardSelatanPA = new BoulevardSelatanPA();
            _ObjDepoPA = new DepoPA();

            _ObjVelodromePHP = new VelodromePHP();
            _ObjEquistrianPHP = new EquistrianPHP();
            _ObjBoulevardSelatanPHP = new BoulevardSelatanPHP();
            _ObjBoulevatdUtaraPHP = new BoulevardUtaraPHP();
            _ObjPulomasPHP = new PulomasPHP();
            _ObjDepoPHP = new DepoDuaPHP();

            _ObjVLDPID = new VelodromePID();
            _ObjEQUPID = new EquistrianPID();
            _ObjPLMPID = new PulomasPID();
            _ObjBLUPID = new BoulevardUtaraPID();
            _ObjBLSPID = new BoulevardSelatanPID();
            _ObjDPDPID = new DepoDuaPID();

            _ObjVLDVOIP = new VelodromeVOIP();
            _ObjEQUVOIP = new EquistrianVOIP();
            _ObjPLMVOIP = new PulomasVOIP();
            _ObjBLUVOIP = new BoulevardUtaraVOIP();
            _ObjBLSVOIP = new BoulevardSelatanVOIP();
            _ObjDPDVOIP = new DepotVOIP();

            GetTblCommunication();
            GetDeviceList("OA_GetPAZoneVLD", _ListPAZoneVLD);
            GetDeviceList("OA_GetPAZoneEQU", _ListPAZoneEQU);
            GetDeviceList("OA_GetPAZonePLM", _ListPAZonePLM);
            GetDeviceList("OA_GetPAZoneBLU", _ListPAZoneBLU);
            GetDeviceList("OA_GetPAZoneBLS", _ListPAZoneBLS);
            GetDeviceList("OA_GetPAZoneDPD", _ListPAZoneDPD);

            var logging = new NLogConfigurator();
            logging.Configure();
            _countDownTimer = new System.Timers.Timer
            {
                Interval = 5000 // 20 second
            };
            _countDownTimer.Elapsed += _countDownTimer_ElapsedSIG;
            _countDownTimer.AutoReset = true;           
        }
        /// <summary>
        /// PA Service From Signalling
        /// </summary>
        public void StartSend(string IP, int Port)
        {
            _IPServer = IP;
            _Port = Port;
        }
        public void GetTblCommunication()
        {
            _ListIPServer.Clear();
            _ListIPServerCXS.Clear();
            _ListIPPA.Clear();
            _ListMIC.Clear();
            _ListNAC.Clear();
            //PID
            _ListIPPIDVLD.Clear();
            _ListIPPIDEQU.Clear();
            _ListIPPIDPLM.Clear();
            _ListIPPIDBLU.Clear();
            _ListIPPIDBLS.Clear();
            _ListIPPIDDPD.Clear();
            //PHP
            _ListIDPHPVLD.Clear();
            _ListIDPHPEQU.Clear();
            _ListIDPHPPLM.Clear();
            _ListIDPHPBLU.Clear();
            _ListIDPHPBLS.Clear();
            _ListIDPHPDPD.Clear();
            //VOIP
            _ListIPVOIPVLD.Clear();
            _ListIPVOIPEQU.Clear();
            _ListIPVOIPPLM.Clear();
            _ListIPVOIPBLU.Clear();
            _ListIPVOIPBLS.Clear();
            _ListIPVOIPDPD.Clear();

            _Con = new Connection();
            _Con.Connected();
            _Con.SqlCmd = new SqlCommand
            {
                Connection = _Con.SqlCon,
                CommandType = CommandType.StoredProcedure,
                CommandText = "OA_ExecuteConfiguration"
            };
            try
            {
                _Con.SqlRead = _Con.SqlCmd.ExecuteReader();
                while (_Con.SqlRead.Read())
                {
                    // Get Configuration station and server From db
                    if (_Con.SqlRead.GetValue(2).Equals("CXS"))
                    {
                        _ListIPServerCXS.Add(_Con.SqlRead.GetValue(1).ToString());
                    }
                    if (_Con.SqlRead.GetValue(2).Equals("Server"))
                    {
                        _ListIPServer.Add(_Con.SqlRead.GetValue(1).ToString());
                    }
                    if (_Con.SqlRead.GetValue(2).Equals("IPPA"))
                    {
                        _ListIPPA.Add(_Con.SqlRead.GetValue(3).ToString());
                    }
                    if (_Con.SqlRead.GetValue(2).Equals("MIC"))
                    {
                        _ListMIC.Add(_Con.SqlRead.GetInt32(3));
                    }
                    if (_Con.SqlRead.GetValue(2).Equals("NAC"))
                    {
                        _ListNAC.Add(_Con.SqlRead.GetValue(3).ToString());
                    }
                    if (_Con.SqlRead.GetValue(2).Equals("PID"))
                    {
                        if (_Con.SqlRead.GetValue(0).Equals(2))
                        {
                            _ListIPPIDVLD.Add(_Con.SqlRead.GetValue(1).ToString());
                        }
                        else if (_Con.SqlRead.GetValue(0).Equals(3))
                        {
                            _ListIPPIDEQU.Add(_Con.SqlRead.GetValue(1).ToString());
                        }
                        else if (_Con.SqlRead.GetValue(0).Equals(4))
                        {
                            _ListIPPIDPLM.Add(_Con.SqlRead.GetValue(1).ToString());
                        }
                        else if (_Con.SqlRead.GetValue(0).Equals(5))
                        {
                            _ListIPPIDBLU.Add(_Con.SqlRead.GetValue(1).ToString());
                        }
                        else if (_Con.SqlRead.GetValue(0).Equals(6))
                        {
                            _ListIPPIDBLS.Add(_Con.SqlRead.GetValue(1).ToString());
                        }
                        else if (_Con.SqlRead.GetValue(0).Equals(7))
                        {
                            _ListIPPIDDPD.Add(_Con.SqlRead.GetValue(1).ToString());
                        }
                    }
                    if (_Con.SqlRead.GetValue(2).Equals("PHP"))
                    {
                        // Location = 2
                        if (_Con.SqlRead.GetValue(0).Equals(2))
                        {
                            _ListIDPHPVLD.Add(_Con.SqlRead.GetValue(3).ToString());
                        }
                        // Location = 3
                        else if (_Con.SqlRead.GetValue(0).Equals(3))
                        {
                            _ListIDPHPEQU.Add(_Con.SqlRead.GetValue(3).ToString());
                        }
                        // Location = 4
                        else if (_Con.SqlRead.GetValue(0).Equals(4))
                        {
                            _ListIDPHPPLM.Add(_Con.SqlRead.GetValue(3).ToString());
                        }
                        // Location = 5
                        else if (_Con.SqlRead.GetValue(0).Equals(5))
                        {
                            _ListIDPHPBLU.Add(_Con.SqlRead.GetValue(3).ToString());
                        }
                        // Location = 6
                        else if (_Con.SqlRead.GetValue(0).Equals(6))
                        {
                            _ListIDPHPBLS.Add(_Con.SqlRead.GetValue(3).ToString());
                        }
                        // Location = 7
                        else if (_Con.SqlRead.GetValue(0).Equals(7))
                        {
                            _ListIDPHPDPD.Add(_Con.SqlRead.GetValue(3).ToString());
                        }
                    }

                }
            }
            catch (SqlException e)
            {
                _logger.Error(_Datetime + e.Message.ToString());
            }
        }
        public void GetDeviceList(string QueryProcedure, List<Int32> _TempVariables)
        {
            _Con = new Connection();
            _Con.Connected();
            _Con.SqlCmd = new SqlCommand
            {
                Connection = _Con.SqlCon,
                CommandType = CommandType.StoredProcedure,
                CommandText = QueryProcedure
            };
            try
            {
                _Con.SqlRead = _Con.SqlCmd.ExecuteReader();
                while (_Con.SqlRead.Read())
                {                
                    _TempVariables.Add(_Con.SqlRead.GetInt32(0));                    
                }
            }
            catch (SqlException e)
            {
                _logger.Error(_Datetime + e.Message.ToString());
            }
        }
        /// <summary>
        /// Connect To Audio Server CXS
        /// </summary>
        public void ConnectAudioServer()
        {
            try
            {
                StringArray ServerAddress = new StringArray
                {
                    //Primary Server
                    _ListIPServerCXS[0],
                    //Secondary Server
                    _ListIPServerCXS[1]
                };
                // set config items
                KeyValueMap ConfigurationItems = new KeyValueMap
                {
                    { "NETSPIRE_SDK_SOCKET_PORT", "20770" },
                    { "NETSPIRE_SDK_SET_LOG_LEVEL", "1" },
                    { "NETSPIRE_SDK_SET_DEBUG_FILE", "Open Access Server.log" },
                    { "NETSPIRE_SDK_SET_ERROR_FILE", "Open Access Server Error.log" }
                };
               // GetTblCommunication();
                // connect to AudioServer
                ConnectToAudioServer(ServerAddress, ConfigurationItems);
            }
            catch (Exception e)
            {
                _logger.Error(_Datetime + e.Message.ToString());
            }
        }
        public void ConnectToAudioServer(StringArray serverAddresses, KeyValueMap configItems)
        {
            try
            {
                if (_AudioServer == null)
                {
                    _AudioServer = new AudioServer();
                }
                _AudioServer.connect(serverAddresses, configItems);
            }
            catch (Exception e)
            {
                _logger.Error(_Datetime + e.Message.ToString());
                return;
            }

            Thread.Sleep(3000);
            while (!_AudioServer.isAudioConnected())
            {
                Console.WriteLine(_Datetime + "Trying to connect CXS....");
                Thread.Sleep(1000);
            }
            if (_AudioServer.isAudioConnected())
            {
                // Connect To AudioServer
                Console.WriteLine("=================================================SERVICE INFO========================================================");
                _AudioServerObserver = new AudioObserver(_ObjIPPA,_ObjMIC,_ObjNAC,
                    _ObjServer,
                    _ObjVelodromePHP,
                    _ObjEquistrianPHP,
                    _ObjBoulevardSelatanPHP,
                    _ObjBoulevatdUtaraPHP,
                    _ObjPulomasPHP,
                    _ObjDepoPHP,
                    _ObjVLDPID,_ObjEQUPID,_ObjPLMPID,_ObjBLUPID,_ObjBLSPID,_ObjDPDPID);
                _AudioServer.registerObserver(_AudioServerObserver);
                Console.WriteLine(_Datetime + "Audio CXS Service Started.....");
                // Connect To VOIP Call
                _CallControllerObserver = new callObserverClass();
                _AudioServer.getCallController().registerObserver(_CallControllerObserver);
                Console.WriteLine(_Datetime + "Call Controller Service Started.....");
                // Connect To PAServer
                _PAControllerObserver = new PAObserverClass();
                _PAControllerObserver.PAObserverSend(_IPServer, _Port);
                _AudioServer.getPAController().registerObserver(_PAControllerObserver);
                Console.WriteLine(_Datetime + "Public Announcements Controller Service Started.....");
                Console.WriteLine("=================================================MESSAGE INFO========================================================");
                // Countdown Timer Started
                _countDownTimer.Start();
                _logger.Debug(_Datetime + "Countdown Timer Started.....");
                Console.WriteLine(DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss :") + "Countdown Timer Started.....");
                SendDevicesState();
            }
        }
        public void DisconnectAudioServer()
        {
            try
            {
                if (_AudioServer != null)
                {
                    if (_mySourceId > 0)
                    {
                        _AudioServer.getPAController().detachPaSource(_mySourceId);
                        _mySourceId = 0;
                    }
                    _AudioServer.disconnect();
                    _AudioServer.Dispose();
                    _AudioServer = null;
                    _AudioServerObserver = null;
                }
            }
            catch (Exception e)
            {
                _logger.Error(_Datetime + e.Message.ToString());
            }
        }
  
        public int GetState(string State)
        {
            if (State.Equals("IDLE"))
            {
                return 1;
            }
            else if (State.Equals("ALERTING"))
            {
                return 2;
            }
            else if (State.Equals("ACTIVE"))
            {
                return 3;
            }
            else if (State.Equals("FAULTY"))
            {
                return 4;
            }
            else if (State.Equals("COMMSFAULT"))
            {
                return 5;
            }
            else if (State.Equals("HELD"))
            {
                return 6;
            }
            else
            {
                return 10;
            }
        }
        public int GetStatePA(string State)
        {
            if (State.Equals("HEALTHY"))
            {
                return 1;
            }
            else if (State.Equals("UNKNOWN_HEALTHSTATE"))
            {
                return 2;
            }
            else if (State.Equals("MINOR_FAULT"))
            {
                return 3;
            }
            else if (State.Equals("MAJOR_FAULT"))
            {
                return 5;
            }
            else
            {
                return 10;
            }
        }
        public int GetStateMIC(string State)
        {
            if (State.Equals("HEALTHY"))
            {
                return 1;
            }
            else if (State.Equals("UNKNOWN_HEALTHSTATE"))
            {
                return 4;
            }
            else if (State.Equals("FAULTY"))
            {
                return 5;
            }
            else
            {
                return 10;
            }
        }

        public void SendIPPAState(string variableName, int Value1, int value2)
        {
            try
            {
                SimpleTcpClient _IPPAClient;
                _IPPAClient = new SimpleTcpClient().Connect(_IPServer, _Port);
                _IPPAClient.Write("" + variableName + "," +
                    Value1 + "," +
                    value2 + ""
                    );
                _IPPAClient.Dispose();
            }
            catch (Exception e)
            {
                _logger.Error(_Datetime + e.Message.ToString());
            }
        }
        public void SendPAState(string variableName, int Value1, int Value2)
        {
            SimpleTcpClient _SendPAState;
            try
            {

                _SendPAState = new SimpleTcpClient().Connect(_IPServer, _Port);
                _SendPAState.Write("" + variableName + "," +
                    Value1 + "," +
                    Value2 + ""
                    );
                _SendPAState.Dispose();
            }
            catch (Exception e)
            {
                _logger.Error(_Datetime + e.Message.ToString());
            }
        }
        public void SendPHPState(string variableName, int Value1, int Value2)
        {
            SimpleTcpClient _SendPHPState;
            try
            {
                _SendPHPState = new SimpleTcpClient().Connect(_IPServer, _Port);
                _SendPHPState.Write("" + variableName + "," +
                    Value1 + "," +
                    Value2 + ""
                    );
                _SendPHPState.Dispose();
            }
            catch (Exception e)
            {
                _logger.Error(_Datetime + e.Message.ToString());
            }
        }
        public void SendPIDState(string variableName, int Value1, int Value2)
        {
            SimpleTcpClient _SendPIDState;
            try
            {

                _SendPIDState = new SimpleTcpClient().Connect(_IPServer, _Port);
                _SendPIDState.Write("" + variableName + "," +
                    Value1 + "," +
                    Value2 + ""
                    );
                _SendPIDState.Dispose();
            }
            catch (Exception e)
            {
                _logger.Error(_Datetime + e.Message.ToString());
            }
        }
        public void SendMicState(string variableName, int Value1, int Value2)
        {
            SimpleTcpClient _SendMICState;
            try
            {

                _SendMICState = new SimpleTcpClient().Connect(_IPServer, _Port);
                _SendMICState.Write("" + variableName + "," +
                    Value1 + "," +
                    Value2 + ""
                    );
                _SendMICState.Dispose();
            }
            catch (Exception e)
            {
                _logger.Error(_Datetime + e.Message.ToString());
            }
        }
        public void SendServerState(string variableName, int ServerIndex, int ServerValue)
        {
            try
            {
                SimpleTcpClient _ServerState;
                _ServerState = new SimpleTcpClient().Connect(_IPServer, _Port);
                _ServerState.Write("" + variableName + "," +
                    ServerIndex + "," +
                    ServerValue + ""
                    );
                _ServerState.Dispose();
            }
            catch (Exception e)
            {
                _logger.Error(_Datetime + e.Message.ToString());
            }
        }
        /// <summary>
        /// Send All Devices Including IPPA,NAC,PHP,PID,PA also Microphone.
        /// </summary>
        public void SendDevicesState()
        {
            // Get All Devices
            DeviceStateArray _DeviceStateArray = _AudioServer.getDeviceStates();
            for (short i = 0; i < _DeviceStateArray.Count; i++)
            {
                Device thisDevice = _DeviceStateArray.ElementAt(i);
                // IPPA List                
                if (thisDevice.getDstNo().ToString().Equals(_ListIPPA[0]))
                {
                    _ObjIPPA.IPPAVLD = GetState(thisDevice.getState().ToString());
                    SendIPPAState("IPPA.Status", 0, _ObjIPPA.IPPAVLD);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListIPPA[1]))
                {
                    _ObjIPPA.IPPAEQU = GetState(thisDevice.getState().ToString());
                    SendIPPAState("IPPA.Status", 1, _ObjIPPA.IPPAEQU);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListIPPA[2]))
                {
                    _ObjIPPA.IPPAPLM = GetState(thisDevice.getState().ToString());
                    SendIPPAState("IPPA.Status", 2, _ObjIPPA.IPPAPLM);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListIPPA[3]))
                {
                    _ObjIPPA.IPPABLU = GetState(thisDevice.getState().ToString());
                    SendIPPAState("IPPA.Status", 3, _ObjIPPA.IPPABLU);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListIPPA[4]))
                {
                    _ObjIPPA.IPPABLS = GetState(thisDevice.getState().ToString());
                    SendIPPAState("IPPA.Status", 4, _ObjIPPA.IPPABLS);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListIPPA[5]))
                {
                    _ObjIPPA.IPPADPD = GetState(thisDevice.getState().ToString());
                    SendIPPAState("IPPA.Status", 5, _ObjIPPA.IPPADPD);
                }
                // NAC List
                if (thisDevice.getDstNo().ToString().Equals(_ListNAC[0]))
                {
                    _ObjNAC.NAC1 = GetState(thisDevice.getState().ToString());
                    SendPHPState("NAC.Status", 0, _ObjNAC.NAC1);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListNAC[1]))
                {
                    _ObjNAC.NAC2 = GetState(thisDevice.getState().ToString());
                    SendPHPState("NAC.Status", 1, _ObjNAC.NAC2);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListNAC[2]))
                {
                    _ObjNAC.NAC3 = GetState(thisDevice.getState().ToString());
                    SendPHPState("NAC.Status", 2, _ObjNAC.NAC3);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListNAC[3]))
                {
                    _ObjNAC.NAC4 = GetState(thisDevice.getState().ToString());
                    SendPHPState("NAC.Status", 3, _ObjNAC.NAC4);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListNAC[4]))
                {
                    _ObjNAC.NAC5 = GetState(thisDevice.getState().ToString());
                    SendPHPState("NAC.Status", 4, _ObjNAC.NAC5);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListNAC[5]))
                {
                    _ObjNAC.NAC6 = GetState(thisDevice.getState().ToString());
                    SendPHPState("NAC.Status", 5, _ObjNAC.NAC6);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListNAC[6]))
                {
                    _ObjNAC.NAC7 = GetState(thisDevice.getState().ToString());
                    SendPHPState("NAC.Status", 6, _ObjNAC.NAC7);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListNAC[7]))
                {
                    _ObjNAC.NAC8 = GetState(thisDevice.getState().ToString());
                    SendPHPState("NAC.Status", 7, _ObjNAC.NAC8);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListNAC[8]))
                {
                    _ObjNAC.NAC9 = GetState(thisDevice.getState().ToString());
                    SendPHPState("NAC.Status", 8, _ObjNAC.NAC9);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListNAC[9]))
                {
                    _ObjNAC.NAC10 = GetState(thisDevice.getState().ToString());
                    SendPHPState("NAC.Status", 9, _ObjNAC.NAC10);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListNAC[10]))
                {
                    _ObjNAC.NAC11 = GetState(thisDevice.getState().ToString());
                    SendPHPState("NAC.Status", 10, _ObjNAC.NAC11);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListNAC[11]))
                {
                    _ObjNAC.NAC12 = GetState(thisDevice.getState().ToString());
                    SendPHPState("NAC.Status", 11, _ObjNAC.NAC12);
                }
                // Server List  CXS Server                      
                if (thisDevice.getIP().ToString().Equals(_ListIPServerCXS[0]))
                {
                    _ObjServer.Server1 = GetState(thisDevice.getState().ToString());
                    SendServerState("CXSServer", 0, _ObjServer.Server1);
                }
                if (thisDevice.getIP().ToString().Equals(_ListIPServerCXS[1]))
                {
                    _ObjServer.Server2 = GetState(thisDevice.getState().ToString());
                    SendServerState("CXSServer", 1, _ObjServer.Server2);
                }
                // PHP Velodrome
                if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPVLD[0]))
                {
                    _ObjVelodromePHP.ObjPHP1 = GetState(thisDevice.getState().ToString());
                    SendPHPState("PHPVLD", 0, _ObjVelodromePHP.ObjPHP1);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPVLD[1]))
                {
                    _ObjVelodromePHP.ObjPHP2 = GetState(thisDevice.getState().ToString());
                    SendPHPState("PHPVLD", 1, _ObjVelodromePHP.ObjPHP2);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPVLD[2]))
                {
                    _ObjVelodromePHP.ObjPHP3 = GetState(thisDevice.getState().ToString());
                    SendPHPState("PHPVLD", 2, _ObjVelodromePHP.ObjPHP3);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPVLD[3]))
                {
                    _ObjVelodromePHP.ObjPHP4 = GetState(thisDevice.getState().ToString());
                    SendPHPState("PHPVLD", 3, _ObjVelodromePHP.ObjPHP4);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPVLD[4]))
                {
                    _ObjVelodromePHP.ObjPHP5 = GetState(thisDevice.getState().ToString());
                    SendPHPState("PHPVLD", 4, _ObjVelodromePHP.ObjPHP5);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPVLD[5]))
                {
                    _ObjVelodromePHP.ObjPHP6 = GetState(thisDevice.getState().ToString());
                    SendPHPState("PHPVLD", 5, _ObjVelodromePHP.ObjPHP6);
                }
                // PHP Equistrian
                if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPEQU[0]))
                {
                    _ObjEquistrianPHP.ObjPHP1 = GetState(thisDevice.getState().ToString());
                    SendPHPState("PHPEQU", 0, _ObjEquistrianPHP.ObjPHP1);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPEQU[1]))
                {
                    _ObjEquistrianPHP.ObjPHP2 = GetState(thisDevice.getState().ToString());
                    SendPHPState("PHPEQU", 1, _ObjEquistrianPHP.ObjPHP2);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPEQU[2]))
                {
                    _ObjEquistrianPHP.ObjPHP3 = GetState(thisDevice.getState().ToString());
                    SendPHPState("PHPEQU", 2, _ObjEquistrianPHP.ObjPHP3);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPEQU[3]))
                {
                    _ObjEquistrianPHP.ObjPHP4 = GetState(thisDevice.getState().ToString());
                    SendPHPState("PHPEQU", 3, _ObjEquistrianPHP.ObjPHP4);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPEQU[4]))
                {
                    _ObjEquistrianPHP.ObjPHP5 = GetState(thisDevice.getState().ToString());
                    SendPHPState("PHPEQU", 4, _ObjEquistrianPHP.ObjPHP5);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPEQU[5]))
                {
                    _ObjEquistrianPHP.ObjPHP6 = GetState(thisDevice.getState().ToString());
                    SendPHPState("PHPEQU", 5, _ObjEquistrianPHP.ObjPHP6);
                }
                // PHP Pulomas
                if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPPLM[0]))
                {
                    _ObjPulomasPHP.ObjPHP1 = GetState(thisDevice.getState().ToString());
                    SendPHPState("PHPPLM", 0, _ObjPulomasPHP.ObjPHP1);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPPLM[1]))
                {
                    _ObjPulomasPHP.ObjPHP2 = GetState(thisDevice.getState().ToString());
                    SendPHPState("PHPPLM", 1, _ObjPulomasPHP.ObjPHP2);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPPLM[2]))
                {
                    _ObjPulomasPHP.ObjPHP3 = GetState(thisDevice.getState().ToString());
                    SendPHPState("PHPPLM", 2, _ObjPulomasPHP.ObjPHP3);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPPLM[3]))
                {
                    _ObjPulomasPHP.ObjPHP4 = GetState(thisDevice.getState().ToString());
                    SendPHPState("PHPPLM", 3, _ObjPulomasPHP.ObjPHP4);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPPLM[4]))
                {
                    _ObjPulomasPHP.ObjPHP5 = GetState(thisDevice.getState().ToString());
                    SendPHPState("PHPPLM", 4, _ObjPulomasPHP.ObjPHP5);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPPLM[5]))
                {
                    _ObjPulomasPHP.ObjPHP6 = GetState(thisDevice.getState().ToString());
                    SendPHPState("PHPPLM", 5, _ObjPulomasPHP.ObjPHP6);
                }
                // PHP Boulevard Raya Utara
                if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPBLU[0]))
                {
                    _ObjBoulevatdUtaraPHP.ObjPHP1 = GetState(thisDevice.getState().ToString());
                    SendPHPState("PHPBLU", 0, _ObjBoulevatdUtaraPHP.ObjPHP1);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPBLU[1]))
                {
                    _ObjBoulevatdUtaraPHP.ObjPHP2 = GetState(thisDevice.getState().ToString());
                    SendPHPState("PHPBLU", 1, _ObjBoulevatdUtaraPHP.ObjPHP2);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPBLU[2]))
                {
                    _ObjBoulevatdUtaraPHP.ObjPHP3 = GetState(thisDevice.getState().ToString());
                    SendPHPState("PHPBLU", 2, _ObjBoulevatdUtaraPHP.ObjPHP3);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPBLU[3]))
                {
                    _ObjBoulevatdUtaraPHP.ObjPHP4 = GetState(thisDevice.getState().ToString());
                    SendPHPState("PHPBLU", 3, _ObjBoulevatdUtaraPHP.ObjPHP4);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPBLU[4]))
                {
                    _ObjBoulevatdUtaraPHP.ObjPHP5 = GetState(thisDevice.getState().ToString());
                    SendPHPState("PHPBLU", 4, _ObjBoulevatdUtaraPHP.ObjPHP5);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPBLU[5]))
                {
                    _ObjBoulevatdUtaraPHP.ObjPHP6 = GetState(thisDevice.getState().ToString());
                    SendPHPState("PHPBLU", 5, _ObjBoulevatdUtaraPHP.ObjPHP6);
                }
                // PHP Boulevard Raya Selatan
                if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPBLS[0]))
                {
                    _ObjBoulevardSelatanPHP.ObjPHP1 = GetState(thisDevice.getState().ToString());
                    SendPHPState("PHPBLS", 0, _ObjBoulevardSelatanPHP.ObjPHP1);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPBLS[1]))
                {
                    _ObjBoulevardSelatanPHP.ObjPHP2 = GetState(thisDevice.getState().ToString());
                    SendPHPState("PHPBLS", 1, _ObjBoulevardSelatanPHP.ObjPHP2);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPBLS[2]))
                {
                    _ObjBoulevardSelatanPHP.ObjPHP3 = GetState(thisDevice.getState().ToString());
                    SendPHPState("PHPBLS", 2, _ObjBoulevardSelatanPHP.ObjPHP3);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPBLS[3]))
                {
                    _ObjBoulevardSelatanPHP.ObjPHP4 = GetState(thisDevice.getState().ToString());
                    SendPHPState("PHPBLS", 3, _ObjBoulevardSelatanPHP.ObjPHP4);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPBLS[4]))
                {
                    _ObjBoulevardSelatanPHP.ObjPHP5 = GetState(thisDevice.getState().ToString());
                    SendPHPState("PHPBLS", 4, _ObjBoulevardSelatanPHP.ObjPHP5);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPBLS[5]))
                {
                    _ObjBoulevardSelatanPHP.ObjPHP6 = GetState(thisDevice.getState().ToString());
                    SendPHPState("PHPBLS", 5, _ObjBoulevardSelatanPHP.ObjPHP6);
                }
                // PHP Depo
                if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPDPD[0]))
                {
                    _ObjDepoPHP.ObjPHP1 = GetState(thisDevice.getState().ToString());
                    SendPHPState("PHPDPD", 0, _ObjDepoPHP.ObjPHP1);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPDPD[1]))
                {
                    _ObjDepoPHP.ObjPHP2 = GetState(thisDevice.getState().ToString());
                    SendPHPState("PHPDPD", 1, _ObjDepoPHP.ObjPHP2);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPDPD[2]))
                {
                    _ObjDepoPHP.ObjPHP3 = GetState(thisDevice.getState().ToString());
                    SendPHPState("PHPDPD", 2, _ObjDepoPHP.ObjPHP3);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPDPD[3]))
                {
                    _ObjDepoPHP.ObjPHP4 = GetState(thisDevice.getState().ToString());
                    SendPHPState("PHPDPD", 3, _ObjDepoPHP.ObjPHP4);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPDPD[4]))
                {
                    _ObjDepoPHP.ObjPHP5 = GetState(thisDevice.getState().ToString());
                    SendPHPState("PHPDPD", 4, _ObjDepoPHP.ObjPHP5);
                }
                if (thisDevice.getDstNo().ToString().Equals(_ListIDPHPDPD[5]))
                {
                    _ObjDepoPHP.ObjPHP6 = GetState(thisDevice.getState().ToString());
                    SendPHPState("PHPDPD", 5, _ObjDepoPHP.ObjPHP6);
                }

                // PID Velodrome
                if (thisDevice.getIP().ToString().Equals(_ListIPPIDVLD[0]))
                {
                    _ObjVLDPID.ObjPID1 = GetState(thisDevice.getState().ToString());
                    SendPIDState("PID.CNC", 0, _ObjVLDPID.ObjPID1);
                }
                if (thisDevice.getIP().ToString().Equals(_ListIPPIDVLD[1]))
                {
                    _ObjVLDPID.ObjPID2 = GetState(thisDevice.getState().ToString());
                    SendPIDState("PID.CNC", 1, _ObjVLDPID.ObjPID2);
                }
                if (thisDevice.getIP().ToString().Equals(_ListIPPIDVLD[2]))
                {
                    _ObjVLDPID.ObjPID3 = GetState(thisDevice.getState().ToString());
                    SendPIDState("PID.PFM", 0, _ObjVLDPID.ObjPID3);
                }
                if (thisDevice.getIP().ToString().Equals(_ListIPPIDVLD[3]))
                {
                    _ObjVLDPID.ObjPID4 = GetState(thisDevice.getState().ToString());
                    SendPIDState("PID.PFM", 1, _ObjVLDPID.ObjPID4);
                }
                // PID Equistrian 
                if (thisDevice.getIP().ToString().Equals(_ListIPPIDEQU[0]))
                {
                    _ObjEQUPID.ObjPID1 = GetState(thisDevice.getState().ToString());
                    SendPIDState("PID.CNC", 2, _ObjEQUPID.ObjPID1);
                }
                if (thisDevice.getIP().ToString().Equals(_ListIPPIDEQU[1]))
                {
                    _ObjEQUPID.ObjPID2 = GetState(thisDevice.getState().ToString());
                    SendPIDState("PID.CNC", 3, _ObjEQUPID.ObjPID2);
                }
                if (thisDevice.getIP().ToString().Equals(_ListIPPIDEQU[2]))
                {
                    _ObjEQUPID.ObjPID3 = GetState(thisDevice.getState().ToString());
                    SendPIDState("PID.PFM", 2, _ObjEQUPID.ObjPID3);
                }
                if (thisDevice.getIP().ToString().Equals(_ListIPPIDEQU[3]))
                {
                    _ObjEQUPID.ObjPID4 = GetState(thisDevice.getState().ToString());
                    SendPIDState("PID.PFM", 3, _ObjEQUPID.ObjPID4);
                }
                // PID Pulomas 
                if (thisDevice.getIP().ToString().Equals(_ListIPPIDPLM[0]))
                {
                    _ObjPLMPID.ObjPID1 = GetState(thisDevice.getState().ToString());
                    SendPIDState("PID.CNC", 4, _ObjPLMPID.ObjPID1);
                }
                if (thisDevice.getIP().ToString().Equals(_ListIPPIDPLM[1]))
                {
                    _ObjPLMPID.ObjPID2 = GetState(thisDevice.getState().ToString());
                    SendPIDState("PID.CNC", 5, _ObjPLMPID.ObjPID2);
                }
                if (thisDevice.getIP().ToString().Equals(_ListIPPIDPLM[2]))
                {
                    _ObjPLMPID.ObjPID3 = GetState(thisDevice.getState().ToString());
                    SendPIDState("PID.PFM", 4, _ObjPLMPID.ObjPID3);
                }
                if (thisDevice.getIP().ToString().Equals(_ListIPPIDPLM[3]))
                {
                    _ObjPLMPID.ObjPID4 = GetState(thisDevice.getState().ToString());
                    SendPIDState("PID.PFM", 5, _ObjPLMPID.ObjPID4);
                }
                // PID Boulevard Utara 
                if (thisDevice.getIP().ToString().Equals(_ListIPPIDBLU[0]))
                {
                    _ObjBLUPID.ObjPID1 = GetState(thisDevice.getState().ToString());
                    SendPIDState("PID.CNC", 6, _ObjBLUPID.ObjPID1);
                }
                if (thisDevice.getIP().ToString().Equals(_ListIPPIDBLU[1]))
                {
                    _ObjBLUPID.ObjPID2 = GetState(thisDevice.getState().ToString());
                    SendPIDState("PID.CNC", 7, _ObjBLUPID.ObjPID2);
                }
                if (thisDevice.getIP().ToString().Equals(_ListIPPIDBLU[2]))
                {
                    _ObjBLUPID.ObjPID3 = GetState(thisDevice.getState().ToString());
                    SendPIDState("PID.PFM", 6, _ObjBLUPID.ObjPID3);
                }
                if (thisDevice.getIP().ToString().Equals(_ListIPPIDBLU[3]))
                {
                    _ObjBLUPID.ObjPID4 = GetState(thisDevice.getState().ToString());
                    SendPIDState("PID.PFM", 7, _ObjBLUPID.ObjPID4);
                }
                // PID Boulevard Selatan 
                if (thisDevice.getIP().ToString().Equals(_ListIPPIDBLS[0]))
                {
                    _ObjBLSPID.ObjPID1 = GetState(thisDevice.getState().ToString());
                    SendPIDState("PID.CNC", 8, _ObjBLSPID.ObjPID1);
                }
                if (thisDevice.getIP().ToString().Equals(_ListIPPIDBLS[1]))
                {
                    _ObjBLSPID.ObjPID2 = GetState(thisDevice.getState().ToString());
                    SendPIDState("PID.CNC", 9, _ObjBLSPID.ObjPID2);
                }
                if (thisDevice.getIP().ToString().Equals(_ListIPPIDBLS[2]))
                {
                    _ObjBLSPID.ObjPID3 = GetState(thisDevice.getState().ToString());
                    SendPIDState("PID.PFM", 8, _ObjBLSPID.ObjPID3);
                }
                if (thisDevice.getIP().ToString().Equals(_ListIPPIDBLS[3]))
                {
                    _ObjBLSPID.ObjPID4 = GetState(thisDevice.getState().ToString());
                    SendPIDState("PID.PFM", 9, _ObjBLSPID.ObjPID4);
                }
                // PID Depo 
                if (thisDevice.getIP().ToString().Equals(_ListIPPIDDPD[0]))
                {
                    _ObjDPDPID.ObjPID1 = GetState(thisDevice.getState().ToString());
                    SendPIDState("PID.CNC", 10, _ObjDPDPID.ObjPID1);
                }
                if (thisDevice.getIP().ToString().Equals(_ListIPPIDDPD[1]))
                {
                    _ObjDPDPID.ObjPID2 = GetState(thisDevice.getState().ToString());
                    SendPIDState("PID.CNC", 11, _ObjDPDPID.ObjPID2);
                }
                if (thisDevice.getIP().ToString().Equals(_ListIPPIDDPD[2]))
                {
                    _ObjDPDPID.ObjPID3 = GetState(thisDevice.getState().ToString());
                    SendPIDState("PID.PFM", 10, _ObjDPDPID.ObjPID3);
                }
                if (thisDevice.getIP().ToString().Equals(_ListIPPIDDPD[3]))
                {
                    _ObjDPDPID.ObjPID4 = GetState(thisDevice.getState().ToString());
                    SendPIDState("PID.PFM", 11, _ObjDPDPID.ObjPID4);
                }          
              }
            //Get PA Audio Status
            PaSinkArray _GetPASink = _AudioServer.getAudioSinks();
            for (int a = 0; a < _GetPASink.Count; a++)
            {
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneVLD[0]))
                {
                    _ObjVelodromePA.ObjPA1 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.PA", 0, _ObjVelodromePA.ObjPA1);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneVLD[1]))
                {
                    _ObjVelodromePA.ObjPA2 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.PA", 1, _ObjVelodromePA.ObjPA2);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneVLD[2]))
                {
                    _ObjVelodromePA.ObjPA3 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.PA", 2, _ObjVelodromePA.ObjPA3);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneVLD[3]))
                {
                    _ObjVelodromePA.ObjPA4 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.PA", 3, _ObjVelodromePA.ObjPA4);
                }

                // Equistrian
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneEQU[0]))
                {
                    _ObjEquistrianPA.ObjPA1 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.PA", 4, _ObjEquistrianPA.ObjPA1);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneEQU[1]))
                {
                    _ObjEquistrianPA.ObjPA2 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.PA", 5, _ObjEquistrianPA.ObjPA2);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneEQU[2]))
                {
                    _ObjEquistrianPA.ObjPA3 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.PA", 6, _ObjEquistrianPA.ObjPA3);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneEQU[3]))
                {
                    _ObjEquistrianPA.ObjPA4 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.PA", 7, _ObjEquistrianPA.ObjPA4);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneEQU[4]))
                {
                    _ObjEquistrianPA.ObjPA5 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.PA", 8, _ObjEquistrianPA.ObjPA5);
                }
                // Pulomas
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZonePLM[0]))
                {
                    _ObjPulomasPA.ObjPA1 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.PA", 9, _ObjPulomasPA.ObjPA1);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZonePLM[1]))
                {
                    _ObjPulomasPA.ObjPA2 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.PA", 10, _ObjPulomasPA.ObjPA2);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZonePLM[2]))
                {
                    _ObjPulomasPA.ObjPA3 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.PA", 11, _ObjPulomasPA.ObjPA3);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZonePLM[3]))
                {
                    _ObjPulomasPA.ObjPA4 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.PA", 12, _ObjPulomasPA.ObjPA4);
                }
                // Boulevard Utara
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneBLU[0]))
                {
                    _ObjBoulevardUtaraPA.ObjPA1 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.PA", 13, _ObjBoulevardUtaraPA.ObjPA1);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneBLU[1]))
                {
                    _ObjBoulevardUtaraPA.ObjPA2 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.PA", 14, _ObjBoulevardUtaraPA.ObjPA2);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneBLU[2]))
                {
                    _ObjBoulevardUtaraPA.ObjPA3 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.PA", 15, _ObjBoulevardUtaraPA.ObjPA3);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneBLU[3]))
                {
                    _ObjBoulevardUtaraPA.ObjPA4 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.PA", 16, _ObjBoulevardUtaraPA.ObjPA4);
                }
                // Boulevard Selatan
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneBLS[0]))
                {
                    _ObjBoulevardSelatanPA.ObjPA1 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.PA", 17, _ObjBoulevardSelatanPA.ObjPA1);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneBLS[1]))
                {
                    _ObjBoulevardSelatanPA.ObjPA2 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.PA", 18, _ObjBoulevardSelatanPA.ObjPA2);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneBLS[2]))
                {
                    _ObjBoulevardSelatanPA.ObjPA3 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.PA", 19, _ObjBoulevardSelatanPA.ObjPA3);
                }

                // Non Public Area
                //Velodrome
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneVLD[4]))
                {
                    _ObjVelodromePA.ObjNPA1 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.NPA", 0, _ObjVelodromePA.ObjNPA1);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneVLD[5]))
                {
                    _ObjVelodromePA.ObjNPA2 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.NPA", 1, _ObjVelodromePA.ObjNPA2);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneVLD[6]))
                {
                    _ObjVelodromePA.ObjNPA3 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.NPA", 2, _ObjVelodromePA.ObjNPA3);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneVLD[7]))
                {
                    _ObjVelodromePA.ObjNPA4 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.NPA", 3, _ObjVelodromePA.ObjNPA4);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneVLD[8]))
                {
                    _ObjVelodromePA.ObjNPA5 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.NPA", 4, _ObjVelodromePA.ObjNPA5);
                }
                // Equistrian
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneEQU[5]))
                {
                    _ObjEquistrianPA.ObjNPA1 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.NPA", 5, _ObjEquistrianPA.ObjNPA1);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneEQU[6]))
                {
                    _ObjEquistrianPA.ObjNPA2 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.NPA", 6, _ObjEquistrianPA.ObjNPA2);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneEQU[7]))
                {
                    _ObjEquistrianPA.ObjNPA3 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.NPA", 7, _ObjEquistrianPA.ObjNPA3);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneEQU[8]))
                {
                    _ObjEquistrianPA.ObjNPA4 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.NPA", 8, _ObjEquistrianPA.ObjNPA4);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneEQU[9]))
                {
                    _ObjEquistrianPA.ObjNPA5 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.NPA", 9, _ObjEquistrianPA.ObjNPA5);
                }
                // Pulomas
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZonePLM[4]))
                {
                    _ObjPulomasPA.ObjNPA1 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.NPA", 10, _ObjPulomasPA.ObjNPA1);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZonePLM[5]))
                {
                    _ObjPulomasPA.ObjNPA2 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.NPA", 11, _ObjPulomasPA.ObjNPA2);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZonePLM[6]))
                {
                    _ObjPulomasPA.ObjNPA3 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.NPA", 12, _ObjPulomasPA.ObjNPA3);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZonePLM[7]))
                {
                    _ObjPulomasPA.ObjNPA4 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.NPA", 13, _ObjPulomasPA.ObjNPA4);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZonePLM[8]))
                {
                    _ObjPulomasPA.ObjNPA5 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.NPA", 14, _ObjPulomasPA.ObjNPA5);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZonePLM[9]))
                {
                    _ObjPulomasPA.ObjNPA6= GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.NPA", 15, _ObjPulomasPA.ObjNPA6);
                }
                // Boulevard Utara
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneBLU[4]))
                {
                    _ObjBoulevardUtaraPA.ObjNPA1 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.NPA", 16, _ObjBoulevardUtaraPA.ObjNPA1);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneBLU[5]))
                {
                    _ObjBoulevardUtaraPA.ObjNPA2 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.NPA", 17, _ObjBoulevardUtaraPA.ObjNPA2);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneBLU[6]))
                {
                    _ObjBoulevardUtaraPA.ObjNPA3 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.NPA", 18, _ObjBoulevardUtaraPA.ObjNPA3);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneBLU[7]))
                {
                    _ObjBoulevardUtaraPA.ObjNPA4 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.NPA", 19, _ObjBoulevardUtaraPA.ObjNPA4);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneBLU[8]))
                {
                    _ObjBoulevardUtaraPA.ObjNPA5 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.NPA", 20, _ObjBoulevardUtaraPA.ObjNPA5);
                }
                // Boulevard Selatan
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneBLS[3]))
                {
                    _ObjBoulevardSelatanPA.ObjNPA1 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.NPA", 21, _ObjBoulevardSelatanPA.ObjNPA1);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneBLS[4]))
                {
                    _ObjBoulevardSelatanPA.ObjNPA2 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.NPA", 22, _ObjBoulevardSelatanPA.ObjNPA2);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneBLS[5]))
                {
                    _ObjBoulevardSelatanPA.ObjNPA3 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.NPA", 23, _ObjBoulevardSelatanPA.ObjNPA3);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneBLS[6]))
                {
                    _ObjBoulevardSelatanPA.ObjNPA4 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.NPA", 24, _ObjBoulevardSelatanPA.ObjNPA4);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneBLS[7]))
                {
                    _ObjBoulevardSelatanPA.ObjNPA5 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.NPA", 25, _ObjBoulevardSelatanPA.ObjNPA5);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneBLS[8]))
                {
                    _ObjBoulevardSelatanPA.ObjNPA6 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.NPA", 26, _ObjBoulevardSelatanPA.ObjNPA6);
                }

                // Platform
                //Velodrome
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneVLD[9]))
                {
                    _ObjVelodromePA.ObjPF1 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.PFM", 0, _ObjVelodromePA.ObjPF1);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneVLD[10]))
                {
                    _ObjVelodromePA.ObjPF2 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.PFM", 1, _ObjVelodromePA.ObjPF2);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneVLD[11]))
                {
                    _ObjVelodromePA.ObjPF3 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.PFM", 2, _ObjVelodromePA.ObjPF3);
                }
                //Equistrian
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneEQU[10]))
                {
                    _ObjEquistrianPA.ObjPF1 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.PFM", 3, _ObjEquistrianPA.ObjPF1);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneEQU[11]))
                {
                    _ObjEquistrianPA.ObjPF2 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.PFM", 4, _ObjEquistrianPA.ObjPF2);
                }
                //Pulomas
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZonePLM[10]))
                {
                    _ObjPulomasPA.ObjPF1 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.PFM", 5, _ObjPulomasPA.ObjPF1);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZonePLM[11]))
                {
                    _ObjPulomasPA.ObjPF2 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.PFM", 6, _ObjPulomasPA.ObjPF2);
                }

                ////Boulevard Utara
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneBLU[9]))
                {
                    _ObjBoulevardUtaraPA.ObjPF1 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.PFM", 7, _ObjBoulevardUtaraPA.ObjPF1);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneBLU[10]))
                {
                    _ObjBoulevardUtaraPA.ObjPF2 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.PFM", 8, _ObjBoulevardUtaraPA.ObjPF2);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneBLU[11]))
                {
                    _ObjBoulevardUtaraPA.ObjPF3 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.PFM", 9, _ObjBoulevardUtaraPA.ObjPF3);
                }
                //Boulevard Selatan
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneBLS[9]))
                {
                    _ObjBoulevardSelatanPA.ObjPF1 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.PFM", 10, _ObjBoulevardSelatanPA.ObjPF1);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneBLS[10]))
                {
                    _ObjBoulevardSelatanPA.ObjPF2 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.PFM", 11, _ObjBoulevardSelatanPA.ObjPF2);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneBLS[11]))
                {
                    _ObjBoulevardSelatanPA.ObjPF3 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.PFM", 12, _ObjBoulevardSelatanPA.ObjPF3);
                }
                // AFIL
                // Velodrome
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneVLD[12]))
                {
                    _ObjVelodromePA.ObjAFIL1 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.AFIL", 0, _ObjVelodromePA.ObjAFIL1);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneVLD[13]))
                {
                    _ObjVelodromePA.ObjAFIL2 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.AFIL", 1, _ObjVelodromePA.ObjAFIL2);
                }
                // Equistrian
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneEQU[12]))
                {
                    _ObjEquistrianPA.ObjAFIL1 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.AFIL", 2, _ObjEquistrianPA.ObjAFIL1);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneEQU[13]))
                {
                    _ObjEquistrianPA.ObjAFIL2 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.AFIL", 3, _ObjEquistrianPA.ObjAFIL2);
                }
                // Pulomas
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZonePLM[12]))
                {
                    _ObjPulomasPA.ObjAFIL1 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.AFIL", 4, _ObjPulomasPA.ObjAFIL1);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZonePLM[13]))
                {
                    _ObjPulomasPA.ObjAFIL2 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.AFIL", 5, _ObjPulomasPA.ObjAFIL2);
                }
                // Boulevard Utara
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneBLU[12]))
                {
                    _ObjBoulevardUtaraPA.ObjAFIL1 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.AFIL", 6, _ObjBoulevardUtaraPA.ObjAFIL1);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneBLU[13]))
                {
                    _ObjBoulevardUtaraPA.ObjAFIL2 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.AFIL", 7, _ObjBoulevardUtaraPA.ObjAFIL2);
                }
                // Boulevard Selatan
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneBLS[12]))
                {
                    _ObjBoulevardSelatanPA.ObjAFIL1 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.AFIL", 8, _ObjBoulevardSelatanPA.ObjAFIL1);
                }
                if (_GetPASink.ElementAt(a).id.Equals(_ListPAZoneBLS[13]))
                {
                    _ObjBoulevardSelatanPA.ObjAFIL2 = GetStatePA(_GetPASink.ElementAt(a).healthState.ToString());
                    SendPAState("PA.AFIL", 9, _ObjBoulevardSelatanPA.ObjAFIL2);
                }
            }
            //Get PA Microphone
            PAController _GetPAController = _AudioServer.getPAController();
            PaSourceArray _GetPAMic = _GetPAController.getPaSources();
            for (int i = 0; i < _GetPAMic.Count; i++)
            {
                PaSource _PASource = _GetPAMic.ElementAt(i);
                if (_PASource.id.Equals(_ListMIC[0]))
                {
                    _ObjMIC.MICVLD = GetStateMIC(_PASource.healthState.ToString());
                    SendMicState("IPPA.Status.Mic", 0, _ObjMIC.MICVLD);
                }
                if (_PASource.id.Equals(_ListMIC[1]))
                {
                    _ObjMIC.MICEQU = GetStateMIC(_PASource.healthState.ToString());
                    SendMicState("IPPA.Status.Mic", 1, _ObjMIC.MICEQU);
                }
                if (_PASource.id.Equals(_ListMIC[2]))
                {
                    _ObjMIC.MICPLM = GetStateMIC(_PASource.healthState.ToString());
                    SendMicState("IPPA.Status.Mic", 2, _ObjMIC.MICPLM);
                }
                if (_PASource.id.Equals(_ListMIC[3]))
                {
                    _ObjMIC.MICBLU = GetStateMIC(_PASource.healthState.ToString());
                    SendMicState("IPPA.Status.Mic", 3, _ObjMIC.MICBLU);
                }
                if (_PASource.id.Equals(_ListMIC[4]))
                {
                    _ObjMIC.MICBLS = GetStateMIC(_PASource.healthState.ToString());
                    SendMicState("IPPA.Status.Mic", 4, _ObjMIC.MICBLS);
                }
                if (_PASource.id.Equals(_ListMIC[5]))
                {
                    _ObjMIC.MICDPD = GetStateMIC(_PASource.healthState.ToString());
                    SendMicState("IPPA.Status.Mic", 5, _ObjMIC.MICDPD);
                }
                if (_PASource.id.Equals(_ListMIC[6]))
                {
                    _ObjMIC.MICOCC = GetStateMIC(_PASource.healthState.ToString());
                    SendMicState("IPPA.Status.Mic", 6, _ObjMIC.MICOCC);
                }
                if (_PASource.id.Equals(_ListMIC[7]))
                {
                    _ObjMIC.MICOCC = GetStateMIC(_PASource.healthState.ToString());
                    SendMicState("IPPA.Status.Mic", 7, _ObjMIC.MICBCC);
                }
            }
        }
        /// <summary>
        /// Play DVA Audio From HMI
        /// </summary>
        public void GetPlayDVA(List<string> ZoneValue, List<uint> CustomMedia)
        {
            // Check audio server connection
            if (_AudioServer == null || !_AudioServer.isAudioConnected())
            {
                _logger.Error(DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss :") + "CXS Not Connected.....!");
                return;
            }
            try
            {
                //Audio configuration
                netspire.Message _NetspireMessages = new netspire.Message();
                StringArray _NetspireZone = new StringArray();
                NumberArray _NetspireMediaItems = new NumberArray();
                MessagePriority _msgPriority = new MessagePriority();
                _msgPriority.setPriorityMode(AnnouncementPriorityMode.APM_RELATIVE_PRIORITY);
                _msgPriority.setPrioritylevel(950);
                // add new zone
                foreach (string a in ZoneValue)
                {
                     //Thread.Sleep(100);
                     _NetspireZone.Add(a);
                     Console.WriteLine("Zone Added :" + a);
                }
                // filter if zone exist
                if (_NetspireZone.Count > 0)
                {
                    try
                    {
                        // add new media items
                        foreach (uint var in CustomMedia)
                        {
                            _NetspireMediaItems.Add(var);
                            Console.WriteLine("Media Added :" + var);
                        }
                        _NetspireMessages.setPriority(_msgPriority);
                        _NetspireMessages.setPreChime(99200); // first audio 
                        _NetspireMessages.setAudioMessageType(netspire.Message.Type.DICTIONARY);
                        // set new media items                       
                        _NetspireMessages.setAudioMessage(_NetspireMediaItems);
                        // play media items
                        _AudioServer.getPAController().playMessage(_NetspireZone, _NetspireMessages);
                        Console.WriteLine(_Datetime + "Initiate DVA Announcements Succesfully");
                        //clear after play
                        _NetspireZone.Clear();
                        _NetspireMediaItems.Clear();
                    }
                    catch (Exception e)
                    {
                        _logger.Error(_Datetime + e.Message.ToString());
                    }                   
                }
            }
            catch (Exception e)
            {
                _logger.Error(_Datetime + e.Message.ToString());
            }
        }
        /// <summary>
        /// Live Announcements function
        /// </summary>
        public void GetPreDVALiveAnnouncements(List<string> ZoneValue, List<uint> CustomMedia)
        {
            // Check audio server connection
            if (_AudioServer == null || !_AudioServer.isAudioConnected())
            {
                _logger.Error(DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss :") + "CXS Not Connected.....!");
                return;
            }
            try
            {
                //Audio configuration
                netspire.Message NetspireMessages = new netspire.Message();
                StringArray NetspireZone = new StringArray();
                NumberArray NetspireMediaItems = new NumberArray();
                MessagePriority msgPriority = new MessagePriority();
                msgPriority.setPriorityMode(AnnouncementPriorityMode.APM_ABSOLUTE_PRIORITY);
                msgPriority.setPrioritylevel(1000);
                // add new zone
                foreach (string a in ZoneValue)
                {
                    NetspireZone.Add(a);
                }
                // add new media items
                foreach (uint var in CustomMedia)
                {
                    NetspireMediaItems.Add(var);
                }
                // filter if zone exist
                if (NetspireZone.Count > 0)
                {
                    try
                    {
                        NetspireMessages.setPriority(msgPriority);
                        // NetspireMessages.setPreChime(99200); // first audio 
                        NetspireMessages.setAudioMessageType(netspire.Message.Type.DICTIONARY);
                        // set new media items                       
                        NetspireMessages.setAudioMessage(NetspireMediaItems);
                        // play media items
                        _AudioServer.getPAController().playMessage(NetspireZone, NetspireMessages);                   
                    }
                    catch (Exception e)
                    {
                        _logger.Error(_Datetime + e.Message.ToString());
                    }
                }
                //clear after play
                NetspireZone.Clear();
                NetspireMediaItems.Clear();
            }
            catch (Exception e)
            {
                _logger.Error(_Datetime + e.Message.ToString());
            }

        }
        public void GetLiveAnnouncement(int SourceID, List<string> Zone)
        {
            if (_AudioServer == null || !_AudioServer.isAudioConnected())
            {
                _logger.Error(DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss :") + "CXS Not Connected For Live Announcements.....!");
                return;
            }
            // Attach new Source
            AttachSourceID(SourceID);
            // Adding New Zone
            AttachPAZone(SourceID, Zone);
            //CreateSWTrigger
            CreateSWTrigger(SourceID);
            //When created sw trigger then activate it         
            ActivatedPATrigger();

        }
        public PaSource GetPaSource(int MicId)
        {
            foreach (PaSource source in _AudioServer.getPAController().getPaSources())
            {
                if (source.id == MicId)
                    return source;
            }
            return null;
        }
        public void AttachSourceID(int SourceID)
        {
            // Attach PaSource
            _mySourceId = SourceID;
            PAController paController = _AudioServer.getPAController();
            PaSourceArray PAFromServer = paController.getPaSources();
            bool attachSuccess = false;
            for (int i = 0; i < PAFromServer.Count; i++)
            {
                if (PAFromServer.ElementAt(i).id == _mySourceId)
                {
                    if (PAFromServer.Count >= PAFromServer.Count)
                    {
                        Thread.Sleep(500);
                        bool retVal = paController.attachPaSource(_mySourceId);
                        if (retVal)
                        {
                            PAFromServer.ElementAt(i).detachAllPaZones();
                            attachSuccess = true;
                            Console.WriteLine(DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss :") + "Attach Success with source id " + _mySourceId + "");
                            break;
                        }
                    }
                }
            }
            Thread.Sleep(1500);
            if (!attachSuccess)
            {
                Console.WriteLine(DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss :") + "Attach Failed");
                _mySourceId = 0;
                return;
            }
        }
        public void AttachPAZone(int SourceID, List<string> Zone)
        {
            _mySourceId = SourceID;
            PaSourceArray PAFromServer = _AudioServer.getPAController().getPaSources();
            // Get PA Zone
            PaSource paSource = GetPaSource(_mySourceId);
            bool attachSuccess = false;
            for (int i = 0; i < PAFromServer.Count; i++)
            {
                if (PAFromServer.ElementAt(i).id == _mySourceId)
                {
                    if (PAFromServer.Count >= PAFromServer.Count)
                    {
                        foreach (string MyZone in Zone)
                        {
                            // Attach All PA Zone
                            paSource.attachPaZone(MyZone, PaSource.AttachMode.ADD_TO_EXISTING_SET);
                        }
                        attachSuccess = true;
                        Console.WriteLine(DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss :") + "Attach Zone Succesfuly");
                    }
                    Thread.Sleep(1000);
                }                
            }
            Thread.Sleep(2000);
            if (!attachSuccess)
            {
                Console.WriteLine(DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss :") + "Attach Zone Failed");
                return;
            }

        }
        public void ActivatedPATrigger()
        {
            if (_mySWTriggerId <= 0)
            {
                return;
            }
            // check status attached
            bool StatusAttachedPA = false;
            PAController paController = _AudioServer.getPAController();
            PaSourceArray GetAllPA = paController.getPaSources();
            // Get List Attached PA Zone
            for (int i = 0; i < GetAllPA.Count(); i++)
            {
                if (GetAllPA.ElementAt(i).id == _mySourceId)
                {
                    StatusAttachedPA = (GetAllPA.ElementAt(i).getAttachedPaZones().Count() > 0);
                    Console.WriteLine(DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss :") + "Attached to system :" + GetAllPA.ElementAt(i).id.ToString());
                    break;
                }
            }
            // check status if true 
            if (StatusAttachedPA)
            {
                paController.activateSwPaTrigger(_mySWTriggerId);
                Console.WriteLine(DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss :") + "PA Annoucements Active with triggerid " + _mySWTriggerId + "");
            }
            else
            {
                Console.WriteLine(DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss :") + "No PA Zone Attached for Source ID >" + _mySourceId + "<");
                return;
            }
        }
        public void GetDisconnectAnnoucement()
        {
            if (_AudioServer == null || !_AudioServer.isAudioConnected())
            {
                return;
            }
            _AudioServer.getPAController().deactivateSwPaTrigger(_mySWTriggerId);
            Console.WriteLine(DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss :") + "PA Announcements Deactivated SW Trigger Succesfully");
            _AudioServer.getPAController().detachPaSource(_mySourceId);
            Console.WriteLine(DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss :") + "PA Announcements Remove SourceID Succesfully");
            _AudioServer.getPAController().deleteSwPaTrigger(_mySWTriggerId);
            Console.WriteLine(DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss :") + "PA Announcements DeleteSWTrigger Succesfully");
            _mySWTriggerId = 0;
        }
        public void CreateSWTrigger(int SourceID)
        {
            if (SourceID > 0)
            {
                if (_mySWTriggerId == 0)
                {
                    int swPAPriority = 100;
                    PAController paC = _AudioServer.getPAController();
                    _mySWTriggerId = paC.createSwPaTrigger(SourceID, swPAPriority);
                    if (_mySWTriggerId > 0)
                        Console.WriteLine(DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss :") + "created new SW triggerId >" + _mySWTriggerId + "<");
                    else
                        Console.WriteLine(DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss :") + "failed create triggerId");
                }
                else
                {
                    Console.WriteLine(DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss :") + "trigger aleready exist");
                }
            }
        }
        /// <summary>
        /// Generate Station String
        /// </summary>
        public string _GetStationListPID(int State)
        {
            if (State == 2)
            {
                return "Depot";
            }
            if (State == 3)
            {
                return "North Boulevard";
            }
            if (State == 4)
            {
                return "South Boulevard";
            }
            if (State == 5)
            {
                return "Pulomas";
            }
            if (State == 6)
            {
                return "Equestrian";
            }
            if (State == 7)
            {
                return "Velodrome";
            }
            else
            {
                return "Null";
            }
        }
        /// <summary>
        /// PID Template Display
        /// </summary>
        public void AddVisualItemPID(string itemKey, string itemValue, int timeout)
        {
            DISPLAY_VALUE_INFO displayValue = new DISPLAY_VALUE_INFO(itemValue, timeout);
            if (PIDDisplay.ContainsKey(itemKey))
            {
                PIDDisplay[itemKey].Add(displayValue);
            }
            else
            {
                DisplayValueList valueList = new DisplayValueList
                {
                    displayValue
                };
                PIDDisplay.Add(itemKey, valueList);
            }
        }
        /// <summary>
        /// Schedule Signalling PID
        /// </summary>
        public void GetPIDSignallingEachStation(int LocationID,int Direction)
        {            
            List<string> _GetTrainNo = new List<string>();
            List<string> _GetTimeArrival = new List<string>();
            List<string> _GetT1 = new List<string>();
            List<string> _GetT2 = new List<string>();
            List<int> _GetStationPID = new List<int>();
            int[] _countDownTime = new int[2];
            _GetTrainNo.Clear();
            _GetStationPID.Clear();
            _GetTimeArrival.Clear();
            _GetT1.Clear();
            _GetT2.Clear();
            _Con = new Connection();
            _Con.Connected();
            // Get 2 Schedule Arrival and Train Number
            _Con.SqlCmd = new SqlCommand
            {
                Connection = _Con.SqlCon,
                CommandType = CommandType.Text,
                CommandText = "select TOP 2 trainno,ScheduledArrival,[Status] from JourneyStop where LocationId="+ LocationID + " and " +
                "IsCancelled=0 and " +
                "IsPassthorugh=0 and " +
                "Arrival IS NULL and TrainID is not null and " +
                "Direction="+Direction+ " ORDER BY ScheduledArrival ASC "
            };
            _Con.SqlRead = _Con.SqlCmd.ExecuteReader();
            while (_Con.SqlRead.Read())
            {
                _GetTrainNo.Add(_Con.SqlRead.GetString(0));
                _GetTimeArrival.Add(_Con.SqlRead.GetString(1));
            }
            _Con.SqlRead.Close();
            _Con.SqlCmd.Dispose();
            foreach (string a in _GetTrainNo)
            {
                Console.WriteLine("Train No" + a);
            }
            // Get all state depending Train ID PID 1
            _Con.SqlCmd = new SqlCommand()
            {
                Connection = _Con.SqlCon,
                CommandType = CommandType.Text,
                CommandText = "select ScheduledArrival from journeystop where TrainNo = '"+_GetTrainNo[0]+"' " +
                "and IsCancelled = 0 " +
                "and IsPassthorugh = 0 " +
                "and Arrival IS NULL order by[index] asc"
            };
            _Con.SqlRead = _Con.SqlCmd.ExecuteReader();
            while (_Con.SqlRead.Read())
            {
               _GetT1.Add(_Con.SqlRead.GetString(0));
            }
            _Con.SqlRead.Close();
            _Con.SqlCmd.Dispose();
            // Get all state depending Train ID PID 2
            _Con.SqlCmd = new SqlCommand()
            {
                Connection = _Con.SqlCon,
                CommandType = CommandType.Text,
                CommandText = "select ScheduledArrival from journeystop where TrainNo = '" + _GetTrainNo[1] + "' " +
                "and IsCancelled = 0 " +
                "and IsPassthorugh = 0 " +
                "and Arrival IS NULL order by[index] asc"
            };
            _Con.SqlRead = _Con.SqlCmd.ExecuteReader();
            while (_Con.SqlRead.Read())
            {
                _GetT2.Add(_Con.SqlRead.GetString(0));
            }
            _Con.SqlRead.Close();
            _Con.SqlCmd.Dispose();

            // Get Countdown timer Each Station
            // KGM
            int _kgmhour1 = TimeSpan.Parse(_GetT1[0]).Hours;
            int _kgmmin1 = TimeSpan.Parse(_GetT1[0]).Minutes;
            int _kgmsec1 = TimeSpan.Parse(_GetT1[0]).Seconds;

            int _kgmhour2 = TimeSpan.Parse(_GetT1[1]).Hours;
            int _kgmmin2 = TimeSpan.Parse(_GetT1[1]).Minutes;
            int _kgmsec2 = TimeSpan.Parse(_GetT1[1]).Seconds;
            // gede
            TimeSpan _timekgm1 = new TimeSpan(
                       _kgmhour1, _kgmmin1, _kgmsec1);
            // kecil
            TimeSpan _timekgm2 = new TimeSpan(
                        _kgmhour2, _kgmmin2, _kgmsec2).Subtract(_timekgm1);

            // KGB
            int _kgbhour1 = TimeSpan.Parse(_GetT1[1]).Hours;
            int _kgbmin1 = TimeSpan.Parse(_GetT1[1]).Minutes;
            int _kgbsec1 = TimeSpan.Parse(_GetT1[1]).Seconds;

            int _kgbhour2 = TimeSpan.Parse(_GetT1[2]).Hours;
            int _kgbmin2 = TimeSpan.Parse(_GetT1[2]).Minutes;
            int _kgbsec2 = TimeSpan.Parse(_GetT1[2]).Seconds;
            // gede
            TimeSpan _timekgb1 = new TimeSpan(
                       _kgbhour1, _kgbmin1, _kgbsec1);
            // kecil
            TimeSpan _timekgb2 = new TimeSpan(
                        _kgbhour2, _kgbmin2, _kgbsec2).Subtract(_timekgb1);

            // PLM
            int _plmhour1 = TimeSpan.Parse(_GetT1[2]).Hours;
            int _plmmin1 = TimeSpan.Parse(_GetT1[2]).Minutes;
            int _plmsec1 = TimeSpan.Parse(_GetT1[2]).Seconds;

            int _plmhour2 = TimeSpan.Parse(_GetT1[3]).Hours;
            int _plmmin2 = TimeSpan.Parse(_GetT1[3]).Minutes;
            int _plmsec2 = TimeSpan.Parse(_GetT1[3]).Seconds;
            // gede
            TimeSpan _timeplm1 = new TimeSpan(
                       _plmhour1, _plmmin1, _plmsec1);
            // kecil
            TimeSpan _timeplm2 = new TimeSpan(
                        _plmhour2, _plmmin2, _plmsec2).Subtract(_timeplm1);

            // PKD
            int _pkdhour1 = TimeSpan.Parse(_GetT1[3]).Hours;
            int _pkdmin1 = TimeSpan.Parse(_GetT1[3]).Minutes;
            int _pkdsec1 = TimeSpan.Parse(_GetT1[3]).Seconds;

            int _pkdhour2 = TimeSpan.Parse(_GetT1[4]).Hours;
            int _pkdmin2 = TimeSpan.Parse(_GetT1[4]).Minutes;
            int _pkdsec2 = TimeSpan.Parse(_GetT1[4]).Seconds;
            // gede
            TimeSpan _timepkd1 = new TimeSpan(
                       _pkdhour1, _pkdmin1, _pkdsec1);
            // kecil
            TimeSpan _timepkd2 = new TimeSpan(
                        _pkdhour2, _pkdmin2, _pkdsec2).Subtract(_timepkd1);

            // VLD
            int _vldhour1 = TimeSpan.Parse(_GetT1[4]).Hours;
            int _vldmin1 = TimeSpan.Parse(_GetT1[4]).Minutes;
            int _vldsec1 = TimeSpan.Parse(_GetT1[4]).Seconds;

            int _vldhour2 = TimeSpan.Parse(_GetT1[5]).Hours;
            int _vldmin2 = TimeSpan.Parse(_GetT1[5]).Minutes;
            int _vldsec2 = TimeSpan.Parse(_GetT1[5]).Seconds;
            // gede
            TimeSpan _timevld1 = new TimeSpan(
                       _vldhour1, _vldmin1, _vldsec1);
            // kecil
            TimeSpan _timevld2 = new TimeSpan(
                        _vldhour2, _vldmin2, _vldsec2).Subtract(_timevld1);

            // KGM
            int _kgmhour3 = TimeSpan.Parse(_GetT2[0]).Hours;
            int _kgmmin3 = TimeSpan.Parse(_GetT2[0]).Minutes;
            int _kgmsec3 = TimeSpan.Parse(_GetT2[0]).Seconds;

            int _kgmhour4 = TimeSpan.Parse(_GetT2[1]).Hours;
            int _kgmmin4 = TimeSpan.Parse(_GetT2[1]).Minutes;
            int _kgmsec4 = TimeSpan.Parse(_GetT2[1]).Seconds;
            // gede
            TimeSpan _timekgm13 = new TimeSpan(
                       _kgmhour3, _kgmmin3, _kgmsec3);
            // kecil
            TimeSpan _timekgm23 = new TimeSpan(
                        _kgmhour4, _kgmmin4, _kgmsec4).Subtract(_timekgm13);
            // KGB
            int _kgbhour5 = TimeSpan.Parse(_GetT2[1]).Hours;
            int _kgbmin5 = TimeSpan.Parse(_GetT2[1]).Minutes;
            int _kgbsec5 = TimeSpan.Parse(_GetT2[1]).Seconds;

            int _kgbhour6 = TimeSpan.Parse(_GetT2[2]).Hours;
            int _kgbmin6 = TimeSpan.Parse(_GetT2[2]).Minutes;
            int _kgbsec6 = TimeSpan.Parse(_GetT2[2]).Seconds;
            // gede
            TimeSpan _timekgb3= new TimeSpan(
                       _kgbhour5, _kgbmin5, _kgbsec5);
            // kecil
            TimeSpan _timekgb4 = new TimeSpan(
                        _kgbhour6, _kgbmin6, _kgbsec6).Subtract(_timekgb3);
            // PLM
            int _plmhour7 = TimeSpan.Parse(_GetT1[2]).Hours;
            int _plmmin7 = TimeSpan.Parse(_GetT1[2]).Minutes;
            int _plmsec7 = TimeSpan.Parse(_GetT1[2]).Seconds;

            int _plmhour8 = TimeSpan.Parse(_GetT1[3]).Hours;
            int _plmmin8 = TimeSpan.Parse(_GetT1[3]).Minutes;
            int _plmsec8 = TimeSpan.Parse(_GetT1[3]).Seconds;
            // gede
            TimeSpan _timeplm3 = new TimeSpan(
                       _plmhour7, _plmmin7, _plmsec7);
            // kecil
            TimeSpan _timeplm4 = new TimeSpan(
                        _plmhour8, _plmmin8, _plmsec8).Subtract(_timeplm3);
            // PKD
            int _pkdhour9 = TimeSpan.Parse(_GetT1[3]).Hours;
            int _pkdmin9 = TimeSpan.Parse(_GetT1[3]).Minutes;
            int _pkdsec9 = TimeSpan.Parse(_GetT1[3]).Seconds;

            int _pkdhour10 = TimeSpan.Parse(_GetT1[4]).Hours;
            int _pkdmin10 = TimeSpan.Parse(_GetT1[4]).Minutes;
            int _pkdsec10 = TimeSpan.Parse(_GetT1[4]).Seconds;
            // gede
            TimeSpan _timepkd3 = new TimeSpan(
                       _pkdhour9, _pkdmin9, _pkdsec9);
            // kecil
            TimeSpan _timepkd4 = new TimeSpan(
                        _pkdhour10, _pkdmin10, _pkdsec10).Subtract(_timepkd3);

            // VLD
            int _vldhour9 = TimeSpan.Parse(_GetT1[4]).Hours;
            int _vldmin9 = TimeSpan.Parse(_GetT1[4]).Minutes;
            int _vldsec9 = TimeSpan.Parse(_GetT1[4]).Seconds;

            int _vldhour10 = TimeSpan.Parse(_GetT1[5]).Hours;
            int _vldmin10 = TimeSpan.Parse(_GetT1[5]).Minutes;
            int _vldsec10 = TimeSpan.Parse(_GetT1[5]).Seconds;
            // gede
            TimeSpan _timevld3 = new TimeSpan(
                       _vldhour9, _vldmin9, _vldsec9);
            // kecil
            TimeSpan _timevld4 = new TimeSpan(
                        _vldhour10, _vldmin10, _vldsec10).Subtract(_timevld3);

            if (LocationID.Equals(3))
            {
                _countDownTime[0] = _timekgm2.Minutes;
                _countDownTime[1] = _timekgm23.Minutes;
            }
            else if (LocationID.Equals(4))
            {
                _countDownTime[0] = _timekgb2.Add(_timekgm2).Minutes;
                _countDownTime[1] = _timekgb4.Add(_timekgm23).Minutes;
            }
            else if (LocationID.Equals(5))
            {
                _countDownTime[0] = _timeplm2.Add(_timekgb2).Add(_timekgm2).Minutes;
                _countDownTime[1] = _timeplm4.Add(_timekgb4).Add(_timekgm23).Minutes;
            }
            else if (LocationID.Equals(6))
            {
                _countDownTime[0] = _timepkd2.Add(_timekgb2).Add(_timekgm2).Add(_timeplm2).Minutes;
                _countDownTime[1] = _timepkd4.Add(_timekgb4).Add(_timekgm23).Add(_timeplm4).Minutes;
            }
            else if (LocationID.Equals(7))
            {
                _countDownTime[0] = _timevld2.Add(_timekgb2).Add(_timekgm2).Add(_timeplm2).Add(_timepkd2).Minutes;
                _countDownTime[1] = _timevld4.Add(_timekgb4).Add(_timekgm23).Add(_timeplm4).Add(_timepkd4).Minutes;
            }

            // if (Direction=1)
            //if (_Hours > 24)
            //{
            //    _Hours = (_Hours - 24) * 60;
            //    _countDownTime[0] = _Minutes+_Hours;
            //}
            //else
            //{
            //    TimeSpan _TimeNormal = new TimeSpan(
            //            _T1).Subtract(_TimeNow);
            //    _countDownTime[0] = _Minutes;
            //}

            //int _Hour = TimeSpan.Parse(_GetTimeArrival[1]).Hours;
            //int _Minute = TimeSpan.Parse(_GetTimeArrival[1]).Minutes;
            //int _Second = TimeSpan.Parse(_GetTimeArrival[1]).Seconds;
            //if (_Hour > 24)
            //{
            //    _Hour = (_Hour - 24) * 60;
            //    _countDownTime[1] = _Minute + _Hour;
            //}
            //else
            //{
            //    _countDownTime[1] = _Minute;
            //}

            // Get Last Destination Station
            _Con.SqlCmd = new SqlCommand()
            {
                Connection = _Con.SqlCon,
                CommandType = CommandType.Text,
                CommandText = "select  top 2  destination from Journey where  " +
                "TrainNo='" + _GetTrainNo[0] + "' or " +
                "TrainNo='" + _GetTrainNo[1] + "' order by TrainNo"
            };
            _Con.SqlRead = _Con.SqlCmd.ExecuteReader();
            while (_Con.SqlRead.Read())
            {
                _GetStationPID.Add(_Con.SqlRead.GetInt32(0));
            }
            _Con.SqlRead.Close();
            _Con.SqlCmd.Dispose();

            // Line Left Station and Line Right Value 1 PID
            _DetailTemplate.Add(_TemplateTmp + "^Line1Left", "" + _GetStationListPID(_GetStationPID[0]) + "");
            if (_countDownTime[0] < 10 && _countDownTime[0] > 0)
            {
                _DetailTemplate.Add(_TemplateTmp + "^Line1Right", "" + "0" + _countDownTime[0] + " Min ");
            }
            else if (_countDownTime[0] <= 0)
            {
                _DetailTemplate.Add(_TemplateTmp + "^Line1Right", "" + "0" + 1 + " Min ");
            }
            else
            {
                _DetailTemplate.Add(_TemplateTmp + "^Line1Right", "" + _countDownTime[0] + " Min ");
            }             
            // Line Left Station and Line Right Value 2 PID
            _DetailTemplate.Add(_TemplateTmp + "^Line2Left", "" + _GetStationListPID(_GetStationPID[1]) + "");
            if (_countDownTime[1] < 10 && _countDownTime[1] > 0)
            {
                _DetailTemplate.Add(_TemplateTmp + "^Line2Right", "" + "0" + _countDownTime[1] + " Min ");
            }
            else if (_countDownTime[1] <= 0)
            {
                _DetailTemplate.Add(_TemplateTmp + "^Line2Right", "" + "0" + 1 + " Min ");
            }
            else
            {
                _DetailTemplate.Add(_TemplateTmp + "^Line2Right", "" + _countDownTime[1] + " Min ");
            }
            // Get All Settings key For PID
            foreach (string key in _DetailTemplate.Keys)
            {
                AddVisualItemPID(key, _DetailTemplate[key], 0);
            }
            Console.WriteLine(DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss :") +
                "SIG: " + _GetStationListPID(_GetStationPID[0]) +
                "," + _GetStationListPID(_GetStationPID[1]) +
                " T1:" + _countDownTime[0] + "Min" +
                " T2:" + _countDownTime[1] + "Min");
            _DetailTemplate.Clear();
            _Con.SqlCon.Close();
        }
        /// <summary>
        /// Countdown Timer Services for PID's
        /// </summary>
        private void _countDownTimer_ElapsedSIG(object sender, ElapsedEventArgs e)
        {
            if (_AudioServer == null || !_AudioServer.isAudioConnected())
            {
                return;
            }
            List<string> _VLD1 = new List<string>
            {
                "PID VEL/Concourse/Concourse 1",
                "PID VEL/Platform/Platform 1"
            };
            List<string> _VLD2 = new List<string>
            {
                "PID VEL/Concourse/Concourse 2",
                "PID VEL/Platform/Platform 2"
            };
            List<string> _EQU1 = new List<string>
            {
                "PID PACUANKUDA/Concourse/Concourse 1",
                "PID PACUANKUDA/Platform/Platform 1"
            };
            List<string> _EQU2 = new List<string>
            {
                "PID PACUANKUDA/Concourse/Concourse 2",
                "PID PACUANKUDA/Platform/Platform 2"
            };
            List<string> _PLM1 = new List<string>
            {
                "PID PULOMAS/Concourse/Concourse 1",
                "PID PULOMAS/Platform/Platform 1"
            };
            List<string> _PLM2 = new List<string>
            {
                "PID PULOMAS/Concourse/Concourse 2",
                "PID PULOMAS/Platform/Platform 2"
            };
            List<string> _BLU1 = new List<string>
            {
                "PID KGB/Concourse/Concourse 1",
                "PID KGB/Platform/Platform 1"
            };
            List<string> _BLU2 = new List<string>
            {
                "PID KGB/Concourse/Concourse 2",
                "PID KGB/Platform/Platform 2"
            };
            List<string> _BLS1 = new List<string>
            {
                "PID KGM/Concourse/Concourse 1",
                "PID KGM/Platform/Platform 1"
            };
            List<string> _BLS2 = new List<string>
            {
                "PID KGM/Concourse/Concourse 2",
                "PID KGM/Platform/Platform 2"
            };
            List<string> _DPT1 = new List<string>
            {
                "PID DPT/Concourse/Concourse 1",
                "PID DPT/Platform/Platform 1"
            };
            List<string> _DPT2 = new List<string>
            {
                "PID DPT/Concourse/Concourse 2",
                "PID DPT/Platform/Platform 2"
            };
            try
            {
                //  Depot, Format: Zone,StationIndex,Direction
                // Platform 2
                Console.WriteLine("===========Platform 1============");
                Console.WriteLine("Line 1 Velodrome");
                _GetPlayPIDSignalling("Display", _VLD1, 7, 1);
                Console.WriteLine("Line 1 Equestrian");
                _GetPlayPIDSignalling("Display", _EQU1, 6, 1);
                Console.WriteLine("Line 1 Pulomas");
                _GetPlayPIDSignalling("Display", _PLM1, 5, 1);
                Console.WriteLine("Line 1 Kelapa Gading Boulevard");
                _GetPlayPIDSignalling("Display", _BLU1, 4, 1);
                Console.WriteLine("Line 1 Kelapa Gading Mall");
                _GetPlayPIDSignalling("Display", _BLS1, 3, 1);
                Console.WriteLine("Line 1 Depot");
                _GetPlayPIDSignalling("Display", _DPT1, 2, 1);
                // Platform 1
                Console.WriteLine("===========Platform 2============");
                Console.WriteLine("Line 2 Depot");
                _GetPlayPIDSignalling("Display", _DPT2, 2, 2);
                Console.WriteLine("Line 2 Kelapa Gading Mall");
                _GetPlayPIDSignalling("Display", _BLS2, 3, 2);
                Console.WriteLine("Line 2 Kelapa Gading Boulevard");
                _GetPlayPIDSignalling("Display", _BLU2, 4, 2);
                Console.WriteLine("Line 2 Pulomas");
                _GetPlayPIDSignalling("Display", _PLM2, 5, 2);
                Console.WriteLine("Line 2 Equestrian");
                _GetPlayPIDSignalling("Display", _EQU2, 6, 2);
                Console.WriteLine("Line 2 Velodrome");
                _GetPlayPIDSignalling("Display", _VLD2, 7, 2);
            }
            catch (Exception ex)
            {
                _logger.Error(_Datetime + ex.Message.ToString());
            }
        }
        /// <summary>
        /// PID Service Signalling, Get Schedule
        /// </summary>
        private void _GetPlayPIDSignalling(string _Template, List<string> _ZoneValue, int _Location,int _Direction)
        {
            // Display List
            PIDDisplay = new DisplayVariableValueMap();
            _TemplateTmp = _Template;
            StringArray _NetspireZone = new StringArray();
            // Select Message type
            netspire.Message _NetspireMessages = new netspire.Message();
            _NetspireMessages.setVisualMessageType(netspire.Message.Type.DISPLAY_TEMPLATE);
            // Select Priority
            MessagePriority msgPriority = new MessagePriority();
            msgPriority.setPriorityMode(AnnouncementPriorityMode.APM_RELATIVE_PRIORITY);
            msgPriority.setPrioritylevel(0);
            // Add new zone
            try
            {
                foreach (string a in _ZoneValue)
                {
                    _NetspireZone.Add(a);                   
                }
                // Filter new zone
                if (_NetspireZone.Count <= 0)
                {
                    return;
                }
                // Format : LocationID , Direction: East/West
                GetPIDSignallingEachStation(_Location, _Direction);
                // Create Template, Key+Value, Timeout
                _NetspireMessages.setVisualMessage("", PIDDisplay, 0);
                // Execute PID
                string RequestID = _AudioServer.getPAController().playMessage(_NetspireZone, _NetspireMessages);
                // Console.WriteLine(DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss :") + "Refresh PID! With RequestId : " + RequestID + "");
                _NetspireMessages.Dispose();
                PIDDisplay.Clear();
                PIDDisplay.Dispose();
            }
            catch (Exception e)
            {
                _logger.Error(_Datetime + e.Message.ToString());
            }
        }
        /// <summary>
        /// PA Service From Signalling
        /// </summary>
        public void _GetDVASignalling(List<string> StationZone, List<uint> CustomMedia)
        {
            // Check audio server connection
            if (_AudioServer == null || !_AudioServer.isAudioConnected())
            {
                _logger.Error(DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss :") + "CXS Not Connected.....!");
                return;
            }
            try
            {
                //Audio configuration
                netspire.Message _NetspireMessages = new netspire.Message();
                StringArray _NetspireZone = new StringArray();
                NumberArray _NetspireMediaItems = new NumberArray();
                MessagePriority _msgPriority = new MessagePriority();
                _msgPriority.setPriorityMode(AnnouncementPriorityMode.APM_RELATIVE_PRIORITY);
                _msgPriority.setPrioritylevel(950);
                // add new zone
                foreach (string a in StationZone)
                {
                    //Thread.Sleep(100);
                    _NetspireZone.Add(a);
                    Console.WriteLine("Zone Added :" + a);
                }
                // filter if zone exist
                if (_NetspireZone.Count > 0)
                {
                    try
                    {
                        // add new media items
                        foreach (uint var in CustomMedia)
                        {
                            _NetspireMediaItems.Add(var);
                            Console.WriteLine("Media Added :" + var);
                        }
                        _NetspireMessages.setPriority(_msgPriority);
                        _NetspireMessages.setPreChime(99200); // first audio 
                        _NetspireMessages.setAudioMessageType(netspire.Message.Type.DICTIONARY);
                        // set new media items                       
                        _NetspireMessages.setAudioMessage(_NetspireMediaItems);
                        // play media items
                        _AudioServer.getPAController().playMessage(_NetspireZone, _NetspireMessages);
                        Console.WriteLine(_Datetime + "Initiate DVA Signalling Succesfully");
                        //clear after play
                        _NetspireZone.Clear();
                        _NetspireMediaItems.Clear();
                    }
                    catch (Exception e)
                    {
                        _logger.Error(_Datetime + e.Message.ToString());
                    }
                }
            }
            catch (Exception e)
            {
                _logger.Error(_Datetime + e.Message.ToString());
            }

        }
        /// <summary>
        /// PID Text Information Line
        /// </summary>  
        public void _GetPlayPIDText(string Template, List<string> ZoneList, string ObjectValue)
        {
            StringArray _OANetspireZone = new StringArray();
            netspire.Message NetspireMessages = new netspire.Message();
            // Display List
            PIDDisplay = new DisplayVariableValueMap();
            // Select Message type
            NetspireMessages = new netspire.Message();
            NetspireMessages.setVisualMessageType(netspire.Message.Type.DISPLAY_TEMPLATE);
            // Select Priority
            MessagePriority msgPriority = new MessagePriority();
            msgPriority.setPriorityMode(AnnouncementPriorityMode.APM_RELATIVE_PRIORITY);
            msgPriority.setPrioritylevel(0);
            // add new zone
            try
            {
                foreach (string _ZoneList in ZoneList)
                {
                    _OANetspireZone.Add(_ZoneList);                  
                }
                // filter new zone
                if (_OANetspireZone.Count <= 0)
                {
                    return;
                }
                // Add Line 3 to PID template and replace the new one Text
                _DetailTemplate.Add(Template + "^Line3", "" + ObjectValue);
                foreach (string key in _DetailTemplate.Keys)
                {
                    AddVisualItemPID(key, _DetailTemplate[key], 0);
                }
                _DetailTemplate.Clear();
                // Template,Key+Value,Timeout
                NetspireMessages.setVisualMessage("", PIDDisplay, 0);
                // Execute PID
                _AudioServer.getPAController().playMessage(_OANetspireZone, NetspireMessages);
                Console.WriteLine(DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss :") + "Initiate PID Display with Text : "+ ObjectValue + "");
                PIDDisplay.Clear();
            }
            catch (Exception e)
            {
                _logger.Error(_Datetime + e.Message.ToString());
            }

        }
    }
}
