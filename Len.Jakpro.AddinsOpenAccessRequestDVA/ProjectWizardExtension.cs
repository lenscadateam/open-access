﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;
using Len.Jakpro.Database.Rendundancy;
using NLog;
using Scada.AddIn.Contracts;
using SimpleTCP;

namespace Len.Jakpro.AddinsOpenAccessRequestDVA
{
    /// <summary>
    /// Description of Project Wizard Extension.
    /// </summary>
    [AddInExtension("OA Driver DVA Audio", "OA Driver DVA Audio Control")]
    public class ProjectWizardExtension : IProjectWizardExtension
    {
        #region IProjectWizardExtension implementation
        private static readonly string _Datetime = DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss : ");
        private List<string> _ZoneList = new List<string>();
        private static readonly string _IPaddrSendZone = "10.202.91.4";
        private static readonly int _PortSendZone = 1504;

        // Get Zone PA From DB
        private static List<string> _PAVLDZoneVariables = new List<string>();
        private static List<string> _PAEQUZoneVariables = new List<string>();
        private static List<string> _PAPLMZoneVariables = new List<string>();
        private static List<string> _PABLUZoneVariables = new List<string>();
        private static List<string> _PABLSZoneVariables = new List<string>();
        private static List<string> _PADPDZoneVariables = new List<string>();
        // Logger
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        //db
        private Connection _Con;
        private string _MessageStation;
        private bool _HasValidationState = false;
        IProject context;
        public void Run(IProject context, IBehavior behavior)
        {
            // enter your code which should be executed on triggering the function "Execute Project Wizard Extension" in the SCADA Runtime
            this.context = context;

            _GetZoneList("OA_GetPAZoneVLD", _PAVLDZoneVariables);
            _GetZoneList("OA_GetPAZoneEQU", _PAEQUZoneVariables);
            _GetZoneList("OA_GetPAZonePLM", _PAPLMZoneVariables);
            _GetZoneList("OA_GetPAZoneBLU", _PABLUZoneVariables);
            _GetZoneList("OA_GetPAZoneBLS", _PABLSZoneVariables);
            _GetZoneList("OA_GetPAZoneDPD", _PADPDZoneVariables);
            /// <summary>
            /// Function to Send a command PA to OA Driver
            /// </summary>

            SimpleTcpClient _SendDVAAudio, _SendZoneList;
            // split value
            string[] _Items = context.VariableCollection["OA.WPF.GetValueDVAMedia"].GetValue(0).ToString().Split(',');
            // set the commands
            try
            {
                _SendDVAAudio = new SimpleTcpClient().Connect(_IPaddrSendZone, _PortSendZone);
                _SendZoneList = new SimpleTcpClient().Connect(_IPaddrSendZone, _PortSendZone);
                _ZoneList.Clear();
                // Add new Zone
                _VelodromeZone();
                _EquistrianZone();
                _PulomasZone();
                _BoulevardUtaraZone();
                _BoulevardSelatanZone();

                int _count = 1;
                _MessageStation = "ZONE,";
                foreach (string _zone in _ZoneList)
                {
                    if (_count == _ZoneList.Count)
                    {
                        // Last Data
                        _MessageStation = _MessageStation + _zone;
                    }
                    else
                    {
                        // Insert New Data
                        _MessageStation = _MessageStation + _zone + ",";
                    }
                    _count++;
                }
                if (_MessageStation != "ZONE,")
                {
                    _SendZoneList.Write(_MessageStation);
                    Thread.Sleep(500);
                    _SendDVAAudio.Write("PADVAAudio," + _Items[0] + "," + _Items[1] + "");
                    _SendZoneList.Dispose();
                    _SendDVAAudio.Dispose();
                }
                context.VariableCollection["OA.DVAAnnouncements"].SetValue(0, 0);
                _logger.Debug(_Datetime + "Request DVA Announcements");
            }
            catch (SocketException ex)
            {
                _logger.Error(_Datetime + ex.Message.ToString());
            }

            /// <summary>
            /// Function to Send a command PID Text to OA Driver
            /// </summary>
        }
        private void _VelodromeZone()
        {
            // Public Area
            if (context.VariableCollection["OA.PA.VLD.Line.1"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.VLD.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[0]);
            }
            if (context.VariableCollection["OA.PA.VLD.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.VLD.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[1]);
            }
            if (context.VariableCollection["OA.PA.VLD.Line.3"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.VLD.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[2]);
            }
            if (context.VariableCollection["OA.PA.VLD.Line.4"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.VLD.Line.4.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[3]);
            }
            // Non Public Area
            if (context.VariableCollection["OA.NPA.VLD.Line.1"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.VLD.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[4]);
            }
            if (context.VariableCollection["OA.NPA.VLD.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.VLD.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[5]);
            }
            if (context.VariableCollection["OA.NPA.VLD.Line.3"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.VLD.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[6]);
            }
            if (context.VariableCollection["OA.NPA.VLD.Line.4"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.VLD.Line.4.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[7]);
            }
            if (context.VariableCollection["OA.NPA.VLD.Line.5"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.VLD.Line.5.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[8]);
            }
            // Platform
            if (context.VariableCollection["OA.PF.VLD.Line.1"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PF.VLD.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[9]);
            }
            if (context.VariableCollection["OA.PF.VLD.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PF.VLD.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[10]);
            }
            if (context.VariableCollection["OA.PF.VLD.Line.3"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PF.VLD.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[11]);
            }
            // AFIL
            if (context.VariableCollection["OA.AFIL.VLD.Line.1"].GetValue(0).ToString() == "1" &&
              context.VariableCollection["OA.AFIL.VLD.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[12]);
            }
            if (context.VariableCollection["OA.AFIL.VLD.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.AFIL.VLD.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[13]);
            }
        }
        private void _EquistrianZone()
        {
            // Public Area
            if (context.VariableCollection["OA.PA.EQU.Line.1"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.EQU.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[0]);
            }
            if (context.VariableCollection["OA.PA.EQU.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.EQU.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[1]);
            }
            if (context.VariableCollection["OA.PA.EQU.Line.3"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.EQU.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[2]);
            }
            if (context.VariableCollection["OA.PA.EQU.Line.4"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.EQU.Line.4.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[3]);
            }
            if (context.VariableCollection["OA.PA.EQU.Line.5"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.EQU.Line.5.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[4]);
            }
            // Non Public Area
            if (context.VariableCollection["OA.NPA.EQU.Line.1"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.EQU.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[5]);
            }
            if (context.VariableCollection["OA.NPA.EQU.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.EQU.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[6]);
            }
            if (context.VariableCollection["OA.NPA.EQU.Line.3"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.EQU.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[7]);
            }
            if (context.VariableCollection["OA.NPA.EQU.Line.4"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.EQU.Line.4.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[8]);
            }
            if (context.VariableCollection["OA.NPA.EQU.Line.5"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.EQU.Line.5.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[9]);
            }
            // Platform
            if (context.VariableCollection["OA.PF.EQU.Line.1"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PF.EQU.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[10]);
            }
            if (context.VariableCollection["OA.PF.EQU.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PF.EQU.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[11]);
            }
            // AFIL
            if (context.VariableCollection["OA.AFIL.EQU.Line.1"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.AFIL.EQU.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[12]);
            }
            if (context.VariableCollection["OA.AFIL.EQU.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.AFIL.EQU.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[13]);
            }
        }
        private void _PulomasZone()
        {
            // Public Area
            if (context.VariableCollection["OA.PA.PLM.Line.1"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.PLM.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[0]);
            }
            if (context.VariableCollection["OA.PA.PLM.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.PLM.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[1]);
            }
            if (context.VariableCollection["OA.PA.PLM.Line.3"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.PLM.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[2]);
            }
            if (context.VariableCollection["OA.PA.PLM.Line.4"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.PLM.Line.4.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[3]);
            }
            // non Public area
            if (context.VariableCollection["OA.NPA.PLM.Line.1"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.PLM.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[4]);
            }
            if (context.VariableCollection["OA.NPA.PLM.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.PLM.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[5]);
            }
            if (context.VariableCollection["OA.NPA.PLM.Line.3"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.PLM.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[6]);
            }
            if (context.VariableCollection["OA.NPA.PLM.Line.4"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.PLM.Line.4.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[7]);
            }
            if (context.VariableCollection["OA.NPA.PLM.Line.5"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.PLM.Line.5.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[8]);
            }
            if (context.VariableCollection["OA.NPA.PLM.Line.6"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.PLM.Line.6.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[9]);
            }
            // platform
            if (context.VariableCollection["OA.PF.PLM.Line.1"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PF.PLM.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[10]);
            }
            if (context.VariableCollection["OA.PF.PLM.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PF.PLM.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[11]);
            }
            // AFIL
            if (context.VariableCollection["OA.AFIL.PLM.Line.1"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.AFIL.PLM.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[12]);
            }
            if (context.VariableCollection["OA.AFIL.PLM.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.AFIL.PLM.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[13]);
            }

        }
        private void _BoulevardUtaraZone()
        {
            // Public Area
            if (context.VariableCollection["OA.PA.BLU.Line.1"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.BLU.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[0]);
            }
            if (context.VariableCollection["OA.PA.BLU.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.BLU.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[1]);
            }
            if (context.VariableCollection["OA.PA.BLU.Line.3"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.BLU.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[2]);
            }
            if (context.VariableCollection["OA.PA.BLU.Line.4"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.BLU.Line.4.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[3]);
            }
            // Non Public Area
            if (context.VariableCollection["OA.NPA.BLU.Line.1"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.BLU.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[4]);
            }
            if (context.VariableCollection["OA.NPA.BLU.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.BLU.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[5]);
            }
            if (context.VariableCollection["OA.NPA.BLU.Line.3"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.BLU.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[6]);
            }
            if (context.VariableCollection["OA.NPA.BLU.Line.4"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.BLU.Line.4.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[7]);
            }
            if (context.VariableCollection["OA.NPA.BLU.Line.5"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.BLU.Line.5.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[8]);
            }
            // Platform
            if (context.VariableCollection["OA.PF.BLU.Line.1"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PF.BLU.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[9]);
            }
            if (context.VariableCollection["OA.PF.BLU.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PF.BLU.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[10]);
            }
            if (context.VariableCollection["OA.PF.BLU.Line.3"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PF.BLU.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[11]);
            }
            // AFIL
            if (context.VariableCollection["OA.AFIL.BLU.Line.1"].GetValue(0).ToString() == "1" &&
               context.VariableCollection["OA.AFIL.BLU.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[12]);
            }
            if (context.VariableCollection["OA.AFIL.BLU.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.AFIL.BLU.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[13]);
            }
        }
        private void _BoulevardSelatanZone()
        {
            // Public Area
            if (context.VariableCollection["OA.PA.BLS.Line.1"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.BLS.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[0]);
            }
            if (context.VariableCollection["OA.PA.BLS.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.BLS.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[1]);
            }
            if (context.VariableCollection["OA.PA.BLS.Line.3"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.BLS.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[2]);
            }
            // Non Public Area
            if (context.VariableCollection["OA.NPA.BLS.Line.1"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.BLS.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[3]);
            }
            if (context.VariableCollection["OA.NPA.BLS.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.BLS.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[4]);
            }
            if (context.VariableCollection["OA.NPA.BLS.Line.3"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.BLS.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[5]);
            }
            if (context.VariableCollection["OA.NPA.BLS.Line.4"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.BLS.Line.4.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[6]);
            }
            if (context.VariableCollection["OA.NPA.BLS.Line.5"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.BLS.Line.5.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[7]);
            }
            if (context.VariableCollection["OA.NPA.BLS.Line.6"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.BLS.Line.6.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[8]);
            }
            // Platform
            if (context.VariableCollection["OA.PF.BLS.Line.1"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PF.BLS.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[9]);
            }
            if (context.VariableCollection["OA.PF.BLS.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PF.BLS.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[10]);
            }
            if (context.VariableCollection["OA.PF.BLS.Line.3"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PF.BLS.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[11]);
            }
            // AFIL
            if (context.VariableCollection["OA.AFIL.BLS.Line.1"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.AFIL.BLS.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[12]);
            }
            if (context.VariableCollection["OA.AFIL.BLS.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.AFIL.BLS.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[13]);
            }
        }

        private void _GetZoneList(string QueryProcedure, List<string> _TempVariables)
        {
            try
            {
                _Con = new Connection();
                _Con.Connected();
                _Con.SqlCmd = new SqlCommand
                {
                    Connection = _Con.SqlCon,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = QueryProcedure
                };
                _Con.SqlRead = _Con.SqlCmd.ExecuteReader();
                while (_Con.SqlRead.Read())
                {
                    _TempVariables.Add(_Con.SqlRead.GetString(3));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(_Datetime + ex.Message.ToString());
            }

            if (!_Con.SqlRead.IsClosed)
            {
                _Con.SqlRead.Close();
            }
        }
        #endregion
    }

}