﻿using System;
using System.Data.SqlClient;
using Len.Jakpro.Logging;
using Microsoft.Win32;
using NLog;
namespace Len.Jakpro.Database.Rendundancy
{
    public class Connection
    {
        private static readonly string _Datetime = DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss : ");
        public SqlCommand SqlCmd;
        public SqlConnection SqlCon;
        public SqlDataReader SqlRead;

        private string _PrimaryServerName;
        private string _SecondaryServerName;
        private string _DbName;
        private string _UserName;
        private string _Password; 
        
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        public Connection()
        {
            var _logging = new NLogConfigurator();
            _logging.Configure();
            //opening the subkey  
            RegistryKey _key = Registry.CurrentUser.OpenSubKey("Software\\Len.Jakpro.OpenAccess\\Server");
            //if it does exist, retrieve the stored values
            if (_key != null)
            {
                _PrimaryServerName= _key.GetValue("ServerPrimary").ToString();
                _SecondaryServerName = _key.GetValue("ServerSecondary").ToString();
                _DbName = _key.GetValue("Database").ToString();
                _UserName = _key.GetValue("Username").ToString();
                _Password = _key.GetValue("Password").ToString();
                _key.Close();
            }
        }
        public void Connected()
        {
            
           SqlCon = new SqlConnection("server=" + _PrimaryServerName + "\\ZENON_2012;" +
           "database=" + _DbName + ";" +
           "user id=" + _UserName + ";" +
           "password=" + _Password + ";");
           try
           {
               SqlCon.Open();
           }
           catch (Exception ex)
           {
               _logger.Error(_Datetime + ex.Message.ToString());
           }
        }
        public void Disconnected()
        {
            try
            {
                SqlCon.Close();
                SqlCmd.Dispose();
                SqlRead.Close();
            }
            catch (Exception ex)
            {
                _logger.Error(_Datetime + ex.Message.ToString());
            }

        }
    }
}
