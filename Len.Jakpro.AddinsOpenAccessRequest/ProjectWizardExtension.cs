﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;
using NLog;
using Scada.AddIn.Contracts;
using SimpleTCP;

namespace Len.Jakpro.AddinsOpenAccessRequest
{
    /// <summary>
    /// Description of Project Wizard Extension.
    /// </summary>
    [AddInExtension("OA Driver Control OCC and BCC", "OA driver Control OCC and BCC")]
    public class ProjectWizardExtension : IProjectWizardExtension
    {
        #region IProjectWizardExtension implementation

        private static readonly string _Datetime = DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss : ");
        private List<string> _ZoneList = new List<string>();
        private static readonly string _IPaddrSendZone = "10.202.91.4";
        private static readonly int _PortSendZone = 1504;
        // Get Zone PID From DB
        private static List<string> _PIDVLDZoneVariables = new List<string>();
        private static List<string> _PIDEQUZoneVariables = new List<string>();
        private static List<string> _PIDPLMZoneVariables = new List<string>();
        private static List<string> _PIDBLUZoneVariables = new List<string>();
        private static List<string> _PIDBLSZoneVariables = new List<string>();
        private static List<string> _PIDDPDZoneVariables = new List<string>();
        // Get Zone PA From DB
        private static List<string> _PAVLDZoneVariables = new List<string>();
        private static List<string> _PAEQUZoneVariables = new List<string>();
        private static List<string> _PAPLMZoneVariables = new List<string>();
        private static List<string> _PABLUZoneVariables = new List<string>();
        private static List<string> _PABLSZoneVariables = new List<string>();
        private static List<string> _PADPDZoneVariables = new List<string>();
        // Logger
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private string _MessageStation;
        private bool _HasValidationState = false;
        IProject context;
        public void Run(IProject context, IBehavior behavior)
        {
            // enter your code which should be executed on triggering the function "Execute Project Wizard Extension" in the SCADA Runtime
            this.context = context;
            SimpleTcpClient _SendZone, _SendCommand, _SendEndCommand;
            /// <summary>
            /// Function to Send a command Microphone to OA Driver
            /// </summary>
            if (context.VariableCollection["OA.Microphone.PA"].GetValue(0).ToString() == "1")
            {
                _SendZone = new SimpleTcpClient().Connect(_IPaddrSendZone, _PortSendZone);
                _SendCommand = new SimpleTcpClient().Connect(_IPaddrSendZone, _PortSendZone);
                _ZoneList.Clear();
                // Add new Zone
                _VelodromeZone();
                _EquistrianZone();
                _PulomasZone();
                _BoulevardUtaraZone();
                _BoulevardSelatanZone();

                int _count = 1;
                _MessageStation = "ZONE,";
                foreach (string _zone in _ZoneList)
                {
                    if (_count == _ZoneList.Count)
                    {
                        // Last Data
                        _MessageStation = _MessageStation + _zone;
                    }
                    else
                    {
                        // Insert New Data
                        _MessageStation = _MessageStation + _zone + ",";
                    }
                    _count++;
                }
                try
                {
                    if (_MessageStation != "ZONE,")
                    {
                        _SendZone.Write(_MessageStation);
                        _SendCommand.Write("PAActive");
                        MessageBox.Show("muncul");
                        _SendZone.Dispose();
                        _SendCommand.Dispose();
                    }
                    _HasValidationState = true;
                    _logger.Debug(_Datetime + "Request PA Annoucements");
                }
                catch (Exception ex)
                {
                    _logger.Error(_Datetime + ex.Message.ToString());
                }
            }

            else if (context.VariableCollection["OA.Microphone.PA"].GetValue(0).ToString() == "0")
            {
                if (_HasValidationState)
                {
                    try
                    {
                        _SendEndCommand = new SimpleTcpClient().Connect(_IPaddrSendZone, _PortSendZone);
                        _SendEndCommand.Write("PAInactive");
                        _SendEndCommand.Dispose();
                        _HasValidationState = false;

                        _logger.Debug(_Datetime + "End PA Announcements");
                    }
                    catch (SocketException ex)
                    {
                        _logger.Error(_Datetime + ex.Message.ToString());
                    }
                }
            }
            /// <summary>
            /// Function to Send a command PA to OA Driver
            /// </summary>
            if (context.VariableCollection["OA.DVAAnnouncements"].GetValue(0).ToString() == "1")
            {
                SimpleTcpClient _SendDVAAudio, _SendZoneList;
                // split value
                string[] _Items = context.VariableCollection["OA.WPF.GetValueDVAMedia"].GetValue(0).ToString().Split(',');
                // set the commands
                try
                {
                    _SendDVAAudio = new SimpleTcpClient().Connect(_IPaddrSendZone, _PortSendZone);
                    _SendZoneList = new SimpleTcpClient().Connect(_IPaddrSendZone, _PortSendZone);
                    _ZoneList.Clear();
                    // Add new Zone
                    _VelodromeZone();
                    _EquistrianZone();
                    _PulomasZone();
                    _BoulevardUtaraZone();
                    _BoulevardSelatanZone();

                    int _count = 1;
                    _MessageStation = "ZONE,";
                    foreach (string _zone in _ZoneList)
                    {
                        if (_count == _ZoneList.Count)
                        {
                            // Last Data
                            _MessageStation = _MessageStation + _zone;
                        }
                        else
                        {
                            // Insert New Data
                            _MessageStation = _MessageStation + _zone + ",";
                        }
                        _count++;
                    }
                    if (_MessageStation != "ZONE,")
                    {
                        _SendZoneList.Write(_MessageStation);
                        Thread.Sleep(500);
                        _SendDVAAudio.Write("PADVAAudio," + _Items[0] + "," + _Items[1] + "");
                        _SendZoneList.Dispose();
                        _SendDVAAudio.Dispose();
                    }
                    context.VariableCollection["OA.DVAAnnouncements"].SetValue(0, 0);
                    _logger.Debug(_Datetime + "Request DVA Announcements");
                }
                catch (SocketException ex)
                {
                    _logger.Error(_Datetime + ex.Message.ToString());
                }
            }
            /// <summary>
            /// Function to Send a command PID Text to OA Driver
            /// </summary>
            if (context.VariableCollection["OA.DVATextAnnouncements"].GetValue(0).ToString() == "1")
            {
                string _Text = context.VariableCollection["OA.WPF.GetValueTextPID"].GetValue(0).ToString();
                SimpleTcpClient _AnnText, _SelectedZone;
                Thread.Sleep(1000);
                // set the commands
                try
                {
                    _SelectedZone = new SimpleTcpClient().Connect(_IPaddrSendZone, _PortSendZone);
                    _AnnText = new SimpleTcpClient().Connect(_IPaddrSendZone, _PortSendZone);
                    _ZoneList.Clear();
                    _AddPIDZone();

                    int _count = 1;
                    _MessageStation = "ZONE,";
                    foreach (string _zone in _ZoneList)
                    {
                        if (_count == _ZoneList.Count)
                        {
                            // Last Data
                            _MessageStation = _MessageStation + _zone;
                        }
                        else
                        {
                            // Insert New Data
                            _MessageStation = _MessageStation + _zone + ",";
                        }
                        _count++;
                    }
                    if (_MessageStation != "ZONE,")
                    {
                        if (_Text != "")
                        {
                            _SelectedZone.Write(_MessageStation);
                            _AnnText.Write("PIDMessage," + _Text + "");
                        }
                        else
                        {
                            MessageBox.Show("Please Choose a Pre Defined Text Or Insert a text from Custom field", "Information",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information,
                            MessageBoxDefaultButton.Button1);
                        }
                        _SelectedZone.Dispose();
                        _AnnText.Dispose();
                    }
                    context.VariableCollection["OA.DVATextAnnouncements"].SetValue(0, 0);
                    _logger.Debug(_Datetime + "Request OA Schedule");
                }
                catch (SocketException ex)
                {
                    _logger.Error(_Datetime + ex.Message.ToString());
                }
            }
        }
        private void _VelodromeZone()
        {
            // Public Area
            if (context.VariableCollection["OA.PA.VLD.Line.1"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.VLD.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[0]);
            }
            if (context.VariableCollection["OA.PA.VLD.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.VLD.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[1]);
            }
            if (context.VariableCollection["OA.PA.VLD.Line.3"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.VLD.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[2]);
            }
            if (context.VariableCollection["OA.PA.VLD.Line.4"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.VLD.Line.4.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[3]);
            }
            // Non Public Area
            if (context.VariableCollection["OA.NPA.VLD.Line.1"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.VLD.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[4]);
            }
            if (context.VariableCollection["OA.NPA.VLD.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.VLD.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[5]);
            }
            if (context.VariableCollection["OA.NPA.VLD.Line.3"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.VLD.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[6]);
            }
            if (context.VariableCollection["OA.NPA.VLD.Line.4"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.VLD.Line.4.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[7]);
            }
            if (context.VariableCollection["OA.NPA.VLD.Line.5"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.VLD.Line.5.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[8]);
            }
            // Platform
            if (context.VariableCollection["OA.PF.VLD.Line.1"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PF.VLD.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[9]);
            }
            if (context.VariableCollection["OA.PF.VLD.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PF.VLD.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[10]);
            }
            if (context.VariableCollection["OA.PF.VLD.Line.3"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PF.VLD.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[11]);
            }
            // AFIL
            if (context.VariableCollection["OA.AFIL.VLD.Line.1"].GetValue(0).ToString() == "1" &&
              context.VariableCollection["OA.AFIL.VLD.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[12]);
            }
            if (context.VariableCollection["OA.AFIL.VLD.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.AFIL.VLD.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[13]);
            }
        }
        private void _EquistrianZone()
        {
            // Public Area
            if (context.VariableCollection["OA.PA.EQU.Line.1"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.EQU.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[0]);
            }
            if (context.VariableCollection["OA.PA.EQU.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.EQU.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[1]);
            }
            if (context.VariableCollection["OA.PA.EQU.Line.3"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.EQU.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[2]);
            }
            if (context.VariableCollection["OA.PA.EQU.Line.4"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.EQU.Line.4.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[3]);
            }
            if (context.VariableCollection["OA.PA.EQU.Line.5"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.EQU.Line.5.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[4]);
            }
            // Non Public Area
            if (context.VariableCollection["OA.NPA.EQU.Line.1"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.EQU.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[5]);
            }
            if (context.VariableCollection["OA.NPA.EQU.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.EQU.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[6]);
            }
            if (context.VariableCollection["OA.NPA.EQU.Line.3"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.EQU.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[7]);
            }
            if (context.VariableCollection["OA.NPA.EQU.Line.4"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.EQU.Line.4.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[8]);
            }
            if (context.VariableCollection["OA.NPA.EQU.Line.5"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.EQU.Line.5.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[9]);
            }
            // Platform
            if (context.VariableCollection["OA.PF.EQU.Line.1"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PF.EQU.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[10]);
            }
            if (context.VariableCollection["OA.PF.EQU.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PF.EQU.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[11]);
            }
            // AFIL
            if (context.VariableCollection["OA.AFIL.EQU.Line.1"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.AFIL.EQU.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[12]);
            }
            if (context.VariableCollection["OA.AFIL.EQU.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.AFIL.EQU.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[13]);
            }
        }
        private void _PulomasZone()
        {
            // Public Area
            if (context.VariableCollection["OA.PA.PLM.Line.1"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.PLM.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[0]);
            }
            if (context.VariableCollection["OA.PA.PLM.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.PLM.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[1]);
            }
            if (context.VariableCollection["OA.PA.PLM.Line.3"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.PLM.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[2]);
            }
            if (context.VariableCollection["OA.PA.PLM.Line.4"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.PLM.Line.4.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[3]);
            }
            // non Public area
            if (context.VariableCollection["OA.NPA.PLM.Line.1"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.PLM.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[4]);
            }
            if (context.VariableCollection["OA.NPA.PLM.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.PLM.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[5]);
            }
            if (context.VariableCollection["OA.NPA.PLM.Line.3"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.PLM.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[6]);
            }
            if (context.VariableCollection["OA.NPA.PLM.Line.4"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.PLM.Line.4.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[7]);
            }
            if (context.VariableCollection["OA.NPA.PLM.Line.5"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.PLM.Line.5.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[8]);
            }
            if (context.VariableCollection["OA.NPA.PLM.Line.6"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.PLM.Line.6.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[9]);
            }
            // platform
            if (context.VariableCollection["OA.PF.PLM.Line.1"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PF.PLM.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[10]);
            }
            if (context.VariableCollection["OA.PF.PLM.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PF.PLM.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[11]);
            }
            // AFIL
            if (context.VariableCollection["OA.AFIL.PLM.Line.1"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.AFIL.PLM.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[12]);
            }
            if (context.VariableCollection["OA.AFIL.PLM.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.AFIL.PLM.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[13]);
            }

        }
        private void _BoulevardUtaraZone()
        {
            // Public Area
            if (context.VariableCollection["OA.PA.BLU.Line.1"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.BLU.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[0]);
            }
            if (context.VariableCollection["OA.PA.BLU.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.BLU.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[1]);
            }
            if (context.VariableCollection["OA.PA.BLU.Line.3"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.BLU.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[2]);
            }
            if (context.VariableCollection["OA.PA.BLU.Line.4"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.BLU.Line.4.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[3]);
            }
            // Non Public Area
            if (context.VariableCollection["OA.NPA.BLU.Line.1"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.BLU.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[4]);
            }
            if (context.VariableCollection["OA.NPA.BLU.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.BLU.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[5]);
            }
            if (context.VariableCollection["OA.NPA.BLU.Line.3"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.BLU.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[6]);
            }
            if (context.VariableCollection["OA.NPA.BLU.Line.4"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.BLU.Line.4.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[7]);
            }
            if (context.VariableCollection["OA.NPA.BLU.Line.5"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.BLU.Line.5.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[8]);
            }
            // Platform
            if (context.VariableCollection["OA.PF.BLU.Line.1"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PF.BLU.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[9]);
            }
            if (context.VariableCollection["OA.PF.BLU.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PF.BLU.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[10]);
            }
            if (context.VariableCollection["OA.PF.BLU.Line.3"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PF.BLU.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[11]);
            }
            // AFIL
            if (context.VariableCollection["OA.AFIL.BLU.Line.1"].GetValue(0).ToString() == "1" &&
               context.VariableCollection["OA.AFIL.BLU.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[12]);
            }
            if (context.VariableCollection["OA.AFIL.BLU.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.AFIL.BLU.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[13]);
            }
        }
        private void _BoulevardSelatanZone()
        {
            // Public Area
            if (context.VariableCollection["OA.PA.BLS.Line.1"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.BLS.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[0]);
            }
            if (context.VariableCollection["OA.PA.BLS.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.BLS.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[1]);
            }
            if (context.VariableCollection["OA.PA.BLS.Line.3"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PA.BLS.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[2]);
            }
            // Non Public Area
            if (context.VariableCollection["OA.NPA.BLS.Line.1"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.BLS.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[3]);
            }
            if (context.VariableCollection["OA.NPA.BLS.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.BLS.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[4]);
            }
            if (context.VariableCollection["OA.NPA.BLS.Line.3"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.BLS.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[5]);
            }
            if (context.VariableCollection["OA.NPA.BLS.Line.4"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.BLS.Line.4.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[6]);
            }
            if (context.VariableCollection["OA.NPA.BLS.Line.5"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.BLS.Line.5.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[7]);
            }
            if (context.VariableCollection["OA.NPA.BLS.Line.6"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.NPA.BLS.Line.6.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[8]);
            }
            // Platform
            if (context.VariableCollection["OA.PF.BLS.Line.1"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PF.BLS.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[9]);
            }
            if (context.VariableCollection["OA.PF.BLS.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PF.BLS.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[10]);
            }
            if (context.VariableCollection["OA.PF.BLS.Line.3"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PF.BLS.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[11]);
            }
            // AFIL
            if (context.VariableCollection["OA.AFIL.BLS.Line.1"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.AFIL.BLS.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[12]);
            }
            if (context.VariableCollection["OA.AFIL.BLS.Line.2"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.AFIL.BLS.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[13]);
            }
        }
        private void _DepotZone()
        {
            // Depo Dua
            //if (context.VariableCollection["OA.PA.DPD.Line.1"].GetValue(0).ToString() == "1" &&
            //    context.VariableCollection["OA.PA.DPD.Line.1.Status"].GetValue(0).ToString() == "1")
            //{
            //    _ZoneList.Add(DPDKey.GetValue("DPD_PA_SET_A").ToString());
            //}
            //if (context.VariableCollection["OA.PA.DPD.Line.2"].GetValue(0).ToString() == "1" &&
            //    context.VariableCollection["OA.PA.DPD.Line.2.Status"].GetValue(0).ToString() == "1")
            //{
            //    _ZoneList.Add(DPDKey.GetValue("DPD_PA_SET_B").ToString());
            //}
            //if (context.VariableCollection["OA.PA.DPD.Line.3"].GetValue(0).ToString() == "1" &&
            //    context.VariableCollection["OA.PA.DPD.Line.3.Status"].GetValue(0).ToString() == "1")
            //{
            //    _ZoneList.Add(DPDKey.GetValue("DPD_PA_SET_C").ToString());
            //}
            //if (context.VariableCollection["OA.PA.DPD.Line.4"].GetValue(0).ToString() == "1" &&
            //    context.VariableCollection["OA.PA.DPD.Line.4.Status"].GetValue(0).ToString() == "1")
            //{
            //    _ZoneList.Add(DPDKey.GetValue("DPD_PA_SET_D").ToString());
            //}
            // Non Public Area




            //// Depot
            //if (context.VariableCollection["OA.NPA.DPD.Line.1"].GetValue(0).ToString() == "1" &&
            //    context.VariableCollection["OA.NPA.DPD.Line.1.Status"].GetValue(0).ToString() == "1")
            //{
            //    _ZoneList.Add(DPDKey.GetValue("DPD_NPA_SET_A").ToString());
            //}
            //if (context.VariableCollection["OA.NPA.DPD.Line.2"].GetValue(0).ToString() == "1" &&
            //    context.VariableCollection["OA.NPA.DPD.Line.2.Status"].GetValue(0).ToString() == "1")
            //{
            //    _ZoneList.Add(DPDKey.GetValue("DPD_NPA_SET_B").ToString());
            //}
            //if (context.VariableCollection["OA.NPA.DPD.Line.3"].GetValue(0).ToString() == "1" &&
            //    context.VariableCollection["OA.NPA.DPD.Line.3.Status"].GetValue(0).ToString() == "1")
            //{
            //    _ZoneList.Add(DPDKey.GetValue("DPD_NPA_SET_C").ToString());
            //}
            //if (context.VariableCollection["OA.NPA.DPD.Line.4"].GetValue(0).ToString() == "1" &&
            //    context.VariableCollection["OA.NPA.DPD.Line.4.Status"].GetValue(0).ToString() == "1")
            //{
            //    _ZoneList.Add(DPDKey.GetValue("DPD_NPA_SET_D").ToString());
            //}
            //if (context.VariableCollection["OA.NPA.DPD.Line.5"].GetValue(0).ToString() == "1" &&
            //    context.VariableCollection["OA.NPA.DPD.Line.5.Status"].GetValue(0).ToString() == "1")
            //{
            //    _ZoneList.Add(DPDKey.GetValue("DPD_NPA_SET_E").ToString());
            //}
            //// Platform

            ////if (context.VariableCollection["OA.PF.VLD.Line.4"].GetValue(0).ToString() == "1" &&
            ////    context.VariableCollection["OA.PF.VLD.Line.4.Status"].GetValue(0).ToString() == "1")
            ////{
            ////    _ZoneList.Add(VLDKey.GetValue("VLD_PF_SET_D").ToString());
            ////}

            ////if (context.VariableCollection["OA.PF.BLU.Line.4"].GetValue(0).ToString() == "1" &&
            ////    context.VariableCollection["OA.PF.BLU.Line.4.Status"].GetValue(0).ToString() == "1")
            ////{
            ////    _ZoneList.Add(BLUKey.GetValue("BLU_PF_SET_D").ToString());
            ////}

            ////if (context.VariableCollection["OA.PF.BLS.Line.4"].GetValue(0).ToString() == "1" &&
            ////    context.VariableCollection["OA.PF.BLS.Line.4.Status"].GetValue(0).ToString() == "1")
            ////{
            ////    _ZoneList.Add(BLSKey.GetValue("BLS_PF_SET_D").ToString());
            ////}
            //// Depot
            //if (context.VariableCollection["OA.PF.DPD.Line.1"].GetValue(0).ToString() == "1" &&
            //    context.VariableCollection["OA.PF.DPD.Line.1.Status"].GetValue(0).ToString() == "1")
            //{
            //    _ZoneList.Add(DPDKey.GetValue("DPD_PF_SET_A").ToString());
            //}
            //if (context.VariableCollection["OA.PF.DPD.Line.2"].GetValue(0).ToString() == "1" &&
            //    context.VariableCollection["OA.PF.DPD.Line.2.Status"].GetValue(0).ToString() == "1")
            //{
            //    _ZoneList.Add(DPDKey.GetValue("DPD_PF_SET_B").ToString());
            //}
            //if (context.VariableCollection["OA.PF.DPD.Line.3"].GetValue(0).ToString() == "1" &&
            //    context.VariableCollection["OA.PF.DPD.Line.3.Status"].GetValue(0).ToString() == "1")
            //{
            //    _ZoneList.Add(DPDKey.GetValue("DPD_PF_SET_C").ToString());
            //}
            //if (context.VariableCollection["OA.PF.DPD.Line.4"].GetValue(0).ToString() == "1" &&
            //    context.VariableCollection["OA.PF.DPD.Line.4.Status"].GetValue(0).ToString() == "1")
            //{
            //    _ZoneList.Add(DPDKey.GetValue("DPD_PF_SET_D").ToString());
            //}
        }
        private void _AddPIDZone()
        {
            // Councourse
            // Velodrome
            if (context.VariableCollection["OA.PID.VLD.CouncourseA"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.VLD.CouncourseA.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDVLDZoneVariables[0]);
            }
            if (context.VariableCollection["OA.PID.VLD.CouncourseB"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.VLD.CouncourseB.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDVLDZoneVariables[1]);
            }
            // Equistrian
            if (context.VariableCollection["OA.PID.EQU.CouncourseA"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.EQU.CouncourseA.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDEQUZoneVariables[0]);
            }
            if (context.VariableCollection["OA.PID.EQU.CouncourseB"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.EQU.CouncourseB.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDEQUZoneVariables[1]);
            }
            // Pulomas
            if (context.VariableCollection["OA.PID.PLM.CouncourseA"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.PLM.CouncourseA.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDPLMZoneVariables[0]);
            }
            if (context.VariableCollection["OA.PID.PLM.CouncourseB"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.PLM.CouncourseB.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDPLMZoneVariables[1]);
            }
            // Boulevard Utara
            if (context.VariableCollection["OA.PID.BLU.CouncourseA"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.BLU.CouncourseA.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDBLUZoneVariables[0]);
            }
            if (context.VariableCollection["OA.PID.BLU.CouncourseB"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.BLU.CouncourseB.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDBLUZoneVariables[1]);
            }
            // Boulevard Selatan
            if (context.VariableCollection["OA.PID.BLS.CouncourseA"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.BLS.CouncourseA.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDBLSZoneVariables[0]);
            }
            if (context.VariableCollection["OA.PID.BLS.CouncourseB"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.BLS.CouncourseB.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDBLSZoneVariables[1]);
            }
            // Depot
            if (context.VariableCollection["OA.PID.DPD.CouncourseA"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.DPD.CouncourseA.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDDPDZoneVariables[0]);
            }
            if (context.VariableCollection["OA.PID.DPD.CouncourseB"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.DPD.CouncourseB.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDDPDZoneVariables[1]);
            }

            // Platform

            // Velodrome
            if (context.VariableCollection["OA.PID.VLD.PlatformA"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.VLD.PlatformA.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDVLDZoneVariables[2]);
            }
            if (context.VariableCollection["OA.PID.VLD.PlatformB"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.VLD.PlatformB.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDVLDZoneVariables[3]);
            }
            // Equistrian
            if (context.VariableCollection["OA.PID.EQU.PlatformA"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.EQU.PlatformA.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDEQUZoneVariables[2]);
            }
            if (context.VariableCollection["OA.PID.EQU.PlatformB"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.EQU.PlatformB.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDEQUZoneVariables[3]);
            }
            // Pulomas
            if (context.VariableCollection["OA.PID.PLM.PlatformA"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.PLM.PlatformA.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDPLMZoneVariables[2]);
            }
            if (context.VariableCollection["OA.PID.PLM.PlatformB"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.PLM.PlatformB.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDPLMZoneVariables[3]);
            }
            // Boulevard Utara
            if (context.VariableCollection["OA.PID.BLU.PlatformA"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.BLU.PlatformA.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDBLUZoneVariables[2]);
            }
            if (context.VariableCollection["OA.PID.BLU.PlatformB"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.BLU.PlatformB.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDBLUZoneVariables[3]);
            }
            // Boulevard Selatan
            if (context.VariableCollection["OA.PID.BLS.PlatformA"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.BLS.PlatformA.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDBLSZoneVariables[2]);
            }
            if (context.VariableCollection["OA.PID.BLS.PlatformB"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.BLS.PlatformB.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDBLSZoneVariables[3]);
            }
            // Depot
            if (context.VariableCollection["OA.PID.DPD.PlatformA"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.DPD.PlatformA.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDDPDZoneVariables[2]);
            }
            if (context.VariableCollection["OA.PID.DPD.PlatformB"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.DPD.PlatformB.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDDPDZoneVariables[3]);
            }
        }
        #endregion
    }

}