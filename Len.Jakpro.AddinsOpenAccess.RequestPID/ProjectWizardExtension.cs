﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;
using Len.Jakpro.Database.Rendundancy;
using NLog;
using Scada.AddIn.Contracts;
using SimpleTCP;

namespace Len.Jakpro.AddinsOpenAccess.RequestPID
{
    /// <summary>
    /// Description of Project Wizard Extension.
    /// </summary>
    [AddInExtension("OA Driver PID", "OA Driver PID Control")]
    public class ProjectWizardExtension : IProjectWizardExtension
    {
        #region IProjectWizardExtension implementation
        private static readonly string _Datetime = DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss : ");
        private List<string> _ZoneList = new List<string>();
        private static readonly string _IPaddrSendZone = "10.202.91.4";
        private static readonly int _PortSendZone = 1504;
        // Get Zone PID From DB
        private static List<string> _PIDVLDZoneVariables = new List<string>();
        private static List<string> _PIDEQUZoneVariables = new List<string>();
        private static List<string> _PIDPLMZoneVariables = new List<string>();
        private static List<string> _PIDBLUZoneVariables = new List<string>();
        private static List<string> _PIDBLSZoneVariables = new List<string>();
        private static List<string> _PIDDPDZoneVariables = new List<string>();
        // Logger
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        //db
        private Connection _Con;
        private string _MessageStation;
        private bool _HasValidationState = false;
        IProject context;
        public void Run(IProject context, IBehavior behavior)
        {
            // enter your code which should be executed on triggering the function "Execute Project Wizard Extension" in the SCADA Runtime
            this.context = context;
            _GetZoneList("OA_GetPIDZoneVLD", _PIDVLDZoneVariables);
            _GetZoneList("OA_GetPIDZoneEQU", _PIDEQUZoneVariables);
            _GetZoneList("OA_GetPIDZonePLM", _PIDPLMZoneVariables);
            _GetZoneList("OA_GetPIDZoneBLU", _PIDBLUZoneVariables);
            _GetZoneList("OA_GetPIDZoneBLS", _PIDBLSZoneVariables);
            _GetZoneList("OA_GetPIDZoneDPD", _PIDDPDZoneVariables);
            /// <summary>
            /// Function to Send a command PID Text to OA Driver
            /// </summary>
            string _Text = context.VariableCollection["OA.WPF.GetValueTextPID"].GetValue(0).ToString();
            SimpleTcpClient _AnnText, _SelectedZone;
            Thread.Sleep(1000);
            // set the commands
            try
            {
                _SelectedZone = new SimpleTcpClient().Connect(_IPaddrSendZone, _PortSendZone);
                _AnnText = new SimpleTcpClient().Connect(_IPaddrSendZone, _PortSendZone);
                _ZoneList.Clear();
                _AddPIDZone();

                int _count = 1;
                _MessageStation = "ZONE,";
                foreach (string _zone in _ZoneList)
                {
                    if (_count == _ZoneList.Count)
                    {
                        // Last Data
                        _MessageStation = _MessageStation + _zone;
                    }
                    else
                    {
                        // Insert New Data
                        _MessageStation = _MessageStation + _zone + ",";
                    }
                    _count++;
                }
                if (_MessageStation != "ZONE,")
                {
                    if (_Text != "")
                    {
                        _SelectedZone.Write(_MessageStation);
                        _AnnText.Write("PIDMessage," + _Text + "");
                    }
                    else
                    {
                        MessageBox.Show("Please Choose a Pre Defined Text Or Insert a text from Custom field", "Information",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information,
                        MessageBoxDefaultButton.Button1);
                    }
                    _SelectedZone.Dispose();
                    _AnnText.Dispose();
                }
                context.VariableCollection["OA.DVATextAnnouncements"].SetValue(0, 0);
                _logger.Debug(_Datetime + "Request OA Schedule");
            }
            catch (SocketException ex)
            {
                _logger.Error(_Datetime + ex.Message.ToString());
            }

        }
        private void _AddPIDZone()
        {
            // Councourse
            // Velodrome
            if (context.VariableCollection["OA.PID.VLD.CouncourseA"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.VLD.CouncourseA.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDVLDZoneVariables[0]);
            }
            if (context.VariableCollection["OA.PID.VLD.CouncourseB"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.VLD.CouncourseB.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDVLDZoneVariables[1]);
            }
            // Equistrian
            if (context.VariableCollection["OA.PID.EQU.CouncourseA"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.EQU.CouncourseA.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDEQUZoneVariables[0]);
            }
            if (context.VariableCollection["OA.PID.EQU.CouncourseB"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.EQU.CouncourseB.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDEQUZoneVariables[1]);
            }
            // Pulomas
            if (context.VariableCollection["OA.PID.PLM.CouncourseA"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.PLM.CouncourseA.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDPLMZoneVariables[0]);
            }
            if (context.VariableCollection["OA.PID.PLM.CouncourseB"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.PLM.CouncourseB.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDPLMZoneVariables[1]);
            }
            // Boulevard Utara
            if (context.VariableCollection["OA.PID.BLU.CouncourseA"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.BLU.CouncourseA.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDBLUZoneVariables[0]);
            }
            if (context.VariableCollection["OA.PID.BLU.CouncourseB"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.BLU.CouncourseB.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDBLUZoneVariables[1]);
            }
            // Boulevard Selatan
            if (context.VariableCollection["OA.PID.BLS.CouncourseA"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.BLS.CouncourseA.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDBLSZoneVariables[0]);
            }
            if (context.VariableCollection["OA.PID.BLS.CouncourseB"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.BLS.CouncourseB.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDBLSZoneVariables[1]);
            }
            // Depot
            if (context.VariableCollection["OA.PID.DPD.CouncourseA"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.DPD.CouncourseA.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDDPDZoneVariables[0]);
            }
            if (context.VariableCollection["OA.PID.DPD.CouncourseB"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.DPD.CouncourseB.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDDPDZoneVariables[1]);
            }

            // Platform

            // Velodrome
            if (context.VariableCollection["OA.PID.VLD.PlatformA"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.VLD.PlatformA.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDVLDZoneVariables[2]);
            }
            if (context.VariableCollection["OA.PID.VLD.PlatformB"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.VLD.PlatformB.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDVLDZoneVariables[3]);
            }
            // Equistrian
            if (context.VariableCollection["OA.PID.EQU.PlatformA"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.EQU.PlatformA.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDEQUZoneVariables[2]);
            }
            if (context.VariableCollection["OA.PID.EQU.PlatformB"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.EQU.PlatformB.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDEQUZoneVariables[3]);
            }
            // Pulomas
            if (context.VariableCollection["OA.PID.PLM.PlatformA"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.PLM.PlatformA.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDPLMZoneVariables[2]);
            }
            if (context.VariableCollection["OA.PID.PLM.PlatformB"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.PLM.PlatformB.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDPLMZoneVariables[3]);
            }
            // Boulevard Utara
            if (context.VariableCollection["OA.PID.BLU.PlatformA"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.BLU.PlatformA.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDBLUZoneVariables[2]);
            }
            if (context.VariableCollection["OA.PID.BLU.PlatformB"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.BLU.PlatformB.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDBLUZoneVariables[3]);
            }
            // Boulevard Selatan
            if (context.VariableCollection["OA.PID.BLS.PlatformA"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.BLS.PlatformA.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDBLSZoneVariables[2]);
            }
            if (context.VariableCollection["OA.PID.BLS.PlatformB"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.BLS.PlatformB.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDBLSZoneVariables[3]);
            }
            // Depot
            if (context.VariableCollection["OA.PID.DPD.PlatformA"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.DPD.PlatformA.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDDPDZoneVariables[2]);
            }
            if (context.VariableCollection["OA.PID.DPD.PlatformB"].GetValue(0).ToString() == "1" &&
                context.VariableCollection["OA.PID.DPD.PlatformB.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDDPDZoneVariables[3]);
            }
        }
        private void _GetZoneList(string QueryProcedure, List<string> _TempVariables)
        {
            try
            {
                _Con = new Connection();
                _Con.Connected();
                _Con.SqlCmd = new SqlCommand
                {
                    Connection = _Con.SqlCon,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = QueryProcedure
                };
                _Con.SqlRead = _Con.SqlCmd.ExecuteReader();
                while (_Con.SqlRead.Read())
                {
                    _TempVariables.Add(_Con.SqlRead.GetString(3));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(_Datetime + ex.Message.ToString());
            }

            if (!_Con.SqlRead.IsClosed)
            {
                _Con.SqlRead.Close();
            }
        }
        #endregion
    }

}