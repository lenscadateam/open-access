﻿using System;
using System.Timers;
using Scada.AddIn.Contracts;
using System.Collections.Generic;
using System.Threading;
using System.Net;
using System.Text;
using SimpleTCP;
using Scada.AddIn.Contracts.Variable;
using Len.Jakpro.AddInSampleLibrary.Subscription;
using System.Net.Sockets;
using NLog;
using System.Data.SqlClient;
using System.Data;
using Len.Jakpro.Database.Rendundancy;
using Len.Jakpro.Addins.Logging;
using System.Windows.Forms;
using Microsoft.Win32;

namespace Len.Jakpro.AddinsOpenAccess
{
    /// <summary>
    /// Description of Project Service Extension.
    /// </summary>
    [AddInExtension("AddIn OpenAccess", "Communicate using TCP IP", DefaultStartMode = DefaultStartupModes.Auto)]
    public class ProjectServiceExtension : IProjectServiceExtension
    {
        private List<string> _ZoneList =  new List<string>();
        private static readonly string _Datetime = DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss : ");
        //Communication
        private static readonly string _IPaddrSendZone = "127.0.0.1";
        private static readonly int _PortSendZone = 1504;
        private static readonly string _IPaddrStatusReceive = "127.0.0.1";
        private static readonly int _PortReceive = 1505;
        private Thread _tcpListenerThread;
        private readonly VariableSubscription _Variables;        
        //Zenon Variable 
        private static IVariableCollection _VariableList;
        //Timer Threads
        private System.Timers.Timer _Refresh;
        private string _MessageStation;
        // Redudancy
        private bool _HasValidationState = false;
        private static byte _sequenceSend = 0;
        private static byte _sequenceReceive = 0;
        // Logger
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        // Database
        private Connection _Con;
        // Main Server / Backup
        private readonly List<string> _ServerLists = new List<string>();
        private readonly List<string> _ServerVariables = new List<string>();
        // Main CXS Server / Backup
        private static List<string> _CXSServerLists = new List<string>();
        private static List<string> _CXSServerVariables = new List<string>();
        // PHP variable Zenon Status
        private static List<string> _PHPVariablesVLDs = new List<string>();
        private static List<string> _PHPVariablesEQUs = new List<string>();
        private static List<string> _PHPVariablesPLMs = new List<string>();
        private static List<string> _PHPVariablesBLUs = new List<string>();
        private static List<string> _PHPVariablesBLSs = new List<string>();
        private static List<string> _PHPVariablesDPDs = new List<string>();
        // PID Variable Zenon Status
        private static List<string> _PIDVariablesVLDs = new List<string>();
        private static List<string> _PIDVariablesEQUs = new List<string>();
        private static List<string> _PIDVariablesPLMs = new List<string>();
        private static List<string> _PIDVariablesBLUs = new List<string>();
        private static List<string> _PIDVariablesBLSs = new List<string>();
        private static List<string> _PIDVariablesDPDs = new List<string>();
        // IPPA Variable Status
        private static List<string> _IPPAVariables = new List<string>();
        private static List<string> _MICVariables = new List<string>();
        private static List<string> _NACVariables = new List<string>();
        // Get Zone PID From DB
        private static List<string> _PIDVLDZoneVariables = new List<string>();
        private static List<string> _PIDEQUZoneVariables = new List<string>();
        private static List<string> _PIDPLMZoneVariables = new List<string>();
        private static List<string> _PIDBLUZoneVariables = new List<string>();
        private static List<string> _PIDBLSZoneVariables = new List<string>();
        private static List<string> _PIDDPDZoneVariables = new List<string>();
        // Get Zone PA From DB
        private static List<string> _PAVLDZoneVariables = new List<string>();
        private static List<string> _PAEQUZoneVariables = new List<string>();
        private static List<string> _PAPLMZoneVariables = new List<string>();
        private static List<string> _PABLUZoneVariables = new List<string>();
        private static List<string> _PABLSZoneVariables = new List<string>();
        private static List<string> _PADPDZoneVariables = new List<string>();
        // Get List VOIP
        private static List<string> _VoIPVLDZoneVariables = new List<string>();
        private static List<string> _VoIPEQUZoneVariables = new List<string>();
        private static List<string> _VoIPPLMZoneVariables = new List<string>();
        private static List<string> _VoIPBLUZoneVariables = new List<string>();
        private static List<string> _VoIPBLSZoneVariables = new List<string>();
        private static List<string> _VoIPDPDZoneVariables = new List<string>();
        public void GetDbConfiguration()
        {
            // Server Device
            _GetServerList("OA_ServerListConfiguration", _ServerLists, _ServerVariables);
            _GetCXSServerList("OA_CXSServerListConfiguration", _CXSServerLists, _CXSServerVariables);
            // PHP Device
            _GetDeviceList("OA_GetPHPVLD", _PHPVariablesVLDs);
            _GetDeviceList("OA_GetPHPEQU", _PHPVariablesEQUs);
            _GetDeviceList("OA_GetPHPPLM", _PHPVariablesPLMs);
            _GetDeviceList("OA_GetPHPBLU", _PHPVariablesBLUs);
            _GetDeviceList("OA_GetPHPBLS", _PHPVariablesBLSs);
            _GetDeviceList("OA_GetPHPDPD", _PHPVariablesDPDs);
            // PID Device
            _GetDeviceList("OA_GetPIDVLD", _PIDVariablesVLDs);
            _GetDeviceList("OA_GetPIDEQU", _PIDVariablesEQUs);
            _GetDeviceList("OA_GetPIDPLM", _PIDVariablesPLMs);
            _GetDeviceList("OA_GetPIDBLU", _PIDVariablesBLUs);
            _GetDeviceList("OA_GetPIDBLS", _PIDVariablesBLSs);
            _GetDeviceList("OA_GetPIDDPD", _PIDVariablesDPDs);
            // IPPA Device
            _GetDeviceList("OA_GetALLIPPA", _IPPAVariables);
            // MIC Device
            _GetDeviceList("OA_GetALLMIC", _MICVariables);
            // NAC Device
            _GetDeviceList("OA_GetAllNAC", _NACVariables);
            // PA Zone
            _GetZoneList("OA_GetPAZoneVLD", _PAVLDZoneVariables);
            _GetZoneList("OA_GetPAZoneEQU", _PAEQUZoneVariables);
            _GetZoneList("OA_GetPAZonePLM", _PAPLMZoneVariables);
            _GetZoneList("OA_GetPAZoneBLU", _PABLUZoneVariables);
            _GetZoneList("OA_GetPAZoneBLS", _PABLSZoneVariables);
            _GetZoneList("OA_GetPAZoneDPD", _PADPDZoneVariables);
            // PID Zone
            _GetZoneList("OA_GetPIDZoneVLD", _PIDVLDZoneVariables);
            _GetZoneList("OA_GetPIDZoneEQU", _PIDEQUZoneVariables);
            _GetZoneList("OA_GetPIDZonePLM", _PIDPLMZoneVariables);
            _GetZoneList("OA_GetPIDZoneBLU", _PIDBLUZoneVariables);
            _GetZoneList("OA_GetPIDZoneBLS", _PIDBLSZoneVariables);
            _GetZoneList("OA_GetPIDZoneDPD", _PIDDPDZoneVariables);
            // Get VOIP
            _GetZoneList("OA_GetVoIPVariablesVLD", _VoIPVLDZoneVariables);
            _GetZoneList("OA_GetVoIPVariablesEQU", _VoIPEQUZoneVariables);
            _GetZoneList("OA_GetVoIPVariablesPLM", _VoIPPLMZoneVariables);
            _GetZoneList("OA_GetVoIPVariablesBLU", _VoIPBLUZoneVariables);
            _GetZoneList("OA_GetVoIPVariablesBLS", _VoIPBLSZoneVariables);
            _GetZoneList("OA_GetVoIPVariablesDPD", _VoIPDPDZoneVariables);
        }
        private void _GetServerList(string QueryProcedure, List<string> _ServerList, List<string> _VariableServerList)
        {
            try
            {
                _Con = new Connection();
                _Con.Connected();
                _Con.SqlCmd = new SqlCommand
                {
                    Connection = _Con.SqlCon,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = QueryProcedure
                };
                _Con.SqlRead = _Con.SqlCmd.ExecuteReader();
                while (_Con.SqlRead.Read())
                {
                    _ServerList.Add(_Con.SqlRead.GetString(0));
                    _VariableServerList.Add(_Con.SqlRead.GetString(1));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(_Datetime + ex.Message.ToString());
            }
            if (!_Con.SqlRead.IsClosed)
            {
                _Con.SqlRead.Close();
            }
        }
        private void _GetCXSServerList(string QueryProcedure, List<string> _CXSServerList, List<string> _CXSVariableServerList)
        {
            try
            {
                _Con = new Connection();
                _Con.Connected();
                _Con.SqlCmd = new SqlCommand
                {
                    Connection = _Con.SqlCon,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = QueryProcedure
                };
                _Con.SqlRead = _Con.SqlCmd.ExecuteReader();
                while (_Con.SqlRead.Read())
                {
                    _CXSServerList.Add(_Con.SqlRead.GetString(0));
                    _CXSVariableServerList.Add(_Con.SqlRead.GetString(2));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(_Datetime + ex.Message.ToString());
            }

            if (!_Con.SqlRead.IsClosed)
            {
                _Con.SqlRead.Close();
            }
        }
        private void _GetDeviceList(string QueryProcedure, List<string> _TempVariables)
        {
            try
            {
                _Con = new Connection();
                _Con.Connected();
                _Con.SqlCmd = new SqlCommand
                {
                    Connection = _Con.SqlCon,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = QueryProcedure
                };
                _Con.SqlRead = _Con.SqlCmd.ExecuteReader();
                while (_Con.SqlRead.Read())
                {
                    _TempVariables.Add(_Con.SqlRead.GetString(2));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(_Datetime + ex.Message.ToString());
            }

            if (!_Con.SqlRead.IsClosed)
            {
                _Con.SqlRead.Close();
            }
        }
        private void _GetZoneList(string QueryProcedure, List<string> _TempVariables)
        {
            try
            {
                _Con = new Connection();
                _Con.Connected();
                _Con.SqlCmd = new SqlCommand
                {
                    Connection = _Con.SqlCon,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = QueryProcedure
                };
                _Con.SqlRead = _Con.SqlCmd.ExecuteReader();
                while (_Con.SqlRead.Read())
                {
                    _TempVariables.Add(_Con.SqlRead.GetString(3));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(_Datetime + ex.Message.ToString());
            }

            if (!_Con.SqlRead.IsClosed)
            {
                _Con.SqlRead.Close();
            }
        }
        public ProjectServiceExtension()
        {
            GetDbConfiguration();
            var _logging = new NLogConfigurator();
            _logging.Configure();
            _Variables = new VariableSubscription(StatusVariableChanged);
        }
        /// <summary>
        /// Start the add ins when the Runtime active
        /// </summary>
        public void Start(IProject context, IBehavior behavior)
        {
            // enter your code which should be executed when starting the SCADA Runtime Service
            // Create new Thread TCP IP Listen
            _tcpListenerThread = new Thread(new ThreadStart(_ListenTCP))
            {
                IsBackground = true
            };
            _tcpListenerThread.Start();
            // Request ACK to server From Add Ins
             _RequestDeviceState("ACK", "GetDeviceState");
            // Create timer for send to HMI
            _Refresh = new System.Timers.Timer(1000);
            _Refresh.Elapsed += new ElapsedEventHandler(_RefreshData);
            _Refresh.Enabled = true;
            _Refresh.Start();
            // Define to Zenon Variable
            _VariableList = context.VariableCollection;
            // Button Also Status PA
            List<string> _ZenonVariableList = new List<string>
            {
                // Variable To Initiate Microphone PA on OCC and BCC
                "OA.Microphone.PA",
                // Variable To Initiate DVA Announcements On OCC and BCC
                "OA.DVAAnnouncements",
                // Variable To Initiate PID Text Announcements OCC
                "OA.DVATextAnnouncements",
                // Variable To Initiate Signalling OA PA
                "OA.Signalling.String"
            };            
            _Variables.Start(context, _ZenonVariableList);
            //_VariableList[_CXSServerVariables[0]].SetValue(0, 5);
            //_VariableList[_CXSServerVariables[1]].SetValue(0, 5);
        }
        /// <summary>
        /// Stop the add ins when the runtime inactive
        /// </summary>
        public void Stop()
        {
            // enter your code which should be executed when stopping the SCADA Runtime Service
            try
            {
                _tcpListenerThread.Abort();
                _Refresh.Stop();
            }
            catch (Exception ex)
            {
                _logger.Error(_Datetime + ex.Message.ToString());
            }
        }
        /// <summary>
        /// Request Current Device Status from OA Driver to HMI Runtime
        /// </summary>
        private void _RequestDeviceState(string Variable, string Events)
        {
            SimpleTcpClient _SendACK;
            try
            {
                _SendACK = new SimpleTcpClient().Connect(_IPaddrSendZone, _PortSendZone);
                _SendACK.Write(Variable + "," + Events + "");
                _SendACK.Dispose();
                _logger.Debug(_Datetime + "Request Device State");
            }
            catch(Exception ex)
            {
                _logger.Error(_Datetime + ex.Message.ToString());
            }
        }
        /// <summary>
        /// Add Zoning Configuration for PA and PID
        /// </summary>
        private void _VelodromeZone()
        {
            // Public Area
            if (_VariableList["OA.PA.VLD.Line.1"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PA.VLD.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[0]);
            }
            if (_VariableList["OA.PA.VLD.Line.2"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PA.VLD.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[1]);
            }
            if (_VariableList["OA.PA.VLD.Line.3"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PA.VLD.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[2]);
            }
            if (_VariableList["OA.PA.VLD.Line.4"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PA.VLD.Line.4.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[3]);
            }
            // Non Public Area
            if (_VariableList["OA.NPA.VLD.Line.1"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.NPA.VLD.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[4]);
            }
            if (_VariableList["OA.NPA.VLD.Line.2"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.NPA.VLD.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[5]);
            }
            if (_VariableList["OA.NPA.VLD.Line.3"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.NPA.VLD.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[6]);
            }
            if (_VariableList["OA.NPA.VLD.Line.4"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.NPA.VLD.Line.4.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[7]);
            }
            if (_VariableList["OA.NPA.VLD.Line.5"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.NPA.VLD.Line.5.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[8]);
            }
            // Platform
            if (_VariableList["OA.PF.VLD.Line.1"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PF.VLD.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[9]);
            }
            if (_VariableList["OA.PF.VLD.Line.2"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PF.VLD.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[10]);
            }
            if (_VariableList["OA.PF.VLD.Line.3"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PF.VLD.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[11]);
            }
            // AFIL
            if (_VariableList["OA.AFIL.VLD.Line.1"].GetValue(0).ToString() == "1" &&
              _VariableList["OA.AFIL.VLD.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[12]);
            }
            if (_VariableList["OA.AFIL.VLD.Line.2"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.AFIL.VLD.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAVLDZoneVariables[13]);
            }
        }
        private void _EquistrianZone()
        {
            // Public Area
            if (_VariableList["OA.PA.EQU.Line.1"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PA.EQU.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[0]);
            }
            if (_VariableList["OA.PA.EQU.Line.2"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PA.EQU.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[1]);
            }
            if (_VariableList["OA.PA.EQU.Line.3"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PA.EQU.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[2]);
            }
            if (_VariableList["OA.PA.EQU.Line.4"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PA.EQU.Line.4.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[3]);
            }
            if (_VariableList["OA.PA.EQU.Line.5"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PA.EQU.Line.5.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[4]);
            }
            // Non Public Area
            if (_VariableList["OA.NPA.EQU.Line.1"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.NPA.EQU.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[5]);
            }
            if (_VariableList["OA.NPA.EQU.Line.2"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.NPA.EQU.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[6]);
            }
            if (_VariableList["OA.NPA.EQU.Line.3"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.NPA.EQU.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[7]);
            }
            if (_VariableList["OA.NPA.EQU.Line.4"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.NPA.EQU.Line.4.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[8]);
            }
            if (_VariableList["OA.NPA.EQU.Line.5"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.NPA.EQU.Line.5.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[9]);
            }
            // Platform
            if (_VariableList["OA.PF.EQU.Line.1"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PF.EQU.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[10]);
            }
            if (_VariableList["OA.PF.EQU.Line.2"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PF.EQU.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[11]);
            }
            // AFIL
            if (_VariableList["OA.AFIL.EQU.Line.1"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.AFIL.EQU.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[12]);
            }
            if (_VariableList["OA.AFIL.EQU.Line.2"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.AFIL.EQU.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAEQUZoneVariables[13]);
            }
        }
        private void _PulomasZone()
        {
            // Public Area
            if (_VariableList["OA.PA.PLM.Line.1"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PA.PLM.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[0]);
            }
            if (_VariableList["OA.PA.PLM.Line.2"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PA.PLM.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[1]);
            }
            if (_VariableList["OA.PA.PLM.Line.3"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PA.PLM.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[2]);
            }
            if (_VariableList["OA.PA.PLM.Line.4"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PA.PLM.Line.4.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[3]);
            }
            // non Public area
            if (_VariableList["OA.NPA.PLM.Line.1"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.NPA.PLM.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[4]);
            }
            if (_VariableList["OA.NPA.PLM.Line.2"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.NPA.PLM.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[5]);
            }
            if (_VariableList["OA.NPA.PLM.Line.3"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.NPA.PLM.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[6]);
            }
            if (_VariableList["OA.NPA.PLM.Line.4"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.NPA.PLM.Line.4.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[7]);
            }
            if (_VariableList["OA.NPA.PLM.Line.5"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.NPA.PLM.Line.5.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[8]);
            }
            if (_VariableList["OA.NPA.PLM.Line.6"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.NPA.PLM.Line.6.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[9]);
            }
            // platform
            if (_VariableList["OA.PF.PLM.Line.1"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PF.PLM.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[10]);
            }
            if (_VariableList["OA.PF.PLM.Line.2"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PF.PLM.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[11]);
            }
            // AFIL
            if (_VariableList["OA.AFIL.PLM.Line.1"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.AFIL.PLM.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[12]);
            }
            if (_VariableList["OA.AFIL.PLM.Line.2"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.AFIL.PLM.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PAPLMZoneVariables[13]);
            }

        }
        private void _BoulevardUtaraZone()
        {
            // Public Area
            if (_VariableList["OA.PA.BLU.Line.1"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PA.BLU.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[0]);
            }
            if (_VariableList["OA.PA.BLU.Line.2"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PA.BLU.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[1]);
            }
            if (_VariableList["OA.PA.BLU.Line.3"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PA.BLU.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[2]);
            }
            if (_VariableList["OA.PA.BLU.Line.4"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PA.BLU.Line.4.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[3]);
            }
            // Non Public Area
            if (_VariableList["OA.NPA.BLU.Line.1"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.NPA.BLU.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[4]);
            }
            if (_VariableList["OA.NPA.BLU.Line.2"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.NPA.BLU.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[5]);
            }
            if (_VariableList["OA.NPA.BLU.Line.3"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.NPA.BLU.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[6]);
            }
            if (_VariableList["OA.NPA.BLU.Line.4"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.NPA.BLU.Line.4.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[7]);
            }
            if (_VariableList["OA.NPA.BLU.Line.5"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.NPA.BLU.Line.5.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[8]);
            }
            // Platform
            if (_VariableList["OA.PF.BLU.Line.1"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PF.BLU.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[9]);
            }
            if (_VariableList["OA.PF.BLU.Line.2"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PF.BLU.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[10]);
            }
            if (_VariableList["OA.PF.BLU.Line.3"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PF.BLU.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[11]);
            }
            // AFIL
            if (_VariableList["OA.AFIL.BLU.Line.1"].GetValue(0).ToString() == "1" &&
               _VariableList["OA.AFIL.BLU.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[12]);
            }
            if (_VariableList["OA.AFIL.BLU.Line.2"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.AFIL.BLU.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLUZoneVariables[13]);
            }
        }
        private void _BoulevardSelatanZone()
        {
            // Public Area
            if (_VariableList["OA.PA.BLS.Line.1"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PA.BLS.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[0]);
            }
            if (_VariableList["OA.PA.BLS.Line.2"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PA.BLS.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[1]);
            }
            if (_VariableList["OA.PA.BLS.Line.3"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PA.BLS.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[2]);
            }
            // Non Public Area
            if (_VariableList["OA.NPA.BLS.Line.1"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.NPA.BLS.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[3]);
            }
            if (_VariableList["OA.NPA.BLS.Line.2"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.NPA.BLS.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[4]);
            }
            if (_VariableList["OA.NPA.BLS.Line.3"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.NPA.BLS.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[5]);
            }
            if (_VariableList["OA.NPA.BLS.Line.4"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.NPA.BLS.Line.4.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[6]);
            }
            if (_VariableList["OA.NPA.BLS.Line.5"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.NPA.BLS.Line.5.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[7]);
            }
            if (_VariableList["OA.NPA.BLS.Line.6"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.NPA.BLS.Line.6.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[8]);
            }
            // Platform
            if (_VariableList["OA.PF.BLS.Line.1"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PF.BLS.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[9]);
            }
            if (_VariableList["OA.PF.BLS.Line.2"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PF.BLS.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[10]);
            }
            if (_VariableList["OA.PF.BLS.Line.3"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PF.BLS.Line.3.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[11]);
            }
            // AFIL
            if (_VariableList["OA.AFIL.BLS.Line.1"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.AFIL.BLS.Line.1.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[12]);
            }
            if (_VariableList["OA.AFIL.BLS.Line.2"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.AFIL.BLS.Line.2.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PABLSZoneVariables[13]);
            }
        }
        private void _DepotZone()
        {
            // Depo Dua
            //if (_VariableList["OA.PA.DPD.Line.1"].GetValue(0).ToString() == "1" &&
            //    _VariableList["OA.PA.DPD.Line.1.Status"].GetValue(0).ToString() == "1")
            //{
            //    _ZoneList.Add(DPDKey.GetValue("DPD_PA_SET_A").ToString());
            //}
            //if (_VariableList["OA.PA.DPD.Line.2"].GetValue(0).ToString() == "1" &&
            //    _VariableList["OA.PA.DPD.Line.2.Status"].GetValue(0).ToString() == "1")
            //{
            //    _ZoneList.Add(DPDKey.GetValue("DPD_PA_SET_B").ToString());
            //}
            //if (_VariableList["OA.PA.DPD.Line.3"].GetValue(0).ToString() == "1" &&
            //    _VariableList["OA.PA.DPD.Line.3.Status"].GetValue(0).ToString() == "1")
            //{
            //    _ZoneList.Add(DPDKey.GetValue("DPD_PA_SET_C").ToString());
            //}
            //if (_VariableList["OA.PA.DPD.Line.4"].GetValue(0).ToString() == "1" &&
            //    _VariableList["OA.PA.DPD.Line.4.Status"].GetValue(0).ToString() == "1")
            //{
            //    _ZoneList.Add(DPDKey.GetValue("DPD_PA_SET_D").ToString());
            //}
            // Non Public Area




            //// Depot
            //if (_VariableList["OA.NPA.DPD.Line.1"].GetValue(0).ToString() == "1" &&
            //    _VariableList["OA.NPA.DPD.Line.1.Status"].GetValue(0).ToString() == "1")
            //{
            //    _ZoneList.Add(DPDKey.GetValue("DPD_NPA_SET_A").ToString());
            //}
            //if (_VariableList["OA.NPA.DPD.Line.2"].GetValue(0).ToString() == "1" &&
            //    _VariableList["OA.NPA.DPD.Line.2.Status"].GetValue(0).ToString() == "1")
            //{
            //    _ZoneList.Add(DPDKey.GetValue("DPD_NPA_SET_B").ToString());
            //}
            //if (_VariableList["OA.NPA.DPD.Line.3"].GetValue(0).ToString() == "1" &&
            //    _VariableList["OA.NPA.DPD.Line.3.Status"].GetValue(0).ToString() == "1")
            //{
            //    _ZoneList.Add(DPDKey.GetValue("DPD_NPA_SET_C").ToString());
            //}
            //if (_VariableList["OA.NPA.DPD.Line.4"].GetValue(0).ToString() == "1" &&
            //    _VariableList["OA.NPA.DPD.Line.4.Status"].GetValue(0).ToString() == "1")
            //{
            //    _ZoneList.Add(DPDKey.GetValue("DPD_NPA_SET_D").ToString());
            //}
            //if (_VariableList["OA.NPA.DPD.Line.5"].GetValue(0).ToString() == "1" &&
            //    _VariableList["OA.NPA.DPD.Line.5.Status"].GetValue(0).ToString() == "1")
            //{
            //    _ZoneList.Add(DPDKey.GetValue("DPD_NPA_SET_E").ToString());
            //}
            //// Platform

            ////if (_VariableList["OA.PF.VLD.Line.4"].GetValue(0).ToString() == "1" &&
            ////    _VariableList["OA.PF.VLD.Line.4.Status"].GetValue(0).ToString() == "1")
            ////{
            ////    _ZoneList.Add(VLDKey.GetValue("VLD_PF_SET_D").ToString());
            ////}

            ////if (_VariableList["OA.PF.BLU.Line.4"].GetValue(0).ToString() == "1" &&
            ////    _VariableList["OA.PF.BLU.Line.4.Status"].GetValue(0).ToString() == "1")
            ////{
            ////    _ZoneList.Add(BLUKey.GetValue("BLU_PF_SET_D").ToString());
            ////}

            ////if (_VariableList["OA.PF.BLS.Line.4"].GetValue(0).ToString() == "1" &&
            ////    _VariableList["OA.PF.BLS.Line.4.Status"].GetValue(0).ToString() == "1")
            ////{
            ////    _ZoneList.Add(BLSKey.GetValue("BLS_PF_SET_D").ToString());
            ////}
            //// Depot
            //if (_VariableList["OA.PF.DPD.Line.1"].GetValue(0).ToString() == "1" &&
            //    _VariableList["OA.PF.DPD.Line.1.Status"].GetValue(0).ToString() == "1")
            //{
            //    _ZoneList.Add(DPDKey.GetValue("DPD_PF_SET_A").ToString());
            //}
            //if (_VariableList["OA.PF.DPD.Line.2"].GetValue(0).ToString() == "1" &&
            //    _VariableList["OA.PF.DPD.Line.2.Status"].GetValue(0).ToString() == "1")
            //{
            //    _ZoneList.Add(DPDKey.GetValue("DPD_PF_SET_B").ToString());
            //}
            //if (_VariableList["OA.PF.DPD.Line.3"].GetValue(0).ToString() == "1" &&
            //    _VariableList["OA.PF.DPD.Line.3.Status"].GetValue(0).ToString() == "1")
            //{
            //    _ZoneList.Add(DPDKey.GetValue("DPD_PF_SET_C").ToString());
            //}
            //if (_VariableList["OA.PF.DPD.Line.4"].GetValue(0).ToString() == "1" &&
            //    _VariableList["OA.PF.DPD.Line.4.Status"].GetValue(0).ToString() == "1")
            //{
            //    _ZoneList.Add(DPDKey.GetValue("DPD_PF_SET_D").ToString());
            //}
        }
        private void _AddPIDZone()
        {
            // Councourse
            // Velodrome
            if (_VariableList["OA.PID.VLD.CouncourseA"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PID.VLD.CouncourseA.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDVLDZoneVariables[0]);
            }
            if (_VariableList["OA.PID.VLD.CouncourseB"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PID.VLD.CouncourseB.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDVLDZoneVariables[1]);
            }
            // Equistrian
            if (_VariableList["OA.PID.EQU.CouncourseA"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PID.EQU.CouncourseA.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDEQUZoneVariables[0]);
            }
            if (_VariableList["OA.PID.EQU.CouncourseB"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PID.EQU.CouncourseB.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDEQUZoneVariables[1]);
            }
            // Pulomas
            if (_VariableList["OA.PID.PLM.CouncourseA"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PID.PLM.CouncourseA.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDPLMZoneVariables[0]);
            }
            if (_VariableList["OA.PID.PLM.CouncourseB"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PID.PLM.CouncourseB.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDPLMZoneVariables[1]);
            }
            // Boulevard Utara
            if (_VariableList["OA.PID.BLU.CouncourseA"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PID.BLU.CouncourseA.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDBLUZoneVariables[0]);
            }
            if (_VariableList["OA.PID.BLU.CouncourseB"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PID.BLU.CouncourseB.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDBLUZoneVariables[1]);
            }
            // Boulevard Selatan
            if (_VariableList["OA.PID.BLS.CouncourseA"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PID.BLS.CouncourseA.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDBLSZoneVariables[0]);
            }
            if (_VariableList["OA.PID.BLS.CouncourseB"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PID.BLS.CouncourseB.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDBLSZoneVariables[1]);
            }
            // Depot
            if (_VariableList["OA.PID.DPD.CouncourseA"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PID.DPD.CouncourseA.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDDPDZoneVariables[0]);
            }
            if (_VariableList["OA.PID.DPD.CouncourseB"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PID.DPD.CouncourseB.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDDPDZoneVariables[1]);
            }

            // Platform

            // Velodrome
            if (_VariableList["OA.PID.VLD.PlatformA"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PID.VLD.PlatformA.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDVLDZoneVariables[2]);
            }
            if (_VariableList["OA.PID.VLD.PlatformB"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PID.VLD.PlatformB.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDVLDZoneVariables[3]);
            }
            // Equistrian
            if (_VariableList["OA.PID.EQU.PlatformA"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PID.EQU.PlatformA.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDEQUZoneVariables[2]);
            }
            if (_VariableList["OA.PID.EQU.PlatformB"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PID.EQU.PlatformB.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDEQUZoneVariables[3]);
            }
            // Pulomas
            if (_VariableList["OA.PID.PLM.PlatformA"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PID.PLM.PlatformA.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDPLMZoneVariables[2]);
            }
            if (_VariableList["OA.PID.PLM.PlatformB"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PID.PLM.PlatformB.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDPLMZoneVariables[3]);
            }
            // Boulevard Utara
            if (_VariableList["OA.PID.BLU.PlatformA"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PID.BLU.PlatformA.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDBLUZoneVariables[2]);
            }
            if (_VariableList["OA.PID.BLU.PlatformB"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PID.BLU.PlatformB.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDBLUZoneVariables[3]);
            }
            // Boulevard Selatan
            if (_VariableList["OA.PID.BLS.PlatformA"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PID.BLS.PlatformA.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDBLSZoneVariables[2]);
            }
            if (_VariableList["OA.PID.BLS.PlatformB"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PID.BLS.PlatformB.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDBLSZoneVariables[3]);
            }
            // Depot
            if (_VariableList["OA.PID.DPD.PlatformA"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PID.DPD.PlatformA.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDDPDZoneVariables[2]);
            }
            if (_VariableList["OA.PID.DPD.PlatformB"].GetValue(0).ToString() == "1" &&
                _VariableList["OA.PID.DPD.PlatformB.Status"].GetValue(0).ToString() == "1")
            {
                _ZoneList.Add(_PIDDPDZoneVariables[3]);
            }
        }
        /// <summary>
        /// Event Handler status variable Zenon
        /// </summary>
        private void StatusVariableChanged(IEnumerable<IVariable> obj)
        {
            SimpleTcpClient _SendZone, _SendCommand, _SendEndCommand;
            /// <summary>
            /// Function to Send a command Microphone to OA Driver
            /// </summary>
            if (_VariableList["OA.Microphone.PA"].GetValue(0).ToString() == "1")
            {
                _SendZone = new SimpleTcpClient().Connect(_IPaddrSendZone, _PortSendZone);
                _SendCommand = new SimpleTcpClient().Connect(_IPaddrSendZone, _PortSendZone);
                _ZoneList.Clear();
                // Add new Zone
                _VelodromeZone();
                _EquistrianZone();
                _PulomasZone();
                _BoulevardUtaraZone();
                _BoulevardSelatanZone();

                int _count = 1;
                _MessageStation = "ZONE,";
                foreach (string _zone in _ZoneList)
                {
                    if (_count == _ZoneList.Count)
                    {
                        // Last Data
                        _MessageStation = _MessageStation + _zone;
                    }
                    else
                    {
                        // Insert New Data
                        _MessageStation = _MessageStation + _zone + ",";
                    }
                    _count++;
                }
                try
                {
                        _SendZone.Write(_MessageStation);
                        _SendCommand.Write("PAActive");
                        _SendZone.Dispose();
                        _SendCommand.Dispose();
                   
                    _HasValidationState = true;
                    _logger.Debug(_Datetime + "Request PA Annoucements");
                }
                catch (Exception ex)
                {
                    _logger.Error(_Datetime + ex.Message.ToString());
                }
            }
            else if (_VariableList["OA.Microphone.PA"].GetValue(0).ToString() == "0")
            {
                if (_HasValidationState)
                {
                    try
                    {
                        _SendEndCommand = new SimpleTcpClient().Connect(_IPaddrSendZone, _PortSendZone);
                        _SendEndCommand.Write("PAInactive");
                        _SendEndCommand.Dispose();
                        _HasValidationState = false;

                        _logger.Debug(_Datetime + "End PA Announcements");
                    }
                    catch (SocketException ex)
                    {
                        _logger.Error(_Datetime + ex.Message.ToString());
                    }
                }
            }
            /// <summary>
            /// Function to Send a command PA to OA Driver
            /// </summary>
            if (_VariableList["OA.DVAAnnouncements"].GetValue(0).ToString() == "1")
            {
                SimpleTcpClient _SendDVAAudio, _SendZoneList;
                // split value
                string[] _Items = _VariableList["OA.WPF.GetValueDVAMedia"].GetValue(0).ToString().Split(',');
                // set the commands
                try
                {
                    _SendDVAAudio = new SimpleTcpClient().Connect(_IPaddrSendZone, _PortSendZone);
                    _SendZoneList = new SimpleTcpClient().Connect(_IPaddrSendZone, _PortSendZone);
                    _ZoneList.Clear();
                    // Add new Zone
                    _VelodromeZone();
                    _EquistrianZone();
                    _PulomasZone();
                    _BoulevardUtaraZone();
                    _BoulevardSelatanZone();

                    int _count = 1;
                    _MessageStation = "ZONE,";
                    foreach (string _zone in _ZoneList)
                    {
                        if (_count == _ZoneList.Count)
                        {
                            // Last Data
                            _MessageStation = _MessageStation + _zone;
                        }
                        else
                        {
                            // Insert New Data
                            _MessageStation = _MessageStation + _zone + ",";
                        }
                        _count++;
                    }
                        _SendZoneList.Write(_MessageStation);
                        Thread.Sleep(500);
                        _SendDVAAudio.Write("PADVAAudio," + _Items[0] + "," + _Items[1] + "");
                        _SendZoneList.Dispose();
                        _SendDVAAudio.Dispose();
                    
                    _VariableList["OA.DVAAnnouncements"].SetValue(0, 0);
                    _logger.Debug(_Datetime + "Request DVA Announcements");
                }
                catch (SocketException ex)
                {
                    _logger.Error(_Datetime + ex.Message.ToString());
                }
            }
            /// <summary>
            /// Function to Send a command PID Text to OA Driver
            /// </summary>
            if (_VariableList["OA.DVATextAnnouncements"].GetValue(0).ToString() == "1")
            {
                string _Text = _VariableList["OA.WPF.GetValueTextPID"].GetValue(0).ToString();
                SimpleTcpClient _AnnText, _SelectedZone;
                Thread.Sleep(1000);
                // set the commands
                try
                {
                    _SelectedZone = new SimpleTcpClient().Connect(_IPaddrSendZone, _PortSendZone);
                    _AnnText = new SimpleTcpClient().Connect(_IPaddrSendZone, _PortSendZone);
                    _ZoneList.Clear();
                    _AddPIDZone();

                    int _count = 1;
                    _MessageStation = "ZONE,";
                    foreach (string _zone in _ZoneList)
                    {
                        if (_count == _ZoneList.Count)
                        {
                            // Last Data
                            _MessageStation = _MessageStation + _zone;
                        }
                        else
                        {
                            // Insert New Data
                            _MessageStation = _MessageStation + _zone + ",";
                        }
                        _count++;
                    }
                        if (_Text != "")
                        {
                            _SelectedZone.Write(_MessageStation);
                            _AnnText.Write("PIDMessage," + _Text + "");
                        }
                        else
                        {
                            MessageBox.Show("Please Choose a Pre Defined Text Or Insert a text from Custom field", "Information",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information,
                            MessageBoxDefaultButton.Button1);
                        }
                        _SelectedZone.Dispose();
                        _AnnText.Dispose();
                    
                    _VariableList["OA.DVATextAnnouncements"].SetValue(0, 0);
                    _logger.Debug(_Datetime + "Request OA Schedule");
                }
                catch (SocketException ex)
                {
                    _logger.Error(_Datetime + ex.Message.ToString());
                }
            }

            _RedudancyCommandSignallingPA(
                _ServerLists[0],
                _ServerVariables[0],
                _ServerLists[1],
                _ServerVariables[1],
                "OA.Signalling.String");
        }
        /// <summary>
        /// Rendundancy Function One of server initiate PA from signalling eg: Arrival,Depart,Approach
        /// </summary>
        private void _RedudancyCommandSignallingPA(
            string _PrimaryServer, 
            string _PrimaryVariables, 
            string _SecondaryServer, 
            string _SecondaryVariables,
            string _TriggerVariables)
        {
            SimpleTcpClient _SendSignallingPA;
            _SendSignallingPA = new SimpleTcpClient().Connect(_IPaddrSendZone, _PortSendZone);
            try
            {
                if (Environment.MachineName == _PrimaryServer)
                {
                    if (_VariableList[_PrimaryVariables].GetValue(0).ToString() == "1")
                    {
                        _SendSignallingPA.Write(_VariableList[_TriggerVariables].GetValue(0).ToString());
                        _SendSignallingPA.Dispose();
                        _VariableList[_TriggerVariables].SetValue(0,"");
                    }
                }
                if (Environment.MachineName == _SecondaryServer)
                {
                    if (_VariableList[_PrimaryVariables].GetValue(0).ToString() == "0" && _VariableList[_SecondaryVariables].GetValue(0).ToString() == "1")
                    {
                        _SendSignallingPA.Write(_VariableList[_TriggerVariables].GetValue(0).ToString());
                        _SendSignallingPA.Dispose();
                        _VariableList[_TriggerVariables].SetValue(0, "");
                    }
                }
            }
            catch (Exception e)
            {
                _logger.Error(_Datetime + e.Message.ToString());
            }
        }
        /// <summary>
        /// Communication Heartbeat from HMI addins to OA Driver
        /// </summary>
        private void _SendHeatBeat(
            string _PrimaryServer, 
            string _PrimaryVariables,
            string _SecondaryServer,
            string _SecondaryVariables)
        {
            if (_sequenceReceive == _sequenceSend)
            {
                if(Environment.MachineName == _PrimaryServer)
                {
                    _VariableList[_PrimaryVariables].SetValue(0, 1);
                }
                else if (Environment.MachineName == _SecondaryServer)
                {
                    _VariableList[_SecondaryVariables].SetValue(0, 1);
                }
            }
            else
            {
                if (Environment.MachineName == _PrimaryServer)
                {
                    _VariableList[_PrimaryVariables].SetValue(0, 0);

                }
                else if (Environment.MachineName == _SecondaryServer)
                {
                    _VariableList[_SecondaryVariables].SetValue(0, 0);
                }
                
            }
            _sequenceSend++;
            if (_sequenceSend >= 255)
            {
                _sequenceSend = 1;
            }
            byte[] _heartBeat = new byte[5] { 0x23, 0x05, _sequenceSend, 0x24, 0x2A };
            SimpleTcpClient _client;
            try
            {
                _client = new SimpleTcpClient().Connect(_IPaddrSendZone, _PortSendZone);
                _client.Write(_heartBeat);
                _client.Dispose();
            }
            catch(SocketException ex)
            {
                _logger.Error(_Datetime + ex.Message.ToString());
            }
        }
        private void _RefreshData(object sender, ElapsedEventArgs e)
        {
            try
            {
                _SendHeatBeat(
                    _ServerLists[0], 
                    _ServerVariables[0],                
                    _ServerLists[1], 
                    _ServerVariables[1]);
            }
            catch (Exception ex)
            {
                _logger.Error(_Datetime + ex.Message.ToString());
            }
        }   
        private void _ListenTCP()
        {
            SimpleTcpServer _Server = new SimpleTcpServer
            {
                Delimiter = 0x13, //Config
                StringEncoder = Encoding.UTF8 //Config
            }; //Instantiate server.
            _Server.DataReceived += _Server_DataReceived; //Subscribe to DataRecieved event.
            IPAddress ip = IPAddress.Parse(_IPaddrStatusReceive); //IP Address using .Parse()
            try
            {
                _Server.Start(ip, _PortReceive); //Start listening to incoming connections and data.
                _logger.Debug(_Datetime + "TCP Server Server Listen..");
            }
            catch (SocketException e)
            {
                _logger.Error(_Datetime + e.Message.ToString());
            }
        }
        /// <summary>
        /// Event Handler Data received from OA Driver
        /// </summary>
        static void _Server_DataReceived(object sender, SimpleTCP.Message m)
        {
            // Get All Devices State From OA Server
            string[] _SplitItems = m.MessageString.Split(',');
            // ACK Heatbeat
            byte[] _mydata;
            string data;
            _mydata = m.Data;
            data = BitConverter.ToString(_mydata);
            //Console.WriteLine("RX: " + data);
            bool _isStartByteCorrect = _mydata[0] == 0x23;
            bool _isLengthCorrect = _mydata.Length == _mydata[1];
            bool _isAck = _mydata[3] == 0x25;
            bool _isStopByteCorrect = _mydata[_mydata.Length - 1] == 0x2A;
            bool _isCorrectSequence = _mydata[2] == _sequenceSend;
            if (_isStartByteCorrect && _isLengthCorrect && _isAck && _isStopByteCorrect && _isCorrectSequence)
            {
                _sequenceReceive = _sequenceSend;
            }
            /// <summary>
            /// Get all status CXS Server
            /// </summary>
            if (_SplitItems[0] == "CXSServer")
            {
                if (_SplitItems[1] == "0")
                {
                    _VariableList[_CXSServerVariables[0]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "1")
                {
                    _VariableList[_CXSServerVariables[1]].SetValue(0, _SplitItems[2]);
                }
            }
            /// <summary>
            /// Get all status PHP, Passenger Help Point
            /// </summary>
            if (_SplitItems[0] == "PHPVLD")
            {
                if (_SplitItems[1] == "0")
                {
                    _VariableList[_PHPVariablesVLDs[0]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "1")
                {
                    _VariableList[_PHPVariablesVLDs[1]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "2")
                {
                    _VariableList[_PHPVariablesVLDs[2]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "3")
                {
                    _VariableList[_PHPVariablesVLDs[3]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "4")
                {
                    _VariableList[_PHPVariablesVLDs[4]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "5")
                {
                    _VariableList[_PHPVariablesVLDs[5]].SetValue(0, _SplitItems[2]);
                }
            }
            if (_SplitItems[0] == "PHPEQU")
            {
                if (_SplitItems[1] == "0")
                {
                    _VariableList[_PHPVariablesEQUs[0]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "1")
                {
                    _VariableList[_PHPVariablesEQUs[1]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "2")
                {
                    _VariableList[_PHPVariablesEQUs[2]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "3")
                {
                    _VariableList[_PHPVariablesEQUs[3]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "4")
                {
                    _VariableList[_PHPVariablesEQUs[4]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "5")
                {
                    _VariableList[_PHPVariablesEQUs[5]].SetValue(0, _SplitItems[2]);
                }
            }
            if (_SplitItems[0] == "PHPPLM")
            {
                if (_SplitItems[1] == "0")
                {
                    _VariableList[_PHPVariablesPLMs[0]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "1")
                {
                    _VariableList[_PHPVariablesPLMs[1]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "2")
                {
                    _VariableList[_PHPVariablesPLMs[2]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "3")
                {
                    _VariableList[_PHPVariablesPLMs[3]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "4")
                {
                    _VariableList[_PHPVariablesPLMs[4]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "5")
                {
                    _VariableList[_PHPVariablesPLMs[5]].SetValue(0, _SplitItems[2]);
                }
            }
            if (_SplitItems[0] == "PHPBLU")
            {
                if (_SplitItems[1] == "0")
                {
                    _VariableList[_PHPVariablesBLUs[0]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "1")
                {
                    _VariableList[_PHPVariablesBLUs[1]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "2")
                {
                    _VariableList[_PHPVariablesBLUs[2]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "3")
                {
                    _VariableList[_PHPVariablesBLUs[3]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "4")
                {
                    _VariableList[_PHPVariablesBLUs[4]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "5")
                {
                    _VariableList[_PHPVariablesBLUs[5]].SetValue(0, _SplitItems[2]);
                }
            }
            if (_SplitItems[0] == "PHPBLS")
            {
                if (_SplitItems[1] == "0")
                {
                    _VariableList[_PHPVariablesBLSs[0]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "1")
                {
                    _VariableList[_PHPVariablesBLSs[1]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "2")
                {
                    _VariableList[_PHPVariablesBLSs[2]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "3")
                {
                    _VariableList[_PHPVariablesBLSs[3]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "4")
                {
                    _VariableList[_PHPVariablesBLSs[4]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "5")
                {
                    _VariableList[_PHPVariablesBLSs[5]].SetValue(0, _SplitItems[2]);
                }
            }
            if (_SplitItems[0] == "PHPDPD")
            {
                if (_SplitItems[1] == "0")
                {
                    _VariableList[_PHPVariablesDPDs[0]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "1")
                {
                    _VariableList[_PHPVariablesDPDs[1]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "2")
                {
                    _VariableList[_PHPVariablesDPDs[2]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "3")
                {
                    _VariableList[_PHPVariablesDPDs[3]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "4")
                {
                    _VariableList[_PHPVariablesDPDs[4]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "5")
                {
                    _VariableList[_PHPVariablesDPDs[5]].SetValue(0, _SplitItems[2]);
                }
            }
            /// <summary>
            /// Get all status IPPA Device
            /// </summary>
            if (_SplitItems[0] == "IPPA.Status")
            {
                if (_SplitItems[1] == "0")
                {
                    _VariableList[_IPPAVariables[0]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "1")
                {
                    _VariableList[_IPPAVariables[1]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "2")
                {
                    _VariableList[_IPPAVariables[2]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "3")
                {
                    _VariableList[_IPPAVariables[3]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "4")
                {
                    _VariableList[_IPPAVariables[4]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "5")
                {
                    _VariableList[_IPPAVariables[5]].SetValue(0, _SplitItems[2]);
                }
            }
            /// <summary>
            /// Get all status Microphone Device on IPPA
            /// </summary>
            if (_SplitItems[0] == "IPPA.Status.Mic")
            {
                RegistryKey key = Registry.CurrentUser.OpenSubKey("Software\\Len.Jakpro.OpenAccess\\IPPASettings");
                object _Obj = key.GetValue("DefaultIPPAStatus");
                if (_SplitItems[1] == "0")
                {
                    _VariableList[_MICVariables[0]].SetValue(0, _SplitItems[2]);
                    if (_Obj.Equals("0"))
                    {
                        _VariableList["OA.IPPA.Mic.Dynamic.Status"].SetValue(0, _SplitItems[2]);
                    }
                    key.Close();
                }
                else if (_SplitItems[1] == "1")
                {
                    _VariableList[_MICVariables[1]].SetValue(0, _SplitItems[2]);
                    if (_Obj.Equals("1")) 
                    {
                        _VariableList["OA.IPPA.Mic.Dynamic.Status"].SetValue(0, _SplitItems[2]);
                    }
                    key.Close();
                }
                else if (_SplitItems[1] == "2")
                {
                    _VariableList[_MICVariables[2]].SetValue(0, _SplitItems[2]);
                    if (_Obj.Equals("2"))
                    {
                        _VariableList["OA.IPPA.Mic.Dynamic.Status"].SetValue(0, _SplitItems[2]);
                    }
                    key.Close();
                }
                else if (_SplitItems[1] == "3")
                {
                    _VariableList[_MICVariables[3]].SetValue(0, _SplitItems[2]);
                    if (_Obj.Equals("3"))
                    {
                        _VariableList["OA.IPPA.Mic.Dynamic.Status"].SetValue(0, _SplitItems[2]);
                    }
                    key.Close();
                }
                else if (_SplitItems[1] == "4")
                {
                    _VariableList[_MICVariables[4]].SetValue(0, _SplitItems[2]);
                    if (_Obj.Equals("4"))
                    {
                        _VariableList["OA.IPPA.Mic.Dynamic.Status"].SetValue(0, _SplitItems[2]);
                    }
                    key.Close();
                }
                else if (_SplitItems[1] == "5")
                {
                    _VariableList[_MICVariables[5]].SetValue(0, _SplitItems[2]);
                    if (_Obj.Equals("5"))
                    {
                        _VariableList["OA.IPPA.Mic.Dynamic.Status"].SetValue(0, _SplitItems[2]);
                    }
                    key.Close();
                }
                else if (_SplitItems[1] == "6")
                {
                    _VariableList[_MICVariables[6]].SetValue(0, _SplitItems[2]);
                    if (_Obj.Equals("6"))
                    {
                        _VariableList["OA.IPPA.Mic.Dynamic.Status"].SetValue(0, _SplitItems[2]);
                    }
                    key.Close();
                }
                else if (_SplitItems[1] == "7")
                {
                    _VariableList[_MICVariables[7]].SetValue(0, _SplitItems[2]);
                    if (_Obj.Equals("7"))
                    {
                        _VariableList["OA.IPPA.Mic.Dynamic.Status"].SetValue(0, _SplitItems[2]);
                    }
                    key.Close();
                }
            }
            /// <summary>
            /// Get all status NAC Device for all station
            /// </summary>
            if (_SplitItems[0] == "NAC.Status")
            {
                if (_SplitItems[1] == "0")
                {
                    _VariableList[_NACVariables[0]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "1")
                {
                    _VariableList[_NACVariables[1]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "2")
                {
                    _VariableList[_NACVariables[2]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "3")
                {
                    _VariableList[_NACVariables[3]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "4")
                {
                    _VariableList[_NACVariables[4]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "5")
                {
                    _VariableList[_NACVariables[5]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "6")
                {
                    _VariableList[_NACVariables[6]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "7")
                {
                    _VariableList[_NACVariables[7]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "8")
                {
                    _VariableList[_NACVariables[8]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "9")
                {
                    _VariableList[_NACVariables[9]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "10")
                {
                    _VariableList[_NACVariables[10]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "11")
                {
                    _VariableList[_NACVariables[11]].SetValue(0, _SplitItems[2]);
                }
            }
            /// <summary>
            /// Get all status VOIP Station
            /// </summary>
            if (_SplitItems[0] == "VOIP.VLD")
            {
                if (_SplitItems[1] == "0")
                {
                    _VariableList[_VoIPVLDZoneVariables[0]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "1")
                {
                    _VariableList[_VoIPVLDZoneVariables[1]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "2")
                {
                    _VariableList[_VoIPVLDZoneVariables[2]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "3")
                {
                    _VariableList[_VoIPVLDZoneVariables[3]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "4")
                {
                    _VariableList[_VoIPVLDZoneVariables[4]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "5")
                {
                    _VariableList[_VoIPVLDZoneVariables[5]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "6")
                {
                    _VariableList[_VoIPVLDZoneVariables[6]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "7")
                {
                    _VariableList[_VoIPVLDZoneVariables[7]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "8")
                {
                    _VariableList[_VoIPVLDZoneVariables[8]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "9")
                {
                    _VariableList[_VoIPVLDZoneVariables[9]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "10")
                {
                    _VariableList[_VoIPVLDZoneVariables[10]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "11")
                {
                    _VariableList[_VoIPVLDZoneVariables[11]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "12")
                {
                    _VariableList[_VoIPVLDZoneVariables[12]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "13")
                {
                    _VariableList[_VoIPVLDZoneVariables[13]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "14")
                {
                    _VariableList[_VoIPVLDZoneVariables[14]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "15")
                {
                    _VariableList[_VoIPVLDZoneVariables[15]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "16")
                {
                    _VariableList[_VoIPVLDZoneVariables[16]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "17")
                {
                    _VariableList[_VoIPVLDZoneVariables[17]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "18")
                {
                    _VariableList[_VoIPVLDZoneVariables[18]].SetValue(0, _SplitItems[2]);
                }
            }
            if (_SplitItems[0] == "VOIP.EQU")
            {
                if (_SplitItems[1] == "0")
                {
                    _VariableList[_VoIPEQUZoneVariables[0]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "1")
                {
                    _VariableList[_VoIPEQUZoneVariables[1]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "2")
                {
                    _VariableList[_VoIPEQUZoneVariables[2]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "3")
                {
                    _VariableList[_VoIPEQUZoneVariables[3]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "4")
                {
                    _VariableList[_VoIPEQUZoneVariables[4]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "5")
                {
                    _VariableList[_VoIPEQUZoneVariables[5]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "6")
                {
                    _VariableList[_VoIPEQUZoneVariables[6]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "7")
                {
                    _VariableList[_VoIPEQUZoneVariables[7]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "8")
                {
                    _VariableList[_VoIPEQUZoneVariables[8]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "9")
                {
                    _VariableList[_VoIPEQUZoneVariables[9]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "10")
                {
                    _VariableList[_VoIPEQUZoneVariables[10]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "11")
                {
                    _VariableList[_VoIPEQUZoneVariables[11]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "12")
                {
                    _VariableList[_VoIPEQUZoneVariables[12]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "13")
                {
                    _VariableList[_VoIPEQUZoneVariables[13]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "14")
                {
                    _VariableList[_VoIPEQUZoneVariables[14]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "15")
                {
                    _VariableList[_VoIPEQUZoneVariables[15]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "16")
                {
                    _VariableList[_VoIPEQUZoneVariables[16]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "17")
                {
                    _VariableList[_VoIPEQUZoneVariables[17]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "18")
                {
                    _VariableList[_VoIPEQUZoneVariables[18]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "19")
                {
                    _VariableList[_VoIPEQUZoneVariables[19]].SetValue(0, _SplitItems[2]);
                }   
            }
            if (_SplitItems[0] == "VOIP.PLM")
            {
                if (_SplitItems[1] == "0")
                {
                    _VariableList[_VoIPPLMZoneVariables[0]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "1")
                {
                    _VariableList[_VoIPPLMZoneVariables[1]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "2")
                {
                    _VariableList[_VoIPPLMZoneVariables[2]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "3")
                {
                    _VariableList[_VoIPPLMZoneVariables[3]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "4")
                {
                    _VariableList[_VoIPPLMZoneVariables[4]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "5")
                {
                    _VariableList[_VoIPPLMZoneVariables[5]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "6")
                {
                    _VariableList[_VoIPPLMZoneVariables[6]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "7")
                {
                    _VariableList[_VoIPPLMZoneVariables[7]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "8")
                {
                    _VariableList[_VoIPPLMZoneVariables[8]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "9")
                {
                    _VariableList[_VoIPPLMZoneVariables[9]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "10")
                {
                    _VariableList[_VoIPPLMZoneVariables[10]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "11")
                {
                    _VariableList[_VoIPPLMZoneVariables[11]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "12")
                {
                    _VariableList[_VoIPPLMZoneVariables[12]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "13")
                {
                    _VariableList[_VoIPPLMZoneVariables[13]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "14")
                {
                    _VariableList[_VoIPPLMZoneVariables[14]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "15")
                {
                    _VariableList[_VoIPPLMZoneVariables[15]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "16")
                {
                    _VariableList[_VoIPPLMZoneVariables[16]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "17")
                {
                    _VariableList[_VoIPPLMZoneVariables[17]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "18")
                {
                    _VariableList[_VoIPPLMZoneVariables[18]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "19")
                {
                    _VariableList[_VoIPPLMZoneVariables[19]].SetValue(0, _SplitItems[2]);
                }
            }
            if (_SplitItems[0] == "VOIP.BLU")
            {
                if (_SplitItems[1] == "0")
                {
                    _VariableList[_VoIPBLUZoneVariables[0]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "1")
                {
                    _VariableList[_VoIPBLUZoneVariables[1]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "2")
                {
                    _VariableList[_VoIPBLUZoneVariables[2]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "3")
                {
                    _VariableList[_VoIPBLUZoneVariables[3]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "4")
                {
                    _VariableList[_VoIPBLUZoneVariables[4]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "5")
                {
                    _VariableList[_VoIPBLUZoneVariables[5]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "6")
                {
                    _VariableList[_VoIPBLUZoneVariables[6]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "7")
                {
                    _VariableList[_VoIPBLUZoneVariables[7]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "8")
                {
                    _VariableList[_VoIPBLUZoneVariables[8]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "9")
                {
                    _VariableList[_VoIPBLUZoneVariables[9]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "10")
                {
                    _VariableList[_VoIPBLUZoneVariables[10]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "11")
                {
                    _VariableList[_VoIPBLUZoneVariables[11]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "12")
                {
                    _VariableList[_VoIPBLUZoneVariables[12]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "13")
                {
                    _VariableList[_VoIPBLUZoneVariables[13]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "14")
                {
                    _VariableList[_VoIPBLUZoneVariables[14]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "15")
                {
                    _VariableList[_VoIPBLUZoneVariables[15]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "16")
                {
                    _VariableList[_VoIPBLUZoneVariables[16]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "17")
                {
                    _VariableList[_VoIPBLUZoneVariables[17]].SetValue(0, _SplitItems[2]);
                }
              
            }
            if (_SplitItems[0] == "VOIP.BLS")
            {
                if (_SplitItems[1] == "0")
                {
                    _VariableList[_VoIPBLSZoneVariables[0]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "1")
                {
                    _VariableList[_VoIPBLSZoneVariables[1]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "2")
                {
                    _VariableList[_VoIPBLSZoneVariables[2]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "3")
                {
                    _VariableList[_VoIPBLSZoneVariables[3]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "4")
                {
                    _VariableList[_VoIPBLSZoneVariables[4]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "5")
                {
                    _VariableList[_VoIPBLSZoneVariables[5]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "6")
                {
                    _VariableList[_VoIPBLSZoneVariables[6]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "7")
                {
                    _VariableList[_VoIPBLSZoneVariables[7]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "8")
                {
                    _VariableList[_VoIPBLSZoneVariables[8]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "9")
                {
                    _VariableList[_VoIPBLSZoneVariables[9]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "10")
                {
                    _VariableList[_VoIPBLSZoneVariables[10]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "11")
                {
                    _VariableList[_VoIPBLSZoneVariables[11]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "12")
                {
                    _VariableList[_VoIPBLSZoneVariables[12]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "13")
                {
                    _VariableList[_VoIPBLSZoneVariables[13]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "14")
                {
                    _VariableList[_VoIPBLSZoneVariables[14]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "15")
                {
                    _VariableList[_VoIPBLSZoneVariables[15]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "16")
                {
                    _VariableList[_VoIPBLSZoneVariables[16]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "17")
                {
                    _VariableList[_VoIPBLSZoneVariables[17]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "18")
                {
                    _VariableList[_VoIPBLSZoneVariables[18]].SetValue(0, _SplitItems[2]);
                }
                else if (_SplitItems[1] == "19")
                {
                    _VariableList[_VoIPBLSZoneVariables[19]].SetValue(0, _SplitItems[2]);
                }
            }
            /// <summary>
            /// Get all status PID Station
            /// </summary>
            if (_SplitItems[0] == "PID.CNC")
            {
                // Station List
                if (_SplitItems[1] == "0")
                {
                    _VariableList[_PIDVariablesVLDs[0]].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "1")
                {
                    _VariableList[_PIDVariablesVLDs[1]].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "2")
                {
                    _VariableList[_PIDVariablesEQUs[0]].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "3")
                {
                    _VariableList[_PIDVariablesEQUs[1]].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "4")
                {
                    _VariableList[_PIDVariablesPLMs[0]].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "5")
                {
                    _VariableList[_PIDVariablesPLMs[1]].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "6")
                {
                    _VariableList[_PIDVariablesBLUs[0]].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "7")
                {
                    _VariableList[_PIDVariablesBLUs[1]].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "8")
                {
                    _VariableList[_PIDVariablesBLSs[0]].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "9")
                {
                    _VariableList[_PIDVariablesBLSs[1]].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "10")
                {
                    _VariableList[_PIDVariablesDPDs[0]].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "11")
                {
                    _VariableList[_PIDVariablesDPDs[1]].SetValue(0, _SplitItems[2]);
                }
            }
            if (_SplitItems[0] == "PID.PFM")
            {
                // Station List
                if (_SplitItems[1] == "0")
                {
                    _VariableList[_PIDVariablesVLDs[2]].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "1")
                {
                    _VariableList[_PIDVariablesVLDs[3]].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "2")
                {
                    _VariableList[_PIDVariablesEQUs[2]].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "3")
                {
                    _VariableList[_PIDVariablesEQUs[3]].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "4")
                {
                    _VariableList[_PIDVariablesPLMs[2]].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "5")
                {
                    _VariableList[_PIDVariablesPLMs[3]].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "6")
                {
                    _VariableList[_PIDVariablesBLUs[2]].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "7")
                {
                    _VariableList[_PIDVariablesBLUs[3]].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "8")
                {
                    _VariableList[_PIDVariablesBLSs[2]].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "9")
                {
                    _VariableList[_PIDVariablesBLSs[3]].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "10")
                {
                    _VariableList[_PIDVariablesDPDs[2]].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "11")
                {
                    _VariableList[_PIDVariablesDPDs[3]].SetValue(0, _SplitItems[2]);
                }
            }
            /// <summary>
            /// Get all status PA Station
            /// </summary>
            if (_SplitItems[0] == "PA.PA")
            {
                // Velodrome
                if (_SplitItems[1] == "0")
                {
                    _VariableList["OA.PA.VLD.Line.1.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "1")
                {
                    _VariableList["OA.PA.VLD.Line.2.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "2")
                {
                    _VariableList["OA.PA.VLD.Line.3.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "3")
                {
                    _VariableList["OA.PA.VLD.Line.4.Status"].SetValue(0, _SplitItems[2]);
                }
                // Equistrian
                if (_SplitItems[1] == "4")
                {
                    _VariableList["OA.PA.EQU.Line.1.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "5")
                {
                    _VariableList["OA.PA.EQU.Line.2.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "6")
                {
                    _VariableList["OA.PA.EQU.Line.3.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "7")
                {
                    _VariableList["OA.PA.EQU.Line.4.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "8")
                {
                    _VariableList["OA.PA.EQU.Line.5.Status"].SetValue(0, _SplitItems[2]);
                }
                // Pulomas
                if (_SplitItems[1] == "9")
                {
                    _VariableList["OA.PA.PLM.Line.1.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "10")
                {
                    _VariableList["OA.PA.PLM.Line.2.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "11")
                {
                    _VariableList["OA.PA.PLM.Line.3.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "12")
                {
                    _VariableList["OA.PA.PLM.Line.4.Status"].SetValue(0, _SplitItems[2]);
                }
                // Boulevard Utara
                if (_SplitItems[1] == "13")
                {
                    _VariableList["OA.PA.BLU.Line.1.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "14")
                {
                    _VariableList["OA.PA.BLU.Line.2.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "15")
                {
                    _VariableList["OA.PA.BLU.Line.3.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "16")
                {
                    _VariableList["OA.PA.BLU.Line.4.Status"].SetValue(0, _SplitItems[2]);
                }
                // Boulevard Selatan
                if (_SplitItems[1] == "17")
                {
                    _VariableList["OA.PA.BLS.Line.1.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "18")
                {
                    _VariableList["OA.PA.BLS.Line.2.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "19")
                {
                    _VariableList["OA.PA.BLS.Line.3.Status"].SetValue(0, _SplitItems[2]);
                }
                // Depo Dua
                if (_SplitItems[1] == "20")
                {
                    _VariableList["OA.PA.DPD.Line.1.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "21")
                {
                    _VariableList["OA.PA.DPD.Line.2.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "22")
                {
                    _VariableList["OA.PA.DPD.Line.3.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "23")
                {
                    _VariableList["OA.PA.DPD.Line.4.Status"].SetValue(0, _SplitItems[2]);
                }

            }
            if (_SplitItems[0] == "PA.NPA")
            {
                // Velodrome
                if (_SplitItems[1] == "0")
                {
                    _VariableList["OA.NPA.VLD.Line.1.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "1")
                {
                    _VariableList["OA.NPA.VLD.Line.2.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "2")
                {
                    _VariableList["OA.NPA.VLD.Line.3.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "3")
                {
                    _VariableList["OA.NPA.VLD.Line.4.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "4")
                {
                    _VariableList["OA.NPA.VLD.Line.5.Status"].SetValue(0, _SplitItems[2]);
                }

                // Equistrian
                if (_SplitItems[1] == "5")
                {
                    _VariableList["OA.NPA.EQU.Line.1.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "6")
                {
                    _VariableList["OA.NPA.EQU.Line.2.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "7")
                {
                    _VariableList["OA.NPA.EQU.Line.3.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "8")
                {
                    _VariableList["OA.NPA.EQU.Line.4.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "9")
                {
                    _VariableList["OA.NPA.EQU.Line.5.Status"].SetValue(0, _SplitItems[2]);
                }

                //Pulomas
                if (_SplitItems[1] == "10")
                {
                    _VariableList["OA.NPA.PLM.Line.1.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "11")
                {
                    _VariableList["OA.NPA.PLM.Line.2.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "12")
                {
                    _VariableList["OA.NPA.PLM.Line.3.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "13")
                {
                    _VariableList["OA.NPA.PLM.Line.4.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "14")
                {
                    _VariableList["OA.NPA.PLM.Line.5.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "15")
                {
                    _VariableList["OA.NPA.PLM.Line.6.Status"].SetValue(0, _SplitItems[2]);
                }

                //Boulevard Utara
                if (_SplitItems[1] == "16")
                {
                    _VariableList["OA.NPA.BLU.Line.1.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "17")
                {
                    _VariableList["OA.NPA.BLU.Line.2.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "18")
                {
                    _VariableList["OA.NPA.BLU.Line.3.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "19")
                {
                    _VariableList["OA.NPA.BLU.Line.4.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "20")
                {
                    _VariableList["OA.NPA.BLU.Line.5.Status"].SetValue(0, _SplitItems[2]);
                }

                //Boulevard Selatan
                if (_SplitItems[1] == "21")
                {
                    _VariableList["OA.NPA.BLS.Line.1.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "22")
                {
                    _VariableList["OA.NPA.BLS.Line.2.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "23")
                {
                    _VariableList["OA.NPA.BLS.Line.3.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "24")
                {
                    _VariableList["OA.NPA.BLS.Line.4.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "25")
                {
                    _VariableList["OA.NPA.BLS.Line.5.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "26")
                {
                    _VariableList["OA.NPA.BLS.Line.6.Status"].SetValue(0, _SplitItems[2]);
                }

                //Depo Dua
                if (_SplitItems[1] == "27")
                {
                    _VariableList["OA.NPA.DPD.Line.1.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "28")
                {
                    _VariableList["OA.NPA.DPD.Line.2.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "29")
                {
                    _VariableList["OA.NPA.DPD.Line.3.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "30")
                {
                    _VariableList["OA.NPA.DPD.Line.4.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "31")
                {
                    _VariableList["OA.NPA.DPD.Line.5.Status"].SetValue(0, _SplitItems[2]);
                }
            }
            if (_SplitItems[0] == "PA.PFM")
            {
                // Velodrome
                if (_SplitItems[1] == "0")
                {
                    _VariableList["OA.PF.VLD.Line.1.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "1")
                {
                    _VariableList["OA.PF.VLD.Line.2.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "2")
                {
                    _VariableList["OA.PF.VLD.Line.3.Status"].SetValue(0, _SplitItems[2]);
                }
                // Equistrian
                if (_SplitItems[1] == "3")
                {
                    _VariableList["OA.PF.EQU.Line.1.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "4")
                {
                    _VariableList["OA.PF.EQU.Line.2.Status"].SetValue(0, _SplitItems[2]);
                }
                // Pulomas
                if (_SplitItems[1] == "5")
                {
                    _VariableList["OA.PF.PLM.Line.1.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "6")
                {
                    _VariableList["OA.PF.PLM.Line.2.Status"].SetValue(0, _SplitItems[2]);
                }
                // Boulevard Utara
                if (_SplitItems[1] == "7")
                {
                    _VariableList["OA.PF.BLU.Line.1.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "8")
                {
                    _VariableList["OA.PF.BLU.Line.2.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "9")
                {
                    _VariableList["OA.PF.BLU.Line.3.Status"].SetValue(0, _SplitItems[2]);
                }
                // Boulevard Selatan
                if (_SplitItems[1] == "10")
                {
                    _VariableList["OA.PF.BLS.Line.1.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "11")
                {
                    _VariableList["OA.PF.BLS.Line.2.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "12")
                {
                    _VariableList["OA.PF.BLS.Line.3.Status"].SetValue(0, _SplitItems[2]);
                }
                // Depo Dua
                if (_SplitItems[1] == "13")
                {
                    _VariableList["OA.PF.DPD.Line.1.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "14")
                {
                    _VariableList["OA.PF.DPD.Line.2.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "15")
                {
                    _VariableList["OA.PF.DPD.Line.3.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "16")
                {
                    _VariableList["OA.PF.DPD.Line.4.Status"].SetValue(0, _SplitItems[2]);
                }
            }
            /// <summary>
            /// Get all status PA AFIL Station
            /// </summary>
            if (_SplitItems[0] == "PA.AFIL")
            {
                // Velodrome
                if (_SplitItems[1] == "0")
                {
                    _VariableList["OA.AFIL.VLD.Line.1.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "1")
                {
                    _VariableList["OA.AFIL.VLD.Line.2.Status"].SetValue(0, _SplitItems[2]);
                }
                // Equistrian
                if (_SplitItems[1] == "2")
                {
                    _VariableList["OA.AFIL.EQU.Line.1.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "3")
                {
                    _VariableList["OA.AFIL.EQU.Line.2.Status"].SetValue(0, _SplitItems[2]);
                }
                // Pulomas
                if (_SplitItems[1] == "4")
                {
                    _VariableList["OA.AFIL.PLM.Line.1.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "5")
                {
                    _VariableList["OA.AFIL.PLM.Line.2.Status"].SetValue(0, _SplitItems[2]);
                }
                // Boulevard Utara
                if (_SplitItems[1] == "6")
                {
                    _VariableList["OA.AFIL.BLU.Line.1.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "7")
                {
                    _VariableList["OA.AFIL.BLU.Line.2.Status"].SetValue(0, _SplitItems[2]);
                }
                // Boulevard Selatan
                if (_SplitItems[1] == "8")
                {
                    _VariableList["OA.AFIL.BLS.Line.1.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "9")
                {
                    _VariableList["OA.AFIL.BLS.Line.2.Status"].SetValue(0, _SplitItems[2]);
                }
                // Depot
                if (_SplitItems[1] == "10")
                {
                    _VariableList["OA.AFIL.DPD.Line.1.Status"].SetValue(0, _SplitItems[2]);
                }
                if (_SplitItems[1] == "11")
                {
                    _VariableList["OA.AFIL.DPD.Line.2.Status"].SetValue(0, _SplitItems[2]);
                }

            }
        }
    }
}