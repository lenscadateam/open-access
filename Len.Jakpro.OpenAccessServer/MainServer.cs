﻿using System;
using System.Net;
using Len.Jakpro.ClassOpenAccess;
using NLog;

namespace Len.Jakpro.OpenAccessServer
{
    public class MainServer
    {
        public static void Main(string[] args)
        {
            //ServiceBase[] ServicesToRun;
            //ServicesToRun = new ServiceBase[]
            //{
            //    new OpenAccessServices()
            //};
            //ServiceBase.Run(ServicesToRun);
            // New Object OA Send ( Comment This when using services)
            OpenAccessDriver _OA = new OpenAccessDriver();
            _OA.StartSend("127.0.0.1", 1505);
            // Note Send All State Devices to HMI
            // this received the data from zenon HMI Ex Microphone and Zone also ACK form add ins
            ClassReceivedTCP _TCP = new ClassReceivedTCP(_OA);
            _TCP.StartListen(IPAddress.Parse("127.0.0.1"), 1504);
            // hold a process
            Console.ReadKey();         
        }
    } 
}
