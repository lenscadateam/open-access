﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading;
using Len.Jakpro.ClassOpenAccess;
using SimpleTCP;
using System.Data.SqlClient;
using System.Data;
using Len.Jakpro.OA.Logging;
using NLog;
using Len.Jakpro.Database.Rendundancy;
using Microsoft.Win32;

namespace Len.Jakpro.OpenAccessServer
{
    class ClassReceivedTCP
    {
        private List<string> _ZoneList = new List<string>();
        private List<string> _ZoneListPA = new List<string>();
        private readonly List<uint> _PreDVA = new List<uint>(new uint[] { 99200 }); // Pre DVA
        private readonly List<string> _DictionarySchedule = new List<string>();
        private OpenAccessDriver _OA;
        private string _Variable;
        private string _Data;
        private string _ZoneData;
        private Connection _Con;
        private Thread _Listener;
        private IPAddress _IPAddr = null;
        private int _Ports = 0;
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private static readonly string _Datetime = DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss : ");

        public ClassReceivedTCP(OpenAccessDriver OA)
        {
            _OA = OA;
            OA.ConnectAudioServer();
        }
        /// <summary>
        /// Listen for data incoming from HMI
        /// </summary>
        public void StartListen(IPAddress ListenIP,int Port)
        {
            var _logging = new NLogConfigurator();
            _logging.Configure();

            _IPAddr = ListenIP;
            _Ports = Port;
            // create new thread
            _Listener = new Thread(new ThreadStart(ListenTCP))
            {
                IsBackground = true
            };
            _Listener.Start();
        }
        /// <summary>
        /// a method for listen TCP
        /// </summary>
        public void ListenTCP()
        {
            SimpleTcpServer _Server = new SimpleTcpServer
            {
                Delimiter = 0x13, //Config
                StringEncoder = Encoding.UTF8 //Config
            }; //Instantiate server.
            try
            {
                _Server.DataReceived += Server_DataReceived; //Subscribe to DataRecieved event.
                _Server.Start(_IPAddr, _Ports); //Start listening to incoming connections and data.
                Console.WriteLine(DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss :")+"Listen TCP ON");
            }
            catch (Exception e)
            {
                _logger.Error(_Datetime + e.Message.ToString());
            }        
        }
        /// <summary>
        /// Event handler for data received
        /// </summary>
        public void Server_DataReceived(object sender, Message m)
        {            
            string[] _SplitItems = m.MessageString.Split(',');
            byte[] _mydata;
            //string data;
            _mydata = m.Data;
            //data = BitConverter.ToString(mydata);
            //Console.WriteLine("RX: " + data);
            try
            {
                bool _isStartByteCorrect = _mydata[0] == 0x23;
                bool _isLengthCorrect = _mydata.Length == _mydata[1];
                bool _isHeartBeat = _mydata[3] == 0x24;
                bool _isStopByteCorrect = _mydata[_mydata.Length - 1] == 0x2A;
                if (_isStartByteCorrect && _isLengthCorrect && _isHeartBeat && _isStopByteCorrect)
                {
                    byte _sequence = _mydata[2];
                    byte[] _reply = new byte[5] { 0x23, 0x05, _sequence, 0x25, 0x2A };
                    SimpleTcpClient _client;
                    _client = new SimpleTcpClient().Connect("127.0.0.1", 1505);
                    _client.Write(_reply);
                    _client.Dispose();
                    Console.WriteLine(_Datetime + "TX: " + BitConverter.ToString(_reply) + " Heartbeat received!");
                }                        
            }
            catch (Exception ex)
            {
                _logger.Error(_Datetime + ex.Message.ToString());
            };
            /// <summary>
            /// Added Zoning For DVA,PA,PID
            /// </summary>
            if (_SplitItems[0] == "ZONE")
            {
                try
                {
                    _ZoneList.Clear();
                    _ZoneListPA.Clear();
                    foreach (string zone in _SplitItems)
                    {
                        _ZoneList.Add(zone);
                        _ZoneListPA.Add(zone);
                    }
                    // Remove format STNZONE
                    _ZoneList.RemoveAt(0);
                    _ZoneListPA.RemoveAt(0);
                  //  System.Windows.Forms.MessageBox.Show(_ZoneList.Count.ToString());
                    // Get Zone Data Received
                    foreach (string zone in _ZoneList)
                    {
                        Console.WriteLine(DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss :") +"Zone Data Received :" +zone);
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(_Datetime + ex.Message.ToString());
                }
            }
            /// <summary>
            /// Live Announcements from Commands HMI
            /// </summary>
            if (_SplitItems[0] == "PAActive")
            {
                try
                {
                    RegistryKey _key = Registry.CurrentUser.OpenSubKey("Software\\Len.Jakpro.OpenAccess\\IPPASettings");
                    object _ObjMic = _key.GetValue("DefaultMicrophone");
                    int _Mic = Convert.ToInt32(_ObjMic);
                    _OA.GetPreDVALiveAnnouncements(_ZoneList, _PreDVA);
                    _OA.GetLiveAnnouncement(_Mic, _ZoneList);
                    _key.Close();
                    Console.WriteLine(_Datetime + "Microphone Registered with ID:" + _Mic);
                }
                catch (Exception ex)
                {
                    _logger.Error(_Datetime + ex.Message.ToString());
                }
            }
            if (_SplitItems[0] == "PAInactive")
            {
                try
                {
                    _OA.GetDisconnectAnnoucement();
                }
                catch (Exception ex)
                {
                    _logger.Error(_Datetime + ex.Message.ToString());
                }
            }
            /// <summary>
            /// PID Message from commands HMI
            /// </summary>
            if (_SplitItems[0] == "PIDMessage")
            {
                try
                {
                    _OA._GetPlayPIDText("Display", _ZoneList, _SplitItems[1]);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
            /// <summary>
            /// Initiate PA DVA Audio From HMI
            /// </summary>
            if (_SplitItems[0] == "PADVAAudio")
            {
                try
                {
                    List<uint> DVA = new List<uint>(new uint[] {
                        uint.Parse(_SplitItems[1]),
                        uint.Parse(_SplitItems[2]) }); // Pre DVA
                    _OA.GetPlayDVA(_ZoneListPA, DVA);
                }
                catch (Exception ex)
                {
                    _logger.Error(_Datetime + ex.Message.ToString());
                }
            }
            /// <summary>
            /// Initiate DVA PA Signalling
            /// </summary>
            if (_SplitItems[0] == "PASignalling")
            {
                List<string> _Zonestation = new List<string>();
                List<uint> _DVAApproach = new List<uint>(new uint[] { });
                List<uint> _DVAArrival = new List<uint>(new uint[] { });
                List<uint> _DVADeparting = new List<uint>(new uint[] { });
                try
                {
                    _Con = new Connection();
                    _Con.Connected();
                    _Con.SqlCmd = new SqlCommand
                    {
                        Connection = _Con.SqlCon,
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "OA_ExecuteSignalling"
                    };
                    // Location ID Station
                    _Con.SqlCmd.Parameters.AddWithValue("@LocationID", _SplitItems[1]);
                    // Platform
                    _Con.SqlCmd.Parameters.AddWithValue("@Platform", _SplitItems[2]);
                    // Category Arrival/Depart/Approach  
                    if (_SplitItems[3] == "1")
                    {
                        _Variable = "Approach";
                    }
                    else if (_SplitItems[3] == "2")
                    {
                        _Variable = "Arrival";
                    }
                    else if (_SplitItems[3] == "3")
                    {
                        _Variable = "Depart";
                    }
                    _Con.SqlCmd.Parameters.AddWithValue("@Category", _Variable);
                    _Con.SqlRead = _Con.SqlCmd.ExecuteReader();
                    while (_Con.SqlRead.Read())
                    {
                        // Zone Station from table location
                        _ZoneData = _Con.SqlRead.GetString(1);
                        string[] _MyZoneSplit = _ZoneData.Split(',');
                        foreach (string _split in _MyZoneSplit)
                        {
                            _Zonestation.Add(_split.ToString());
                        }
                        if (_Con.SqlRead.GetString(3)=="Approach")
                        {
                            _Data = _Con.SqlRead.GetString(4);
                            string[] _MySplit = _Data.Split(',');
                            foreach (string _split in _MySplit)
                            {
                                _DVAApproach.Add(Convert.ToUInt32(_split));
                            }
                            _OA._GetDVASignalling(_Zonestation, _DVAApproach);
                            foreach (string _zone in _Zonestation)
                            {
                                Console.WriteLine(DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss :") + "Initiate Zone :"+ _zone);
                            }
                            Console.WriteLine(DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss :") + "Train Position : Approaching  Now !");
                            _DVAApproach.Clear();
                        }
                        else if (_Con.SqlRead.GetString(3)=="Arrival")
                        {
                            _Data = _Con.SqlRead.GetString(4);
                            string[] _MySplit = _Data.Split(',');
                            foreach (string _split in _MySplit)
                            {
                                _DVAArrival.Add(Convert.ToUInt32(_split));
                            }
                            _OA._GetDVASignalling(_Zonestation,_DVAArrival);
                            foreach (string _zone in _Zonestation)
                            {
                                Console.WriteLine(DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss :") + "Initiate Zone :" + _zone);
                            }
                            Console.WriteLine(DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss :") + "Train Position : Arrival  Now !");
                            _DVAArrival.Clear();
                        }
                        else if (_Con.SqlRead.GetString(3)=="Depart")
                        {
                            _Data = _Con.SqlRead.GetString(4);
                            string[] _MySplit = _Data.Split(',');
                            foreach (string _split in _MySplit)
                            {
                                _DVADeparting.Add(Convert.ToUInt32(_split));
                            }
                            _OA._GetDVASignalling(_Zonestation, _DVADeparting);
                            foreach (string zone in _Zonestation)
                            {
                                Console.WriteLine(DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss :") + "Initiate Zone :" + zone);
                            }
                            Console.WriteLine(DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss :") + "Train Position : Departing Now !");
                            _DVADeparting.Clear();
                        }
                    }
                    _Data = "";
                    _ZoneData = "";
                    _Con.SqlCmd.Parameters.Clear();
                }
                catch (SqlException ex)
                {
                    _logger.Error(_Datetime + ex.Message.ToString());
                }
                catch (Exception ex)
                {
                    _logger.Error(_Datetime + ex.Message.ToString());
                }
                // Closed connectiion read after used
                if (!_Con.SqlRead.IsClosed)
                {
                    _Con.SqlRead.Close();
                }
            }
            /// <summary>
            /// Send device state to HMI when runtime running
            /// </summary>
            if (_SplitItems[0] == "ACK" && _SplitItems[1] == "GetDeviceState")
            {
                try
                {
                    _OA.SendDevicesState();
                    Console.WriteLine(DateTime.Now.ToString("dd-MM-yyyy : hh:mm:ss :") + "Request All Device State ....");
                }
                catch (Exception ex)
                {
                    _logger.Error(_Datetime + ex.Message.ToString());
                }
            }
        }
    }
}
